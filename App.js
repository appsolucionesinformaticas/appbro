import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {
  Platform,
  Image,
  ImageBackground,
  SafeAreaView,
  StatusBar,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import Login from './componets/Login';
import Categorias from './componets/Categorias';
import ListaCategoriasItem from './componets/ListaCategoriasItem';
import ListaProductosEmpresa from './componets/ListaProductosEmpresa';
import Ubicacion from './componets/Ubicacion';
import Carrito from './componets/Carrito';
import RegistroMain from './componets/registro/RegistroMain';
import BienvenidoIntro from './componets/registro/BienvenidoIntro';
import Estado from './componets/Estado';
import MapaUbicacionLocation from './componets/MapaUbicacionLocation';
import MiCuenta from './componets/MiCuenta';
import Historial from './componets/Historial';
import Ayuda from './componets/Ayuda';
import UbicacionesFavoritas from './componets/UbicacionesFavoritas';
import Notificaciones from './componets/Notificaciones';
import Brocoins from './componets/Brocoins';
import RecuperarPass from './componets/RecuperarPass';
import MainMenu from './componets/MainMenu';
import LoginNew from './componets/LoginNew';
import Registro from './componets/Registro';
import Localidad from './componets/Localidad';
import MisDatos from './componets/MisDatos';
import FollowDriver from './componets/FollowDriver';
import MisPedidos from './componets/MisPedidos';
import Profile from './componets/profile/Profile';

import MisRestaurants from './componets/MisRestaurants';

import messaging from '@react-native-firebase/messaging';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();

function globalVariable() {
  global.linkServer = 'http://3.140.165.7:8080/broServer/broRest/';
  // global.linkServer = 'http://192.168.1.15:8080/broServer/broRest/';
}

async function requestUserPermission() {
  const authStatus = await messaging().requestPermission();
  const enabled =
    authStatus === AuthorizationStatus.AUTHORIZED ||
    authStatus === AuthorizationStatus.PROVISIONAL;

  if (enabled) {
  }
}

function App() {
  const [person, setperson] = useState();
  const [firts, setfirts] = useState();

  useEffect(() => {
    getInfo();
  }, []);

  const getInfo = () => {
    // AsyncStorage.setItem('registro', false);
    AsyncStorage.getItem('firts')
      .then((firts) => {
        if (firts !== null) {
          setfirts(true);
          console.log('Segunda Vez');
          AsyncStorage.getItem('person')
            .then((data) => {
              if (data !== null) {
                const per = JSON.parse(data);

                console.log('---person' + per);
                setperson(per);
              } else {
                console.log('--- no hay person');
              }
            })
            .catch((err) => {
              alert(err);
            });
        } else {
          AsyncStorage.mergeItem('firts', JSON.stringify(true));
          console.log('Primer vez');

          // const itemPerson = {
          //   name: "",
          //   lastname: "",
          //   email: "",
          //   ci: "",
          //   cellphone: "",
          //   pass: "",
          //   latitud: 0.0,
          //   longitud: 0.0,
          //   urlphoto: "",
          //   addres: '',
          //   codeCity: 0,
          //   nameCiudad: "",
          //   latitudCiudad: 0.0,
          //   longitudCiudad: 0.0,
          //   tarifa: "",
          //   distancia: "",
          //   tminima: "",
          // };

          // // AsyncStorage.getItem('person')
          // // .then((data) => {
          // AsyncStorage.setItem('person', JSON.stringify(itemPerson));
          // props.navigation.reset({ routes: [{name: 'MainMenu'}],});
          // })
          // .catch((error) => {
          //   setcargando(false);
          //   alert(error);
          // });
        }
      })
      .catch((err) => {
        alert(err);
      });
  };
  globalVariable();
  requestUserPermission();
  return (
    <>
      <SafeAreaView style={{flex: 0, backgroundColor: '#0f8d8c'}} />
      <SafeAreaProvider>
        <NavigationContainer>
          <StatusBar backgroundColor="#0f8d8c" />
          <Stack.Navigator>
            {firts == null ? (
              <>
                <Stack.Screen
                  name="Localidad"
                  component={Localidad}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="MainMenu"
                  component={MainMenu}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="RecuperarPass"
                  component={RecuperarPass}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Profile"
                  component={Profile}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="MisRestaurants"
                  component={MisRestaurants}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="MisPedidos"
                  component={MisPedidos}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Registro"
                  component={Registro}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="MisDatos"
                  component={MisDatos}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Categorias"
                  component={Categorias}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Ubicacion"
                  component={Ubicacion}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="ListaCategoriasItem"
                  component={ListaCategoriasItem}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="ListaProductosEmpresa"
                  component={ListaProductosEmpresa}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Carrito"
                  component={Carrito}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="RegistroMain"
                  component={RegistroMain}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="BienvenidoIntro"
                  component={BienvenidoIntro}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Estado"
                  component={Estado}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="MapaUbicacionLocation"
                  component={MapaUbicacionLocation}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="MiCuenta"
                  component={MiCuenta}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Historial"
                  component={Historial}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Ayuda"
                  component={Ayuda}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="UbicacionesFavoritas"
                  component={UbicacionesFavoritas}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Notificaciones"
                  component={Notificaciones}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Brocoins"
                  component={Brocoins}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="FollowDriver"
                  component={FollowDriver}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="LoginNew"
                  component={LoginNew}
                  options={{headerShown: false}}
                />
              </>
            ) : (
              <>
                {/* <Stack.Screen
                    name="Localidad"
                    component={Localidad}
                    options={{ headerShown: false }}
                  /> */}
                <Stack.Screen
                  name="MainMenu"
                  component={MainMenu}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Profile"
                  component={Profile}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="RecuperarPass"
                  component={RecuperarPass}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Login"
                  component={Login}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Registro"
                  component={Registro}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="MisPedidos"
                  component={MisPedidos}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="FollowDriver"
                  component={FollowDriver}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="MisRestaurants"
                  component={MisRestaurants}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="MisDatos"
                  component={MisDatos}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Localidad"
                  component={Localidad}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="LoginNew"
                  component={LoginNew}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Categorias"
                  component={Categorias}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Ubicacion"
                  component={Ubicacion}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="ListaCategoriasItem"
                  component={ListaCategoriasItem}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="ListaProductosEmpresa"
                  component={ListaProductosEmpresa}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Carrito"
                  component={Carrito}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="RegistroMain"
                  component={RegistroMain}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="BienvenidoIntro"
                  component={BienvenidoIntro}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="Estado"
                  component={Estado}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="MapaUbicacionLocation"
                  component={MapaUbicacionLocation}
                  options={{headerShown: false}}
                />

                <Stack.Screen
                  name="MiCuenta"
                  component={MiCuenta}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Historial"
                  component={Historial}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Ayuda"
                  component={Ayuda}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="UbicacionesFavoritas"
                  component={UbicacionesFavoritas}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Notificaciones"
                  component={Notificaciones}
                  options={{headerShown: false}}
                />
                <Stack.Screen
                  name="Brocoins"
                  component={Brocoins}
                  options={{headerShown: false}}
                />
              </>
            )}
          </Stack.Navigator>
        </NavigationContainer>
      </SafeAreaProvider>
    </>
  );
}
export default App;
