import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import MapView, {
  PROVIDER_GOOGLE,
  Marker,
  AnimatedRegion,
  ProviderPropType,
} from 'react-native-maps';
var {height, width} = Dimensions.get('window');

import Geocoder from 'react-native-geocoding';
import database from '@react-native-firebase/database';
import FastImage from 'react-native-fast-image';

const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;

class FollowDriver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ref: '',
      change: '',
      latitude: -2.891447,
      longitude: -78.999071,
      latitudeDelta: 0.007,
      longitudeDelta: 0.007,

      latmove: 0,
      lonmove: 0,
      rotaciom: 0,

      coordinate: new AnimatedRegion({
        latitude: -2.891447,
        longitude: -78.999071,
        latitudeDelta: 0,
        longitudeDelta: 0,
      }),
    };
  }

  componentDidMount() {
    Geocoder.init('AIzaSyCb7GI6TekZoXdGgUQWXP59benBCf-1VEo');

    try {
      this.state.ref = database().ref(
        '/locationDriver/' + this.props.route.params.idDriver + '/',
      );

      this.state.change = this.state.ref.on('value', (snapshot) => {
        if (snapshot.val() != null) {
          this.animate(snapshot.val().l[0], snapshot.val().l[1], 270);
        }
      });
    } catch (error) {}
  }

  componentWillUnmount() {
    this.state.ref.off('value', this.state.change);
  }

  animate(latitud, longituded, rotacion) {
    try {
      let {coordinate, latitude, longitude, rotaciom} = this.state;

      // coordinate.latitude. = -2.892447
      // coordinate.longitude = -78.949071

      rotaciom = rotacion;
      latitude = latitud;
      longitude = longituded;
      coordinate = new AnimatedRegion({
        latitude: latitud,
        longitude: longituded,
        latitudeDelta: 0.007,
        longitudeDelta: 0.007,
      });

      this.setState({coordinate, latitude, longitude, rotaciom});

      // const newCoordinate = {
      //   latitude: -2.892447 ,
      //   longitude: -78.949071
      // };

      // if (Platform.OS === 'android') {
      //   if (this.marker) {
      //     this.marker._component.animateMarkerToCoordinate(newCoordinate, 400);
      //   }
      // } else {
      //   coordinate.timing(newCoordinate).start();
      // }
    } catch (error) {}
  }

  render() {
    return (
      <View style={{flex: 1, width: width, height: height}}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          showUserLocation={true}
          followUserLocation={true}
          loadingEnabled={true}
          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}>
          <Marker.Animated
            ref={(marker) => {
              this.marker = marker;
            }}
            anchor={{x: 0.45, y: 0.42}}
            coordinate={this.state.coordinate}
            animateMarkerToCoordinate={this.state.coordinate}

            // image={<Image source={require('../../assets/img/ic_vesi_driver.png')} />}
          >
            <FastImage
              source={require('../assets/img/ic_bro_camino_2.gif')}
              style={{height: 50, width: 50}}
              resizeMode="center"
            />
          </Marker.Animated>
        </MapView>

        <View
          style={{
            position: 'absolute',
            top: '3%',
            width: '90%',
            flexDirection: 'row',

            justifyContent: 'space-between',
          }}>
          <TouchableOpacity
            style={styles.positionFixes2}
            activeOpacity={0.9}
            onPress={() => this.props.navigation.goBack()}>
            <FastImage
              style={styles.imgBack}
              resizeMode="contain"
              source={require('../images/ic_bro_atras.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

FollowDriver.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  markerFixed: {
    left: '50%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%',
  },
  positionFixes: {
    left: 20,
    right: 20,
    position: 'absolute',
    top: '2%',
    alignItems: 'flex-end',
  },

  imgBack: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },

  positionFixes2: {
    left: 20,
    right: 20,
    resizeMode: 'contain',

    top: '2%',
    alignItems: 'flex-start',
  },

  titleFixes: {
    left: 20,
    right: 20,
    backgroundColor: '#482181',
    position: 'absolute',
    top: '10%',
    alignItems: 'center',
  },

  aceptesFixes: {
    left: 20,
    right: 20,
    position: 'absolute',
    top: '92%',
    alignItems: 'center',
  },

  marker: {
    height: 60,
    width: 60,
  },

  imgPosition: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },

  footer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: 0,
    position: 'absolute',
    width: '100%',
  },
  region: {
    color: '#fff',
    lineHeight: 20,
    margin: 20,
  },
});

export default FollowDriver;
