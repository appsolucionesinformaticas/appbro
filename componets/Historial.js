import React, {Component, useState, useEffect} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';

var {height, width} = Dimensions.get('window');
function Historial(props) {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backJS);

    return () => BackHandler.removeEventListener('hardwareBackPress', backJS);
  }, []);

  const backJS = () => {
    props.navigation.goBack();
  };
  const [listHistoria, setlistHistoria] = useState([
    {
      name: '123123123',
      id: 12,
    },
    {
      name: '123123123',
      id: 2,
    },
    {
      name: '123123123',
      id: 3,
    },
    {
      name: '123123123',
      id: 4,
    },
    {
      name: '123123123',
      id: 5,
    },
    {
      name: '123123123',
      id: 5,
    },
    {
      name: '123123123',
      id: 5,
    },
    {
      name: '123123123',
      id: 5,
    },
    {
      name: '123123123',
      id: 5,
    },
  ]);

  const _renderItemClient = (item) => {
    return (
      <View
        style={{
          alignItems: 'center',
          alignContent: 'center',
          width: width - 20,
          marginTop: 5,
          backgroundColor: 'rgba(0, 178, 170,0.70)',
          borderRadius: 12,
          width: width / 1.1,
        }}>
        <Text
          style={{
            fontSize: 16,
            textAlign: 'center',
            color: 'white',
            width: width / 1.1,
            marginHorizontal: 5,
            paddingVertical: 5,
          }}>
          02-07-2020 14:45{' '}
        </Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: 'left',
            color: 'white',
            width: width / 1.1,
            marginHorizontal: 5,
            paddingVertical: 1,
            paddingLeft: 8,
          }}>
          - Ciudadel la Cascada
        </Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: 'left',
            color: 'white',
            width: width / 1.1,
            marginHorizontal: 5,
            paddingVertical: 1,
            paddingLeft: 8,
          }}>
          - Calle xxxx
        </Text>
        <Text
          style={{
            fontSize: 16,
            textAlign: 'left',
            color: 'white',
            width: width / 1.1,
            marginHorizontal: 5,
            paddingVertical: 5,
            paddingLeft: 8,
          }}>
          - Carlos Andres Arevalo Fernandez
        </Text>
      </View>
    );
  };

  return (
    <ImageBackground
      source={require('../images/ic_bro_fondo_app.png')}
      style={{
        height: null,
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'flex-start',
            alignItems: 'flex-start',
            width: width - 40,
          }}>
          <TouchableOpacity onPress={() => backJS()}>
            <FastImage
              style={{width: 30, height: 30, marginTop: 10}}
              resizeMode="contain"
              source={require('../images/ic_bro_atras_celeste.png')}
            />
          </TouchableOpacity>
        </View>

        <FastImage
          style={{
            width: width / 4,
            height: width / 4,
            marginTop: 10,
            marginBottom: 10,
          }}
          resizeMode="contain"
          source={require('../images/ic_bro_hitorial_celeste.png')}
        />

        <FlatList
          data={listHistoria}
          keyExtractor={(item, index) => index.toString()}
          renderItem={_renderItemClient}
        />
      </SafeAreaView>
    </ImageBackground>
  );
}

export default Historial;
