import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';

var {height, width} = Dimensions.get('window');
function Estado(props) {
  return (
    <ImageBackground
      source={require('../images/ic_bro_fondo_app.png')}
      style={{
        height: null,
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flex: 1,
            alignItems: 'center',
            alignContent: 'center',
            justifyContent: 'center',
          }}>
          <FastImage
            style={{width: width / 4, height: width / 4}}
            resizeMode="contain"
            source={require('../images/ic_bro_estado.png')}
          />

          <Text
            style={{
              color: '#00b2aa',
              marginVertical: 10,
              width: width - 50,
              textAlign: 'center',
            }}>
            Revisa e Interactua con el estado del Pedido
          </Text>

          <View style={{flexDirection: 'row', marginTop: 30}}>
            <TouchableOpacity
              style={{alignItems: 'center', alignContent: 'center'}}>
              <FastImage
                style={{
                  width: width / 4,
                  height: width / 4,
                  marginHorizontal: 5,
                }}
                resizeMode="contain"
                source={require('../images/ic_bro_recibido_des.png')}
              />
              <Text
                style={{
                  color: '#00b2aa',
                  marginVertical: 10,
                  width: width / 2 - 20,
                  textAlign: 'center',
                }}>
                Recibido
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{alignItems: 'center', alignContent: 'center'}}>
              <FastImage
                style={{
                  width: width / 4,
                  height: width / 4,
                  marginHorizontal: 5,
                }}
                resizeMode="contain"
                source={require('../images/ic_bro_preparando_des.png')}
              />
              <Text
                style={{
                  color: '#00b2aa',
                  marginVertical: 10,
                  width: width / 2 - 20,
                  textAlign: 'center',
                }}>
                Preparando
              </Text>
            </TouchableOpacity>
          </View>

          <View style={{flexDirection: 'row', marginTop: 20}}>
            <TouchableOpacity
              style={{alignItems: 'center', alignContent: 'center'}}>
              <FastImage
                style={{
                  width: width / 4,
                  height: width / 4,
                  marginHorizontal: 5,
                }}
                resizeMode="contain"
                source={require('../images/ic_bro_camino_des.png')}
              />
              <Text
                style={{
                  color: '#00b2aa',
                  marginVertical: 10,
                  width: width / 2 - 20,
                  textAlign: 'center',
                }}>
                En Camino
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{alignItems: 'center', alignContent: 'center'}}>
              <FastImage
                style={{
                  width: width / 4,
                  height: width / 4,
                  marginHorizontal: 5,
                }}
                resizeMode="contain"
                source={require('../images/ic_bro_llego_des.png')}
              />
              <Text
                style={{
                  color: '#00b2aa',
                  marginVertical: 10,
                  width: width / 2 - 20,
                  textAlign: 'center',
                }}>
                Llego!
              </Text>
            </TouchableOpacity>
          </View>

          <Text
            style={{
              color: 'white',
              marginVertical: 30,
              width: width - 50,
              textAlign: 'center',
              backgroundColor: 'rgba(238, 130, 79,0.9)',
              width: width / 2,
              paddingVertical: 8,
              borderRadius: 8,
            }}>
            Finalizar
          </Text>
        </View>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default Estado;
