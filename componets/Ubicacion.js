import React,{Component,useState} from 'react';
import {ImageBackground,Text,Dimensions,FlatList,StatusBar,Modal,TouchableOpacity,TouchableWithoutFeedback,Linking, SafeAreaView, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle'

var {height,width} = Dimensions.get("window")
function Ubicacion(props) {

    return(


        <ImageBackground
                source={require('../images/ic_bro_fondo_app.png')}
                style={{height: null,
                width: width,
                resizeMode: "cover",
                overflow: "hidden",
                flex: 1}}>

        <SafeAreaView style = {styles.container}>  

       

            
            <View style={{ flex:1,alignItems:'center',alignContent:'center',justifyContent:'center'}}>

                <FastImage style = {{width:width/4,height:width/4}} resizeMode='contain' source={require('../images/ic_bro_ubicacion_celeste.png')}/>
                
                <Text style={{color:'#00b2aa',marginVertical:10,width:width-50,textAlign:'center'}}>Comparte tu ubicación o coloca la dirección hacia donde quieras llegue tu pedido</Text>

                <Text style={{color:'white',marginVertical:20,width:width-50,textAlign:'center',backgroundColor:"rgba(238, 130, 79,0.9)",width:width/2,paddingVertical:8,borderRadius:8}}>Ubicación Actual</Text>

                <Text style={{color:'white',marginVertical:20,width:width-50,textAlign:'center',backgroundColor:"rgba(238, 130, 79,0.9)",width:width/2,paddingVertical:8,borderRadius:8}}>Agrega una dirección</Text>


            </View>

                    


           
        </SafeAreaView>

        </ImageBackground>
    )

}





export default Ubicacion;