import * as React from 'react';
import {Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {MaterialCommunityIcons} from 'react-native-vector-icons';
//import { createMaterialTopTabNavigator } from '@react-navigation/bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import FastImage from 'react-native-fast-image';
import Categorias from './Categorias';
import Miorden from './Miorden';
import Profile from './profile/Profile';

//const Tab = createBottomTabNavigator();

const Tab = createMaterialBottomTabNavigator();

function MainMenu() {
  return (
    <Tab.Navigator
      style={{backgroundColor: 'red'}}
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'Explorar') {
            iconName = focused
              ? require('../assets/img/ic_bro_explorar_act.png')
              : require('../assets/img/ic_bro_explorar_des.png');
          } else if (route.name === 'Mi orden') {
            iconName = focused
              ? require('../assets/img/ic_bro_orden_act.png')
              : require('../assets/img/ic_bro_orden_des.png');
          } else if (route.name === 'Perfil') {
            iconName = focused
              ? require('../assets/img/ic_bro_perfil_act.png')
              : require('../assets/img/ic_bro_perfil_des.png');
          }

          // You can return any component that you like here!
          return (
            <FastImage
              source={iconName}
              style={{width: 25, height: 25}}
              resizeMode="contain"
            />
          );
        },
      })}
      activeColor="#ffa35d"
      inactiveColor="gray"
      barStyle={{backgroundColor: '#ffff'}}
      backBehavior="none">
      <Tab.Screen name="Explorar" component={Categorias} />
      <Tab.Screen name="Mi orden" component={Miorden} />

      <Tab.Screen name="Perfil" component={Profile} />
    </Tab.Navigator>
  );
}

export default MainMenu;
