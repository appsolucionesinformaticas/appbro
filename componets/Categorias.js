import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';
import Swiper from 'react-native-swiper';

var {height, width} = Dimensions.get('window');
function Categorias(props) {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isModalMenu, setisModalMenu] = useState(false);
  const [search, setSearch] = useState('');

  const [dataBanner] = useState([
    require('../assets/img/banner1.png'),
    require('../assets/img/banner2.png'),
    require('../assets/img/banner3.png'),
  ]);
  const sliupMenu = React.createRef();

  const [ltsCategorias, setltsCategorias] = useState([]);
  const [ltsPublicidad, setltsPublicidad] = useState([]);
  const [person, setperson] = useState('');
  const [publicidades, setpublicidades] = useState(0);
  const [procesos, setprocesos] = useState(0);

  const porcentajeHeigt = (por) => {
    return (height * por) / 100;
  };

  const _renderItemClient = (item) => {
    return (
      <View
        style={{
          alignItems: 'center',
          alignContent: 'center',
          width: '100%',
          marginTop: 10,
        }}>
        <TouchableOpacity
          // style={styles.listCategoria}
          style={{
            alignItems: 'center',
            alignContent: 'center',
            width: '80%',
            backgroundColor: item.item.cat_color,
            borderRadius: 12,
          }}
          activeOpacity={0.1}
          onPress={() =>
            props.navigation.navigate('ListaCategoriasItem', {categoria: item})
          }>
          {/* <Text style={styles.textCategoria}>Modelo moto</Text> */}
          <View style={styles.imgCategoriaEmpresas}>
            <FastImage
              style={styles.imgCategoriaEmpresas2}
              source={{
                uri: item.item.cat_url_image,
                priority: FastImage.priority.normal,
              }}
              resizeMode={FastImage.resizeMode.contain}
            />
          </View>
        </TouchableOpacity>

        <Text
          style={{
            fontSize: 14,
            textAlign: 'center',
            color: 'black',
            fontFamily: 'Metropolis-Bold',
          }}>
          {item.item.cat_name}
        </Text>
      </View>
    );
  };

  const updateSearch = (search) => {
    setSearch(search);
  };

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);

          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
          }, 100);

          getPublicidad(personData.codeCity);
          lisCategorias(personData.codeCity);
          if (person.ci != '') {
            getProcesos(personData.ci);
          }
        } else {
        }
      })
      .catch((err) => {
        alert(err);
      });
  };
  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
    });

    return unsubscribe;
  }, []);

  const lisCategorias = (code) => {
    fetch(global.linkServer + 'wscategory/ltsCategory?idLoc=' + code)
      .then((response) => response.json())
      .then((data) => {
        setltsCategorias(data);
      })
      .catch((error) => console.log(error));
  };

  const getPublicidad = (code) => {
    console.log(global.linkServer + 'wspublicidad/ltsPublicidad?idLoc=' + code);
    fetch(global.linkServer + 'wspublicidad/ltsPublicidad?idLoc=' + code)
      .then((response) => response.json())
      .then((data) => {
        setpublicidades(data.length);
        setltsPublicidad(data);
        // setltsCategorias(data);
      })
      .catch((error) => console.log(error));
  };

  const getProcesos = (code) => {
    fetch(global.linkServer + 'wsorder/getLtsOrderByUserStatus?ci=' + code)
      .then((response) => response.json())
      .then((data) => {
        setprocesos(data.length);
      })
      .catch((error) => console.log(error));
  };

  const proceso = () => {
    if (procesos >= 1) {
      return (
        <TouchableOpacity
          activeOpacity={0.1}
          onPress={() => props.navigation.navigate('Notificaciones')}>
          <FastImage
            style={{width: porcentaje(12), height: porcentaje(12)}}
            resizeMode="contain"
            source={require('../assets/img/ic_bro_camino_2.gif')}
          />
        </TouchableOpacity>
      );
    }
  };

  const publucidad = () => {
    if (publicidades >= 1) {
      return (
        <>
          <View
            style={{
              width: '100%',
              height: '65%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                width: '90%',
              }}>
              <TouchableOpacity activeOpacity={0.1}>
                <Text
                  style={{
                    marginTop: 8,
                    fontFamily: 'Metropolis-Bold',
                    fontSize: 18,
                  }}>
                  {person.nameCiudad}
                </Text>
              </TouchableOpacity>

              {proceso()}
            </View>

            <FlatList
              data={ltsCategorias}
              style={{
                width: '100%',

                alignContent: 'center',
              }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={_renderItemClient}
              refreshing={isRefreshing}
            />
          </View>

          <View
            style={{
              width: '100%',
              height: '35%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              alignContent: 'center',
            }}>
            <View
              style={{
                height: '80%',
                width: '90%',
                borderRadius: 12,
              }}>
              <Swiper
                style={{height: '100%', borderRadius: 12}}
                autoplay={true}
                autoplayTimeout={5}>
                {ltsPublicidad.map((itemmap) => {
                  return (
                    <TouchableOpacity activeOpacity={0.9}>
                      <FastImage
                        style={{height: '100%', borderRadius: 12}}
                        resizeMode="cover"
                        source={{
                          uri: itemmap.pub_photo,
                          priority: FastImage.priority.normal,
                        }}
                      />
                    </TouchableOpacity>
                  );
                })}
              </Swiper>
            </View>
          </View>
        </>
      );
    } else {
      return (
        <View
          style={{
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
            alignSelf: 'center',
            alignContent: 'center',
          }}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '90%',
            }}>
            <TouchableOpacity activeOpacity={0.1}>
              <Text
                style={{
                  marginTop: 8,
                  fontFamily: 'Metropolis-Bold',
                  fontSize: 18,
                }}>
                {person.nameCiudad}
              </Text>
            </TouchableOpacity>

            {proceso()}
          </View>
          <FlatList
            data={ltsCategorias}
            style={{
              width: '100%',
              alignContent: 'center',
            }}
            keyExtractor={(item, index) => index.toString()}
            renderItem={_renderItemClient}
            refreshing={isRefreshing}
          />
        </View>
      );
    }
  };

  return (
    <SafeAreaView
      style={{
        alignItems: 'center',
        alignContent: 'center',
      }}>
      {publucidad()}
    </SafeAreaView>
  );
}

export default Categorias;
