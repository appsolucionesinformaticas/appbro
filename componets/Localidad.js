import React, {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  Animated,
  Keyboard,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import AppIntroSlider from 'react-native-app-intro-slider';
import DropDownPicker from 'react-native-dropdown-picker';
import {Icon} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import {ScrollView} from 'react-native-gesture-handler';
var {height, width} = Dimensions.get('window');
function Localidad(props) {
  const [cities, setcities] = useState([]);
  const [country, setcountry] = useState('');
  const [inicio, setinicio] = useState(0);
  const [itemSelect, setitemSelect] = useState();

  const [person, setperson] = useState();

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  const saveLocalizacion = () => {
    AsyncStorage.getItem('person')
      .then((data) => {
        if (data !== null) {
          const per = JSON.parse(data);
          if (per.ci != '') {
            fetch(global.linkServer + 'wsperson/UpdatePerson', {
              method: 'POST',
              headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                code: '',
                per_name: person.name,
                per_last_name: person.lastname,
                per_ci: person.ci,
                per_cellphone: person.cellphone,
                per_email: person.email,
                per_password: person.pass,
                per_url_image: person.urlphoto,
                street: person.addres,
                lon: person.longitud,
                lat: person.latitud,
                idLoc: itemSelect.codigo,
              }),
            })
              .then((response) => response.json())
              .then((data) => {
                if (data == true) {
                  const itemPerson = {
                    codeCity: itemSelect.codigo,
                    nameCiudad: itemSelect.label,
                    latitudCiudad: itemSelect.latitud,
                    longitudCiudad: itemSelect.longitud,
                    tarifa: itemSelect.tarifa,
                    distancia: itemSelect.distancia,
                    tminima: itemSelect.tminima,
                    latitud: 0.0,
                    longitud: 0.0,
                    addres: '',
                  };

                  try {
                    // AsyncStorage.setItem('person', JSON.stringify(itemPerson));
                    AsyncStorage.mergeItem(
                      'person',
                      JSON.stringify(itemPerson),
                    );
                    props.navigation.navigate('MainMenu');
                  } catch (error) {
                    alert('Hubo un error Intenalo de nuevo');
                  }
                } else {
                  alert('Hubo un error Intenalo de nuevo');
                }
              })
              .catch((error) => console.log(error));
          } else if (per.ci == '') {
            console.log('- - - - - - - - - - - editando y dejando en vacio');
            const itemPerson = {
              codeCity: itemSelect.codigo,
              nameCiudad: itemSelect.label,
              latitudCiudad: itemSelect.latitud,
              longitudCiudad: itemSelect.longitud,
              tarifa: itemSelect.tarifa,
              distancia: itemSelect.distancia,
              tminima: itemSelect.tminima,
              latitud: 0.0,
              longitud: 0.0,
              addres: '',
              name: '',
              lastname: '',
              email: '',
              ci: '',
              cellphone: '',
              pass: '',
              latitud: 0.0,
              longitud: 0.0,
              urlphoto: '',
            };

            try {
              AsyncStorage.setItem('person', JSON.stringify(itemPerson));
              // AsyncStorage.mergeItem('person', JSON.stringify(itemPerson));
              props.navigation.navigate('MainMenu');
            } catch (error) {
              alert('Hubo un error Intenalo de nuevo');
            }
          }
        } else {
          console.log('- - - - - - - - - - - editando y dejando en vacio');
          const itemPerson = {
            codeCity: itemSelect.codigo,
            nameCiudad: itemSelect.label,
            latitudCiudad: itemSelect.latitud,
            longitudCiudad: itemSelect.longitud,
            tarifa: itemSelect.tarifa,
            distancia: itemSelect.distancia,
            tminima: itemSelect.tminima,
            latitud: 0.0,
            longitud: 0.0,
            addres: '',
            name: '',
            lastname: '',
            email: '',
            ci: '',
            cellphone: '',
            pass: '',
            latitud: 0.0,
            longitud: 0.0,
            urlphoto: '',
          };

          try {
            AsyncStorage.setItem('person', JSON.stringify(itemPerson));
            // AsyncStorage.mergeItem('person', JSON.stringify(itemPerson));
            props.navigation.navigate('MainMenu');
          } catch (error) {
            alert('Hubo un error Intenalo de nuevo');
          }
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  const slides = [
    {
      key: 1,
      title: '¿Qué es BroDelivery?',
      text:
        'Es la empresa más innovadora y flexible del país, nos dedicamos a la entrega de productos a domicilio; con los precios más bajos del mercado, nuestros deliverys están altamente capacitados ya que sabemos la exigencia de nuestros clientes.',
      image: require('../assets/img/mano.png'),
    },

    {
      key: 2,
      title: 'La Aplicación más segura',
      text:
        'Nuestros tratos con nuestros afiliados son confiables, todo trámite lo llevamos de una manera formal.',
      image: require('../assets/img/manos.png'),
    },
    {
      key: 3,
      title: 'BroCoins - Wallet',
      text:
        'Descubre cuanto generas por ser un Agente Vendedor de BroDelivery ',
      image: require('../assets/img/transfer.png'),
    },
    {
      key: 4,
      title: 'Transferencias',
      text:
        'Todos los lunes, nuestros personal se encargará de comunicarte lo que has generado; nuestras transferencias se acreditarán a tu cuenta bancaria registrada.',
      image: require('../assets/img/wallet.png'),
    },
  ];

  useEffect(() => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);
          setperson(personData);
        }
      })
      .catch((err) => {
        alert(err);
      });

    fetch(global.linkServer + 'wslocation/ltsCities2')
      .then((response) => response.json())
      .then((data) => {
        const newData = data.reduce((r, s) => {
          r.push({
            label: s.loc_name,
            value: s.loc_name,
            latitud: s.loc_latitude,
            longitud: s.loc_longitude,
            codigo: s.loc_id,
            tarifa: s.loc_tarifa,
            distancia: s.loc_distancia_min,
            tminima: s.loc_tarifa_min,
          });
          return r;
        }, []);

        setcities(newData);
        setcountry(data[0].loc_name);
        setitemSelect(newData[0]);
      })
      .catch((error) => console.log(error));

    AsyncStorage.getItem('firtsscreen')
      .then((firts) => {
        if (firts !== null) {
          setinicio(1);
        } else {
          AsyncStorage.mergeItem('firtsscreen', JSON.stringify(true));
          setinicio(0);
        }
      })
      .catch((err) => {
        alert(err);
      });
  }, []);

  const _onDone = () => {
    setinicio(1);
  };

  const _renderItem = ({item}) => {
    return (
      <View style={styles.container}>
        <FastImage
          style={styles.imgLogo}
          resizeMode="contain"
          source={item.image}
        />

        <Text style={styles.txtTitleConsejo}>{item.title}</Text>
        <Text style={styles.txtConsejo}>{item.text}</Text>
      </View>
    );
  };

  const _renderDone = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon type="material" name="check" color={'#0f8d8c'} />
      </View>
    );
  };

  const _renderNextButton = () => {
    return (
      <View style={styles.buttonCircle}>
        <Icon type="material" name="navigate-next" color={'#0f8d8c'} />
      </View>
    );
  };

  const tipoScreen = (tipo) => {
    if (tipo === 1) {
      return (
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}>
          <KeyboardAvoidingView
            behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
            style={{flex: 1}}>
            <SafeAreaView
              style={{
                borderRadius: 12,
              }}>
              <View style={{height: '100%', backgroundColor: 'white'}}>
                <View
                  style={{
                    width: '90%',
                    alignItems: 'flex-start',
                    marginTop: 8,
                  }}>
                  {/* <TouchableOpacity
                  activeOpacity={0.5}
                  style={{marginLeft: 8, padding: 5}}
                  onPress={() => props.navigation.goBack()}>
                  <Icon
                    type="ionicon"
                    name="arrow-back"
                    color="white"
                    size={porcentaje(9)}
                    iconStyle={{color: 'black'}}
                  />
                </TouchableOpacity> */}
                </View>
                <ScrollView
                  style={{
                    height: '100%',
                    borderRadius: 20,
                    backgroundColor: 'white',
                  }}>
                  <View
                    style={{
                      flex: 1,
                      alignItems: 'center',
                      alignContent: 'center',
                      borderRadius: 30,
                      paddingBottom: 20,
                    }}>
                    <Text
                      style={{
                        marginTop: 30,
                        fontSize: 35,
                        textAlign: 'center',
                        color: '#ffa35d',
                        fontFamily: 'Metropolis-Bold',
                        width: '90%',
                      }}>
                      Bienvenido/a
                    </Text>

                    <Text
                      style={{
                        marginTop: 30,
                        fontSize: 25,
                        textAlign: 'center',
                        color: 'black',
                        fontFamily: 'Metropolis-Bold',
                        width: '90%',
                      }}>
                      Selecciona tú ciudad
                    </Text>

                    <Text
                      style={{
                        marginTop: 12,
                        marginBottom: 12,
                        fontSize: 16,
                        textAlign: 'justify',
                        color: 'gray',
                        fontFamily: 'Metropolis-Medium',
                        paddingHorizontal: 24,
                        width: '90%',
                      }}>
                      Por favor selecciona la ciudad donde vives para mostrarte
                      los productos disponibles.
                    </Text>

                    <DropDownPicker
                      items={cities}
                      defaultValue={country}
                      containerStyle={{
                        height: 40,
                        width: '85%',
                        marginTop: '15%',
                      }}
                      style={{
                        backgroundColor: '#fafafa',
                        fontFamily: 'Metropolis-Medium',
                      }}
                      itemStyle={{
                        justifyContent: 'flex-start',
                        fontFamily: 'Metropolis-Medium',
                      }}
                      dropDownStyle={{backgroundColor: '#fafafa'}}
                      onChangeItem={(item) => {
                        setitemSelect(item);
                        setcountry(item.value);
                      }}
                    />

                    <TouchableOpacity
                      style={{
                        backgroundColor: '#ffa35d',
                        marginTop: '15%',

                        borderRadius: 30,

                        width: '55%',
                      }}
                      activeOpacity={0.9}
                      onPress={() => saveLocalizacion()}>
                      <Text
                        style={{
                          paddingVertical: 12,
                          textAlign: 'center',
                          borderRadius: 30,
                          color: 'white',

                          width: '100%',

                          fontFamily: 'Metropolis-Medium',
                        }}>
                        Aceptar
                      </Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
              </View>
            </SafeAreaView>
          </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
      );
    } else if (tipo === 0) {
      return (
        <AppIntroSlider
          renderItem={_renderItem}
          data={slides}
          onDone={_onDone}
          renderDoneButton={_renderDone}
          renderNextButton={_renderNextButton}
        />
      );
    }
  };

  return (
    <View style={{flex: 1}}>{tipoScreen(inicio)}</View>

    // <TouchableWithoutFeedback
    //   onPress={() => {
    //     Keyboard.dismiss();
    //   }}>
    //   <KeyboardAvoidingView
    //     behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
    //     style={{flex: 1}}>
    //     <SafeAreaView
    //       style={{
    //         borderRadius: 12,
    //       }}>
    //       <View style={{height: '100%', backgroundColor: 'white'}}>
    //         <View
    //           style={{
    //             width: '90%',
    //             alignItems: 'flex-start',
    //             marginTop: 8,
    //           }}>
    //           {/* <TouchableOpacity
    //             activeOpacity={0.5}
    //             style={{marginLeft: 8, padding: 5}}
    //             onPress={() => props.navigation.goBack()}>
    //             <Icon
    //               type="ionicon"
    //               name="arrow-back"
    //               color="white"
    //               size={porcentaje(9)}
    //               iconStyle={{color: 'black'}}
    //             />
    //           </TouchableOpacity> */}
    //         </View>
    //         <ScrollView
    //           style={{
    //             height: '100%',
    //             borderRadius: 20,
    //             backgroundColor: 'white',
    //           }}>
    //           <View
    //             style={{
    //               flex: 1,
    //               alignItems: 'center',
    //               alignContent: 'center',
    //               borderRadius: 30,
    //               paddingBottom: 20,
    //             }}>
    //             <Text
    //               style={{
    //                 marginTop: 30,
    //                 fontSize: 35,
    //                 textAlign: 'center',
    //                 color: '#ffa35d',
    //                 fontFamily: 'Metropolis-Bold',
    //                 width: '90%',
    //               }}>
    //               Bienvenido/a
    //             </Text>

    //             <Text
    //               style={{
    //                 marginTop: 30,
    //                 fontSize: 25,
    //                 textAlign: 'center',
    //                 color: 'black',
    //                 fontFamily: 'Metropolis-Bold',
    //                 width: '90%',
    //               }}>
    //               Selecciona tú ciudad
    //             </Text>

    //             <Text
    //               style={{
    //                 marginTop: 12,
    //                 marginBottom: 12,
    //                 fontSize: 16,
    //                 textAlign: 'justify',
    //                 color: 'gray',
    //                 fontFamily: 'Metropolis-Medium',
    //                 paddingHorizontal: 24,
    //                 width: '90%',
    //               }}>
    //               Por favor selecciona la ciudad donde vives para mostrarte los
    //               productos disponibles.
    //             </Text>

    //             <DropDownPicker
    //               items={cities}
    //               defaultValue={country}
    //               containerStyle={{
    //                 height: 40,
    //                 width: '85%',
    //                 marginTop: '15%',
    //               }}
    //               style={{
    //                 backgroundColor: '#fafafa',
    //                 fontFamily: 'Metropolis-Medium',
    //               }}
    //               itemStyle={{
    //                 justifyContent: 'flex-start',
    //                 fontFamily: 'Metropolis-Medium',
    //               }}
    //               dropDownStyle={{backgroundColor: '#fafafa'}}
    //               onChangeItem={(item) => {
    //                 setitemSelect(item);
    //                 setcountry(item.value);
    //               }}
    //             />

    //             <TouchableOpacity
    //               style={{
    //                 backgroundColor: '#ffa35d',
    //                 marginTop: '15%',

    //                 borderRadius: 30,

    //                 width: '55%',
    //               }}
    //               activeOpacity={0.9}
    //               onPress={() => saveLocalizacion()}>
    //               <Text
    //                 style={{
    //                   paddingVertical: 12,
    //                   textAlign: 'center',
    //                   borderRadius: 30,
    //                   color: 'white',

    //                   width: '100%',

    //                   fontFamily: 'Metropolis-Medium',
    //                 }}>
    //                 Aceptar
    //               </Text>
    //             </TouchableOpacity>
    //           </View>
    //         </ScrollView>
    //       </View>
    //     </SafeAreaView>
    //   </KeyboardAvoidingView>
    // </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    width: '90%',
    justifyContent: 'center',
  },
  container3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    width: '90%',
    justifyContent: 'center',
  },

  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
  },

  txtConsejo: {
    color: '#848687',
    marginTop: '8%',
    textAlign: 'center',
    marginHorizontal: 25,
    fontSize: 16,
    fontFamily: 'Metropolis-Medium',
    textAlign: 'justify',
    marginHorizontal: 35,
    lineHeight: 30,
  },

  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },

  container4: {
    flex: 1,
  },

  imagenEscudo: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },

  imageLogo: {
    width: '65%',
    height: '50%',
    resizeMode: 'contain',
  },
  imgLogo: {
    width: width / 2,
    height: width / 2,
    marginTop: '20%',
  },

  txtTitleConsejo: {
    color: '#0f8d8c',
    fontSize: 18,
    marginTop: '5%',
    fontFamily: 'Metropolis-Bold',
  },

  txtIngresa: {
    width: '40%',
    // fontFamily: 'DINPro-CondensedRegular',
    height: 40,
    borderColor: '#dc751b',
    borderWidth: 4,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#482181',
    marginTop: 25,
    fontSize: 15,
    padding: 5,
  },

  etxt: {
    height: 40,
    // fontFamily: 'din',
    width: '20%',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    marginLeft: 10,
    padding: 10,
  },
  txtGreenL: {
    // fontFamily: 'din',
    width: '100%',
    fontSize: 12,
    color: '#65B22E',
    textAlign: 'left',
    marginTop: 8,
    paddingTop: 8,
    paddingBottom: 8,
    // backgroundColor: 'red'
  },
  txtGreenR: {
    // fontFamily: 'din',
    width: '100%',
    fontSize: 12,
    color: '#65B22E',
    textAlign: 'right',
    marginTop: 8,

    paddingTop: 8,
    paddingBottom: 8,
  },
  image: {
    height: 150,
    width: '100%',
    marginTop: 32,
    marginBottom: 32,
  },
  inputDatos: {
    backgroundColor: 'rgba(0, 0, 0,0.0525)',
    borderRadius: 5,
    width: '80%',

    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 15,
    textAlign: 'center',
    color: 'gray',

    marginVertical: 4,
    marginTop: 16,

    // paddingVertical: height / 20
  },
});

export default Localidad;
