import React, {useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Linking,
  Alert,
  Image,
  Dimensions,
} from 'react-native';
import {ListItem, Icon} from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
var {height, width} = Dimensions.get('window');
export default function AccountOptions(props) {
  const menuOtions = [
    {
      title: 'Mis datos',
      icon: require('../../assets/img/ic_bro_cuenta_mis_datos.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => props.navigation.navigate('MisDatos'),
    },

    {
      title: 'Notificaciones',
      icon: require('../../assets/img/ic_bro_cuenta_notificaciones.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => props.navigation.navigate('Notificaciones'),
    },

    {
      title: 'Mis Pedidos',
      icon: require('../../assets/img/ic_bro_cuenta_mis_pedidos.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => props.navigation.navigate('MisPedidos'),
    },

    {
      title: 'Bro Coins',
      icon: require('../../assets/img/ic_bro_cuenta_mis_brocoins.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',

      onPress: () => props.navigation.navigate('MisRestaurants'),
    },

    {
      title: 'Ciudad',
      icon: require('../../assets/img/ic_bro_cuenta_mis_ciudad.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => props.navigation.navigate('Localidad'),
    },
    {
      title: 'Localización',
      icon: require('../../assets/img/ic_bro_cuenta_mis_localizacion.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => props.navigation.navigate('MapaUbicacionLocation'),
    },
    {
      title: 'Centro de Ayuda',
      icon: require('../../assets/img/ic_bro_cuenta_centro.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => openHelp('https://brodelivery593.com/'),
    },
    {
      title: 'Acerca de',
      icon: require('../../assets/img/ic_bro_cuenta_acerca.png'),
      iconNameRight: 'chevron-right',
      iconColorRight: 'gray',
      onPress: () => openHelp('https://brodelivery593.com/'),
    },
    {
      title: 'Cerrar sesión',
      icon: require('../../assets/img/ic_bro_cuenta_cerrar_session.png'),

      onPress: () => logout(),
    },
  ];

  const logout = async () => {
    // await AsyncStorage.removeItem('person');
    const itemPerson = {
      latitud: 0.0,
      longitud: 0.0,
      addres: '',
      name: '',
      lastname: '',
      email: '',
      ci: '',
      cellphone: '',
      pass: '',
      latitud: 0.0,
      longitud: 0.0,
      urlphoto: '',
    };
    AsyncStorage.mergeItem('person', JSON.stringify(itemPerson));
    props.navigation.reset({
      routes: [{name: 'MainMenu'}],
    });
  };

  const openHelp = async (url) => {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      // Opening the link with some app, if the URL scheme is "http" the web link should be opened
      // by some browser in the mobile
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  };

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  return (
    <View>
      {menuOtions.map((menu, index) => (
        <ListItem onPress={menu.onPress}>
          <Image
            source={menu.icon}
            style={{width: porcentaje(7), height: porcentaje(7)}}
          />

          <Text
            style={{
              width: '75%',

              fontFamily: 'Metropolis-Medium',
            }}>
            {menu.title}
          </Text>

          <Icon
            name={menu.iconNameRight}
            type={menu.iconType}
            color={menu.iconColorRight}
          />
        </ListItem>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  viewInfoUser: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    backgroundColor: '#f2f2f2',
    paddingBottom: 30,
    paddingTop: 30,
  },
  userinfoAvatar: {
    marginRight: 20,
  },
  textName: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'left',
    color: '#7c7c7c',
  },
  menuItem: {
    borderBottomWidth: 1,
    borderBottomColor: '#e3e3e3',
  },
  ratingImage: {
    height: 25,
    width: 25,
  },
});
