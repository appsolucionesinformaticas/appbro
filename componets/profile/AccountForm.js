import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  ScrollView,
  Image,
  View,
  Text,
  BackHandler,
} from 'react-native';
import {Divider, Input, Icon, Button} from 'react-native-elements';
import {SafeAreaView} from 'react-native-safe-area-context';
// import { Icon } from "react-native-vector-icons/Ionicons";
// import { Icon } from "react-native-vector-icons/FontAwesome5";
import Loading from '../load/Loading';
// import AsyncStorage from '@react-native-community/async-storage';
// import Modal from 'react-native-modal';

function AccountForm(props) {
  // const {toastRef, navigation} = props
  // console .log(props)
  const [gestor, setGestor] = useState([]);

  const [hidePassword, setHidePAssword] = useState(true);
  const [hideRepeatPasswor, setHideRepeatPasswotd] = useState(true);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [repeatPassword, setRepeatPasswordl] = useState('');
  const [userName, setUserName] = useState('');
  const [lastName, setLastName] = useState('');
  const [cellphone, setCellphone] = useState('');
  const [cedula, setCedula] = useState('');
  const [isVisibleLoading, setIsVisibleLoading] = useState(false);
  const [currentUser, setCurrentUser] = useState();
  const [enableSave, setEnableSave] = useState(false);
  const [editName, setEditName] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [infoModal, setInfoModal] = useState('');

  useEffect(() => {
    getInfo();
  }, []);

  const getInfo = async () => {
    setisVisibleLoading(true);
    await AsyncStorage.getItem('gestorData').then((data) => {
      if (data != null) {
        const gestordat = JSON.parse(data);
        setGestor(gestordat);
      }
    });
    setisVisibleLoading(false);
  };

  // Obj = new Registro();
  // const [values, setValues] = useState({
  //     email: "",
  //     password: "",
  //     repeatPassword: "",
  //     userName: "",
  //     lastName: "",
  //     cellphone: "",
  //     cedula: ""
  // });

  // const printValues = e => {
  //     e.preventDefault();
  //     console.log(form.email);
  // };

  // const updateField = () => {
  //     setState({
  //         ...form,
  //         [e.target.email]: e.target.value
  //     });
  // };

  // const handleChange = e => {
  //     const { name, value } = e.target;
  //     setState(prevState => ({
  //         ...prevState,
  //         [name]: value
  //     }));
  //     };

  // toggleModal = () => {
  //     // this.setState({isModalVisible: !this.state.isModalVisible});
  //     setIsModalVisible(!isModalVisible);
  // };

  const updateUser = async () => {
    setIsVisibleLoading(true);
    if (!email || !cellphone || !password) {
      setInfoModal(
        props.currentUser.firstName + ', todos los campos son obligatorios',
      );
      setIsModalVisible(true);
    } else {
      if (!validateEmail(email)) {
        setInfoModal({email} + ', es un correo invalido');
        setIsModalVisible(true);
      } else {
        fetch(global.linkServer + 'userws/updateUser', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            usr_name: props.currentUser.usr_name,
            usr_last_name: props.currentUser.usr_last_name,
            usr_email: email,
            usr_cellphone: cellphone,
            usr_url_image: props.currentUser.usr_url_image,
            usr_password: password,
            // usr_id: props.currentUser.usr_id
          }),
        })
          .then((response) => response.json())
          .then((data) => {
            console.log(data.status);
            if (data.status == 'exitoso') {
              setInfoModal(
                {firstName} + ', tus datos han sido guardados con exito!',
              );
              setIsModalVisible(true);
            } else {
              setInfoModal(
                {firstName} + ', tenemos problemas para actualizar tus datos',
              );
              setIsModalVisible(true);
            }
          })
          .catch((error) => console.log(error));
      }
    }
    setIsVisibleLoading(false);
  };

  function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    // const re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/;
    return re.test(String(email).toLocaleLowerCase());
  }

  function showToast(message) {
    Toast.show(message, {
      duration: Toast.durations.LONG,
      position: 40,
      shadow: true,
      animation: true,
      hideOnPress: true,
      delay: 0,
      backgroundColor: '#C61B45',
      shadowColor: '#4f0a1b',
      textColor: '#ffffff',
      opacity: 1.0,
      onShow: () => {
        // calls on toast\`s appear animation start
      },
      onShown: () => {
        // calls on toast\`s appear animation end.
      },
      onHide: () => {
        // calls on toast\`s hide animation start.
      },
      onHidden: () => {
        // calls on toast\`s hide animation end.
      },
    });
  }

  const changePass = async () => {
    console.log('cambios');
    setEditName(true);
    setUserName(props.currentUser.firstName);
    txtName.focus();
  };

  // const { navigation } = props;
  //
  return (
    <View style={{backgroundColor: 'red'}}>
      {gestor.gestor_data != null ? (
        <View style={styles.formContainer}>
          <View style={styles.row}>
            <View style={styles.inputWrap}>
              <Input
                value={gestor.gestor_data.ges_nombre}
                inputContainerStyle={styles.inputTxt}
                // text =" {currentUser.firstName}"
                containerStyle={styles.inputForm}
                inputStyle={styles.inputStyleNO}
                onChange={(e) => setUserName(e.nativeEvent.text)}
                editable={false}
                // leftIcon={
                //     <Icon
                //         type="material-community"
                //         name="account"
                //         iconStyle={styles.iconRight}
                //     />
                // }
              ></Input>
            </View>
            <View style={styles.inputWrap}>
              <Input
                value={props.currentUser.lastName}
                inputContainerStyle={styles.inputTxt}
                containerStyle={styles.inputForm}
                inputStyle={styles.inputStyleNO}
                onChange={(e) => setLastName(e.nativeEvent.text)}
                editable={false}
                // rightIcon={
                //     <Icon
                //         type="material-community"
                //         name="account"
                //         iconStyle={styles.iconRight}
                //     />
                // }
              ></Input>
            </View>
          </View>

          <Input
            ref={(e) => {
              txtName = e;
            }}
            // value = {props.currentUser.email}
            placeholder={props.currentUser.email}
            inputContainerStyle={styles.inputTxt}
            containerStyle={styles.inputForm}
            inputStyle={styles.inputStyle}
            onChange={(e) => setEmail(e.nativeEvent.text)}
            editable={false}
            // rightIcon={
            //     <Icon
            //         type="material"
            //         name="create"
            //         iconStyle={styles.iconRight}
            //         onPress={() => {
            //             txtName.focus();
            //         }}
            //         // onPress={() => changePass()}
            //     />
            // }
            leftIcon={
              <Icon
                type="material-community"
                name="email"
                iconStyle={styles.iconLeft}
              />
            }></Input>

          <Input
            editable={false}
            value={props.currentUser.cedula}
            inputContainerStyle={styles.inputTxt}
            keyboardType="number-pad"
            maxLength={10}
            containerStyle={styles.inputForm}
            inputStyle={styles.inputStyleNO}
            onChange={(e) => setCedula(e.nativeEvent.text)}
            // rightIcon={
            //     <Icon
            //     type="material"
            //     name="create"
            //         iconStyle={styles.iconRight}
            //         // onPress={() => setHidePAssword(!hidePassword)}
            //     />
            // }
            leftIcon={
              <Icon
                type="material"
                name="portrait"
                iconStyle={styles.iconLeft}
              />
            }></Input>

          <Input
            placeholder={props.currentUser.cellphone}
            inputContainerStyle={styles.inputTxt}
            keyboardType="number-pad"
            editable={false}
            maxLength={10}
            containerStyle={styles.inputForm}
            inputStyle={styles.inputStyle}
            onChange={(e) => setCellphone(e.nativeEvent.text)}
            // rightIcon={
            //     <Icon
            //     type="material"
            //     name="create"
            //         iconStyle={styles.iconRight}
            //         // onPress={() => setHidePAssword(!hidePassword)}
            //     />
            // }
            leftIcon={
              <Icon
                type="material-community"
                name="cellphone"
                iconStyle={styles.iconLeft}
              />
            }></Input>

          <Input
            underlineColorAndroid="rgba(0,0,0,0)"
            // value = {props.currentUser.password}
            placeholder={props.currentUser.password}
            password={true}
            editable={false}
            secureTextEntry={hidePassword}
            inputContainerStyle={styles.inputTxt}
            containerStyle={styles.inputForm}
            inputStyle={styles.inputStyle}
            onChange={(e) => setPassword(e.nativeEvent.text)}
            leftIcon={
              <Icon
                type="material-community"
                name={hidePassword ? 'eye-outline' : 'eye-off-outline'}
                iconStyle={styles.iconLeft}
                onPress={() => setHidePAssword(!hidePassword)}
              />
            }
            // rightIcon={
            //     <Icon
            //     type="material"
            //     name="create"
            //         iconStyle={styles.iconRight}
            //         // onPress={() => setHidePAssword(!hidePassword)}
            //     />
            // }
          ></Input>
        </View>
      ) : (
        <Loading text="Cargando datos..." isVisible={isVisibleLoading} />
      )}
    </View>
  );
}

export default AccountForm;

const styles = StyleSheet.create({
  inputTxt: {
    borderBottomWidth: 0,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
  },
  inputWrap: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  image: {
    height: 175,
    width: '100%',
    // marginBottom: 20
  },
  viewContainer: {
    marginLeft: 32,
    marginRight: 32,
    //
  },
  formContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 24,
    //
  },

  inputForm: {
    width: '100%',
    marginBottom: 20,
    flex: 1,
    backgroundColor: '#f9f7f7',
    borderWidth: 0,
    borderRadius: 5,
  },
  iconRight: {
    color: '#E6464B',
  },
  iconLeft: {
    color: '#E6464B',
    marginLeft: -16,
    marginRight: 8,
  },
  btnStyle: {
    backgroundColor: '#C61B45',
    shadowColor: '#4f0a1b',
    shadowRadius: 5,
    shadowOpacity: 0.6,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
  },
  btnContainer: {
    width: '95%',
    marginBottom: 16,
    marginTop: 16,
    paddingHorizontal: 32,
  },
  btnTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#ffffff', ////"#EEBECB"
  },
  inputStyle: {
    flex: 1,
    fontSize: 17,
    fontWeight: 'bold',
    paddingVertical: 0,
  },
  inputStyleNO: {
    flex: 1,
    fontSize: 17,
    fontWeight: 'bold',
    paddingVertical: 0,
    color: '#aeaeae',
  },
  textDesc: {
    fontWeight: 'normal',
    fontSize: 14,
    textAlign: 'justify',
    paddingHorizontal: 32,
    color: '#4F0A1B',
  },
  image: {
    height: 85,
    width: 85,
    // marginBottom: 16
  },
  alertContent: {
    backgroundColor: '#f2f2f2',
    width: '90%',
    borderRadius: 15,
    borderWidth: 1,
    // borderColor: "#C61B45",
    alignItems: 'center',
    // height: "30%"
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
    // marginVertical: 16,
    marginBottom: 16,
    textAlign: 'center',
    color: '#C61B45',
  },
});
