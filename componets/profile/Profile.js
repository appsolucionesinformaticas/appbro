import React, {Component, useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
} from 'react-native';
import {Avatar} from 'react-native-elements';
import styles from './profilestyle';
import AsyncStorage from '@react-native-community/async-storage';
import AccountOptions from './AccountOptions';
import Loading from '../load/Loading';
import storage from '@react-native-firebase/storage';
import {Icon, Button} from 'react-native-elements';
import Modal from 'react-native-modal';
// import ImagePicker from 'react-native-image-picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {ScrollView} from 'react-native-gesture-handler';
import LoginNew from '../../componets/LoginNew';
import Login from '../../componets/Login';

var {height, width} = Dimensions.get('window');
function Profile(props) {
  const [isVisibleLoading, setisVisibleLoading] = useState(false);
  const [isModalMenuOk, setisModalMenuOk] = useState(false);
  const [person, setperson] = useState('');
  const [nameError, setnameError] = useState('');

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };
  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
    });
  }, []);

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);
          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
          }, 100);
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  const logout = async () => {
    AsyncStorage.removeItem('gestorData');
  };

  const uploadImage = async (uri, uid) => {
    const response = await fetch(uri);
    const blob = await response.blob();
    const ref = storage().ref().child('avatar/').child(uid);

    return ref.put(blob);
  };

  const chooseFile = () => {
    var options = {
      title: 'Selecionar nueva imagen',
      quality: 0.6,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'images',
        quality: 0.6,
        maxWidth: 500,
        maxHeight: 500,
      },
    };

    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
      } else if (response.error) {
      } else if (response.customButton) {
        alert(response.customButton);
      } else {
        let source = response;

        uploadImage(source.uri, person.ci)
          .then((data) => {
            updatePhotUrl(person.ci);
          })
          .catch(() => {});
      }
    });
  };

  const updatePhotUrl = (uid) => {
    setisVisibleLoading(true);
    const ref = storage().ref().child('avatar/').child(uid);

    ref
      .getDownloadURL()
      .then(async (result) => {
        openUrlImage(result);
      })
      .catch(() => {
        setisVisibleLoading(false);
        console.log('Error al recuperar la foto');
      });

    const openUrlImage = (url) => {
      fetch(global.linkServer + 'wsperson/UpdatePerson', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          code: '',
          per_name: person.name,
          per_last_name: person.lastname,
          per_ci: person.ci,
          per_cellphone: person.cellphone,
          per_email: person.email,
          per_password: person.pass,
          per_url_image: url,
          street: person.addres,
          lon: person.latitud,
          lat: person.longitud,
        }),
      })
        .then((response) => response.json())
        .then((data) => {
          if (data == true) {
            const itemPerson = {
              urlphoto: url,
            };

            try {
              AsyncStorage.mergeItem('person', JSON.stringify(itemPerson));
              getInfo();
              setisVisibleLoading(false);
            } catch (error) {
              alert('Hubo un error Intenalo de nuevo');
              setisVisibleLoading(false);
            }
          } else {
            alert('Hubo un error Intenalo de nuevo');
            setisVisibleLoading(false);
          }
        })
        .catch((error) => {
          setisVisibleLoading(false);
          console.log(error);
        });
    };
  };

  return person.ci != '' ? (
    <View style={{height: '100%'}}>
      <View style={styles.viewInfoUser}>
        <Avatar
          rounded
          size="large"
          showEditButton
          onPress={() => chooseFile()}
          onEditPress={() => chooseFile()}
          containerStyle={styles.userinfoAvatar}
          source={{
            uri:
              person.urlphoto != ''
                ? person.urlphoto
                : 'https://firebasestorage.googleapis.com/v0/b/brodelivery-579eb.appspot.com/o/banners%2FApp%20Store.png?alt=media&token=c3f56099-4d7b-47a3-b845-31faa4873e43',
          }}></Avatar>
        <View style={{width: '70%'}}>
          <Text style={styles.textName}>
            {person.name} {person.lastname}
            {/* {gestor.gestor_data.ges_nombre}{' '}
                {gestor.gestor_data.ges_apellido} */}
          </Text>
          {/* <Text>{gestor.gestor_data.ges_email} </Text>
              <Text>{gestor.gestor_data.ges_celular} </Text> */}
          <Text style={{fontFamily: 'Metropolis-Medium', color: 'gray'}}>
            {person.email}
          </Text>
          <Text style={{fontFamily: 'Metropolis-Medium', color: 'gray'}}>
            {person.cellphone}
          </Text>

          <Text
            style={{
              fontFamily: 'Metropolis-Medium',
              color: 'gray',
              width: '90%',
            }}>
            {person.addres}
          </Text>
        </View>
      </View>
      <ScrollView>
        <AccountOptions
          navigation={props.navigation}
          setisModalMenuOk={setisModalMenuOk}></AccountOptions>
      </ScrollView>

      <Modal isVisible={isModalMenuOk}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() => setisModalMenuOk(!isModalMenuOk)}></TouchableOpacity>

          <Icon
            type="font-awesome-5"
            name="piggy-bank"
            color="white"
            size={porcentaje(25)}
            iconStyle={{color: '#0f8d8c', marginTop: 8}}
          />

          <Text style={styles.txtDialogTitle}>
            Bro Coins Próximamente en BroApp
          </Text>

          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Aceptar"
              onPress={() => setisModalMenuOk(false)}
            />
          </View>
        </View>
      </Modal>
    </View>
  ) : (
    <View
      style={{
        justifyContent: 'center',
        alignContent: 'center',
        alignItems: 'center',
        height: '100%',
      }}>
      <Text
        style={{
          fontFamily: 'Metropolis-Medium',
          color: 'gray',
          fontSize: 22,
          marginBottom: 20,
        }}>
        Bienvenido a Bro
      </Text>
      <Text
        style={{
          fontFamily: 'Metropolis-Medium',
          color: 'gray',
          fontSize: 15,
          paddingHorizontal: 32,
          textAlign: 'center',
        }}>
        Inicia sesión o registrate en Bro para acceder a todas las
        funcionalidades
      </Text>
      <Button
        buttonStyle={styles.btnStyleDialogClose}
        containerStyle={styles.btnContainerDialog}
        titleStyle={styles.btnTitleDialog}
        title="Iniciar Bro"
        onPress={() => props.navigation.navigate('LoginNew')}
      />
    </View>
  );
  // <LoginNew navigation={props.navigation}></LoginNew>

  // return (
  //   <SafeAreaView>
  //     {person.ci = "" ?
  //       <View style={{ height: '100%' }}>
  //         <View style={styles.viewInfoUser}>
  //           <Avatar
  //             rounded
  //             size="large"
  //             showEditButton
  //             onPress={() => chooseFile()}
  //             onEditPress={() => chooseFile()}
  //             containerStyle={styles.userinfoAvatar}
  //             source={{
  //               uri:
  //                 person.urlphoto != ''
  //                   ? person.urlphoto
  //                   : 'https://firebasestorage.googleapis.com/v0/b/brodelivery-579eb.appspot.com/o/banners%2FApp%20Store.png?alt=media&token=c3f56099-4d7b-47a3-b845-31faa4873e43',
  //             }}></Avatar>
  //           <View style={{ width: '70%' }}>
  //             <Text style={styles.textName}>
  //               {person.name} {person.lastname}
  //               {/* {gestor.gestor_data.ges_nombre}{' '}
  //               {gestor.gestor_data.ges_apellido} */}
  //             </Text>
  //             {/* <Text>{gestor.gestor_data.ges_email} </Text>
  //             <Text>{gestor.gestor_data.ges_celular} </Text> */}
  //             <Text style={{ fontFamily: 'Metropolis-Medium', color: 'gray' }}>
  //               {person.email}
  //             </Text>
  //             <Text style={{ fontFamily: 'Metropolis-Medium', color: 'gray' }}>
  //               {person.cellphone}
  //             </Text>

  //             <Text
  //               style={{
  //                 fontFamily: 'Metropolis-Medium',
  //                 color: 'gray',
  //                 width: '90%',
  //               }}>
  //               {person.addres}
  //             </Text>
  //           </View>
  //         </View>
  //         <ScrollView>
  //           <AccountOptions
  //             navigation={props.navigation}
  //             setisModalMenuOk={setisModalMenuOk}></AccountOptions>
  //         </ScrollView>
  //       </View>
  //       :
  //       // <Loading text="Cargando datos..." isVisible={isVisibleLoading} />
  //       <View>
  //         <Text>Bienvenido, registrate o inicia sesión para acceder a todas las funcionalidades de BroDelivery</Text>
  //         {/* <LoginNew></LoginNew> */}
  //       </View>
  //       // <Login></Login>
  //     }

  //     <Modal isVisible={isModalMenuOk}>
  //       <View
  //         style={{
  //           // flex: 1,
  //           alignSelf: 'center',
  //           width: width - 32,
  //           // height: 200,
  //           backgroundColor: 'white',
  //           borderRadius: 20,
  //           paddingBottom: 20,
  //         }}>
  //         <TouchableOpacity
  //           style={{
  //             alignSelf: 'flex-end',
  //           }}
  //           activeOpacity={0.2}
  //           onPress={() => setisModalMenuOk(!isModalMenuOk)}></TouchableOpacity>

  //         <Icon
  //           type="font-awesome-5"
  //           name="piggy-bank"
  //           color="white"
  //           size={porcentaje(25)}
  //           iconStyle={{ color: '#0f8d8c', marginTop: 8 }}
  //         />

  //         <Text style={styles.txtDialogTitle}>
  //           Bro Coins Próximamente en BroApp
  //         </Text>

  //         <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
  //           <Button
  //             buttonStyle={styles.btnStyleDialogClose}
  //             containerStyle={styles.btnContainerDialog}
  //             titleStyle={styles.btnTitleDialog}
  //             title="Aceptar"
  //             onPress={() => setisModalMenuOk(false)}
  //           />
  //         </View>
  //       </View>
  //     </Modal>
  //     <Loading text="Cargando datos..." isVisible={isVisibleLoading} />
  //   </SafeAreaView>
  // );
}

export default Profile;
