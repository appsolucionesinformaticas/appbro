const React = require('react-native');

import {Dimensions, StyleSheet, StatusBar} from 'react-native';

var {height, width} = Dimensions.get('window');

export default {
  viewInfoUser: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row',
    backgroundColor: '#ECECEC',
    paddingBottom: 30,
    paddingTop: 8,
    width: '100%',
  },
  userinfoAvatar: {
    marginRight: 20,
  },
  textName: {
    fontFamily: 'Metropolis-Bold',
    fontSize: 18,
    textAlign: 'left',
    color: 'black',
  },
  btnStyleDialogClose: {
    backgroundColor: '#0f8d8c',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialogClose2: {
    backgroundColor: '#ffa35d',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialog: {
    backgroundColor: 'rgba(238, 130, 79,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },
  btnContainerDialog: {
    width: '40%',
    alignSelf: 'center',

    // position: 'absolute',
    // bottom: 16,
    // paddingBottom: 32,
  },
  btnTitleDialog: {
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Metropolis-Medium',
    color: '#ffffff',
  },
  txtDialogTitle: {
    // backgroundColor: 'red',
    width: width - 32,
    // height: '100%',
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontFamily: 'Metropolis-Bold',
    marginTop: 15,
    // position: 'absolute',
    // bottom: 0,
  },
};
