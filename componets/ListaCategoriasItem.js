import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  Image,
  ScrollView,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';
import {Badge, Icon} from 'react-native-elements';
import AnimatedHeader from 'react-native-animated-header';
import {Card} from 'react-native-shadow-cards';
import AsyncStorage from '@react-native-community/async-storage';

var {height, width} = Dimensions.get('window');
function ListaCategoriasItem(props) {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [cantidad, setcantidad] = useState(0);
  const [ltsCategorias, setltsCategorias] = useState([]);
  const [totalCart, settotalCart] = useState(0);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      AsyncStorage.getItem('cart')
        .then((cart) => {
          if (cart !== null) {
            const cartfood = JSON.parse(cart);

            var number = cartfood.length;
            settotalCart(number);
          }
        })
        .catch((err) => {
          alert(err);
        });
    });

    listEmpresas();
  }, []);

  const listEmpresas = () => {
    fetch(
      global.linkServer +
        'wscompany/ltsCompanies?idCat=' +
        props.route.params.categoria.item.cat_id,
    )
      .then((response) => response.json())
      .then((data) => {
        setcantidad(data.length);
        setltsCategorias(data);
      })
      .catch((error) => console.log(error));
  };

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };
  const _renderItemClient = (item) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          width: '100%',
          marginTop: 4,
          marginBottom: 4,
          overflow: 'hidden',
        }}>
        <TouchableOpacity
          activeOpacity={0.1}
          style={{alignItems: 'center', alignContent: 'center', width: '90%'}}
          onPress={() =>
            props.navigation.navigate('ListaProductosEmpresa', {empresa: item})
          }>
          <Card
            style={{
              flexDirection: 'row',
              padding: 10,
              paddingTop: 15,
              marginTop: 2,
              marginBottom: 10,
              padding: 10,
              margin: 10,
              elevation: 10,
              cornerRadius: 12,
            }}>
            <View
              style={{
                width: '40%',

                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={styles.imgCategoriaEmpresas}>
                <FastImage
                  style={styles.imgCategoriaEmpresas}
                  source={{
                    uri: item.item.com_url_logo,
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </View>
            </View>

            <View
              style={{
                width: '60%',

                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  alignItems: 'center',
                  width: '100%',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 18,
                    fontFamily: 'Metropolis-Bold',
                  }}>
                  {item.item.com_name}
                </Text>
                <Icon
                  type="material"
                  name="link"
                  color="white"
                  size={porcentaje(7)}
                  iconStyle={{color: '#BDBDBD'}}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 8,
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Icon
                  type="ionicon"
                  name="location"
                  color="white"
                  size={porcentaje(4)}
                  iconStyle={{color: '#BDBDBD'}}
                />
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 12,

                    fontFamily: 'Metropolis-Medium',
                  }}>
                  {item.item.com_address}
                </Text>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  backgroundColor: '#0f8d8c',
                  borderRadius: 12,
                  marginTop: 8,
                }}>
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 12,
                    color: 'white',
                    padding: 5,
                    paddingHorizontal: 12,
                    textAlign: 'center',
                    fontFamily: 'Metropolis-Bold',
                  }}>
                  Hacer Pedido
                </Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
    );
  };

  const itemCarrito = (numberProductos) => {
    if (numberProductos <= 0) {
      return (
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => props.navigation.navigate('Mi orden')}>
          <Icon
            type="ionicon"
            name="ios-cart-outline"
            color="white"
            size={porcentaje(8)}
            iconStyle={{color: 'white', marginRight: 25}}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <View>
          {/* <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.reset({index: 0,routes: [{ name: 'MainMenu' }]})}></TouchableOpacity> */}
          <TouchableOpacity
            activeOpacity={0.5}
            style={{padding: 5}}
            onPress={() => props.navigation.navigate('Mi orden')}>
            <Icon
              type="ionicon"
              name="ios-cart-outline"
              color="white"
              size={porcentaje(8)}
              iconStyle={{color: 'white', marginRight: 25}}
            />
            <Badge
              value={numberProductos}
              status="error"
              containerStyle={{
                position: 'absolute',
                top: -4,
                right: 0,
                marginRight: 25,
              }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        width: '100%',
      }}>
      <AnimatedHeader
        style={{flex: 1}}
        title={props.route.params.categoria.item.cat_name}
        renderLeft={() => (
          <TouchableOpacity
            activeOpacity={0.5}
            style={{marginLeft: 8, padding: 5}}
            onPress={() => props.navigation.goBack()}>
            <Icon
              type="ionicon"
              name="arrow-back"
              color="white"
              size={porcentaje(8)}
              iconStyle={{color: 'white'}}
            />
          </TouchableOpacity>
        )}
        renderRight={() => itemCarrito(totalCart)}
        backStyle={{fontWeight: 'bold', color: 'white', fontSize: 14}}
        backTextStyle={{fontSize: 14, color: 'white'}}
        titleStyle={{
          fontSize: 25,
          left: 10,
          bottom: 5,
          color: 'white',
          right: 50,
          fontWeight: 'bold',
        }}
        imageSource={{
          uri: props.route.params.categoria.item.cat_txt_2,
        }}
        toolbarColor="#0f8d8c"
        disabled={false}>
        <ScrollView>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              alignContent: 'center',
            }}>
            <Text
              style={{
                width: '90%',
                marginTop: 8,
                marginBottom: 8,
                fontSize: 20,
                fontFamily: 'Metropolis-Bold',
              }}>
              {cantidad} Lugares
            </Text>
            <FlatList
              data={ltsCategorias}
              style={{
                width: '100%',
                alignContent: 'center',
              }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={_renderItemClient}
              refreshing={isRefreshing}
            />
          </View>
        </ScrollView>
      </AnimatedHeader>
    </SafeAreaView>
  );
}

export default ListaCategoriasItem;
