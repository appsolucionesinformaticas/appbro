import React, {Component, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ImageBackground,
  SafeAreaView,
} from 'react-native';
var {height, width} = Dimensions.get('window');
import styles from './registro/registrostyle';
function Login(props) {
  return (
    <ImageBackground
      source={require('../images/ic_bro_fondo_app.png')}
      style={{
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView
        style={{
          flex: 1,
          alignItems: 'center',
          alignContent: 'center',
        }}>
        <Text onPress={() => props.navigation.navigate('MainMenu')}>
          {' '}
          INGRESAR{' '}
        </Text>
        <Text
          style={{marginTop: 100}}
          onPress={() => props.navigation.navigate('RegistroMain')}>
          {' '}
          Registrarse{' '}
        </Text>

        <Text
          style={{marginTop: 100}}
          onPress={() => props.navigation.navigate('LoginNew')}>
          {' '}
          Carlos{' '}
        </Text>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default Login;
