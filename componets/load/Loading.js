import React from 'react';
import {StyleSheet, View, Text, ActivityIndicator} from 'react-native';
import {Overlay} from 'react-native-elements';

export default function Loading(props) {
  const {isVisible, text} = props;
  return (
    <Overlay
      isVisible={isVisible}
      windowBackgroundColor="rgba(241,241,241,.7)"
      overlayBackgroundColor="transparent"
      overlayStyle={styles.overlay}
      close>
      <View style={styles.view}>
        <ActivityIndicator size="large" color="#0f8d8c" />
        {text && <Text style={styles.text}>{text}</Text>}
      </View>
    </Overlay>
  );
}

const styles = StyleSheet.create({
  overlay: {
    height: 100,
    width: 200,
    backgroundColor: '#fff',
    borderColor: '#002c5f',

    borderRadius: 10,
  },
  view: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#0f8d8c',
    textTransform: 'uppercase',
    marginTop: 10,
    fontFamily: 'Metropolis-Bold',
  },
});
