import React, {Component, useState, useEffect} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';
import AsyncStorage from '@react-native-community/async-storage';
import {Icon} from 'react-native-elements';

var {height, width} = Dimensions.get('window');
function MisRestaurants(props) {
  const [person, setperson] = useState('');
  const [total, settotal] = useState(0);
  const [pasada, setpasada] = useState(0);
  const [actual, setactual] = useState(0);
  const [data, setdata] = useState([]);
  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
    });
  }, []);

  const getMyRestaurant = (ci) => {
    fetch(global.linkServer + 'wspay/getDetailBrocoin?ci=' + ci)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setdata(data.lista);
        settotal(data.val_total);
        setpasada(data.val_last_week);
        setactual(data.val_week);
        // setdataSection(data.ltsCategoryKit);
      })
      .catch((error) => console.log(error));
  };

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);

          getMyRestaurant(personData.ci);
          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
          }, 100);
        }
      })
      .catch((err) => {
        alert(err);
      });
  };
  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };
  const _renderItem = (item) => {
    // console.log(item);
    return (
      <View style={{alignItems: 'center', marginTop: 8}}>
        <View
          style={{
            width: '95%',

            flexDirection: 'row',
            justifyContent: 'space-between',
            borderRadius: 8,
            borderWidth: 1,
            borderColor: 'black',
            paddingTop: 8,
            paddingBottom: 8,
          }}>
          <FastImage
            style={{
              borderRadius: porcentaje(20) / 2,
              width: porcentaje(20),
              height: porcentaje(20),
            }}
            source={{
              uri: item.item.com_url_image,
              priority: FastImage.priority.normal,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />

          <View style={{width: '55%', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                textAlign: 'center',
                color: '#0f8d8c',
                fontFamily: 'Metropolis-Medium',

                width: '100%',
              }}>
              {item.item.com_name}
            </Text>
          </View>

          <View style={{width: '20%', justifyContent: 'center'}}>
            <Text
              style={{
                fontSize: 18,
                textAlign: 'center',
                color: '#0f8d8c',
                fontFamily: 'Metropolis-Bold',

                width: '100%',
              }}>
              {(item.item.com_total * 1.0).toFixed(2)}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView style={{alignItems: 'center'}}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'flex-start',
          width: '100%',
        }}>
        <TouchableOpacity
          activeOpacity={0.5}
          style={{marginLeft: 8, padding: 5}}
          onPress={() => props.navigation.goBack()}>
          <Icon
            type="ionicon"
            name="arrow-back"
            color="#0f8d8c"
            size={30}
            iconStyle={{color: '#0f8d8c'}}
          />
        </TouchableOpacity>
      </View>

      <View
        style={{
          width: '95%',

          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <View
          style={{
            width: '30%',
            paddingTop: 8,
            paddingBottom: 8,
            borderWidth: 1,
            borderColor: 'black',
            borderRadius: 8,
          }}>
          <Text
            style={{
              fontSize: 22,
              textAlign: 'center',
              color: '#0f8d8c',
              fontFamily: 'Metropolis-Bold',
              width: '100%',
            }}>
            {total.toFixed(2)}
          </Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: 'center',
              color: 'black',
              fontFamily: 'Metropolis-Medium',

              width: '100%',
            }}>
            Comisión Total
          </Text>
        </View>
        <View
          style={{
            width: '30%',
            paddingTop: 8,
            paddingBottom: 8,
            borderWidth: 1,
            borderColor: 'black',
            borderRadius: 8,
          }}>
          <Text
            style={{
              fontSize: 22,
              textAlign: 'center',
              color: '#0f8d8c',
              fontFamily: 'Metropolis-Bold',
              width: '100%',
            }}>
            {pasada.toFixed(2)}
          </Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: 'center',
              color: 'black',
              fontFamily: 'Metropolis-Medium',

              width: '100%',
            }}>
            Sem. Pasada
          </Text>
        </View>
        <View
          style={{
            width: '30%',
            paddingTop: 8,
            paddingBottom: 8,
            borderWidth: 1,
            borderColor: 'black',
            borderRadius: 8,
          }}>
          <Text
            style={{
              fontSize: 22,
              textAlign: 'center',
              color: '#0f8d8c',
              fontFamily: 'Metropolis-Bold',
              width: '100%',
            }}>
            {actual.toFixed(2)}
          </Text>
          <Text
            style={{
              fontSize: 16,
              textAlign: 'center',
              color: 'black',
              fontFamily: 'Metropolis-Medium',

              width: '100%',
            }}>
            Esta Semana
          </Text>
        </View>
      </View>

      <FlatList
        data={data}
        keyExtractor={(item, index) => index.toString()}
        style={{width: '100%', height: '85%', marginTop: 8}}
        renderItem={_renderItem}
      />
    </SafeAreaView>
  );
}

export default MisRestaurants;
