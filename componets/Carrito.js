import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  StyleSheet,
  SafeAreaView,
  View,
} from 'react-native';
import {Badge, Icon, CheckBox, Button} from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {Card} from 'react-native-shadow-cards';
import AsyncStorage from '@react-native-community/async-storage';
import Loading from '../componets/load/Loading';
import Modal from 'react-native-modal';
import messaging from '@react-native-firebase/messaging';

var {height, width} = Dimensions.get('window');
function Carrito(props) {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [dataCart, setdataCart] = useState([]);
  const [total, settotal] = useState(0.0);
  const [valorEntrega, setvalorEntrega] = useState(0.0);
  const [person, setperson] = useState('');
  const [newallTotal, setnewallTotal] = useState(0.0);
  const [ltsCategorias, setltsCategorias] = useState([]);
  const [isVisibleLoading, setisVisibleLoading] = useState(false);
  const [isModalLocalizacion, setisModalLocalizacion] = useState(false);
  const [isModalMenuOk, setisModalMenuOk] = useState(false);
  const [nameError, setnameError] = useState('');
  const [token, settoken] = useState('');

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      setltsCategorias(props.route.params.empresa.listeProduct);
      sumaProductos(props.route.params.empresa.listeProduct);

      messaging()
        .getToken()
        .then((token) => {
          settoken(token);
          //return saveTokenToDatabase(token);
        });

      getInfo();
    });
  }, []);

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  const completeShop = () => {
    setisVisibleLoading(true);
    const listProductos = [];

    ltsCategorias.map((item, index) => {
      item.producto.map((item2, index2) =>
        item2.data.map((item3, index3) => {
          console.log(item);
          if (item3.default == true) {
            const itemList = {
              idProduct: item3.pro_var_id,
              quantity: item.quality,
              idKit: item.kit_id,
              titleKit: item.name,
            };

            listProductos.push(itemList);
          }
        }),
      );
    });

    // console.log(listProductos);

    fetch(global.linkServer + 'wsorder/saveOrder', {
      method: 'POST',
      // headers: {
      //   Accept: 'application/json',
      //   'Content-Type': 'application/json',
      // },
      body: JSON.stringify({
        com_id: props.route.params.empresa.code,
        ord_type: 'ahora',
        cellphonePerson: person.cellphone,
        ciPerson: person.ci,
        ltsDetail: listProductos,
        ord_addres: person.addres,
        ord_total_delivery: parseFloat(valorEntrega).toFixed(2),
        total: parseFloat(total).toFixed(2),
        ord_lat: person.latitud,
        ord_log: person.longitud,
        token: token,
        orderDate: 0,
        idHourSelected: 0,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data) {
          AsyncStorage.getItem('cart')
            .then((datacart) => {
              if (datacart !== null) {
                const cart = JSON.parse(datacart);

                if (cart.length > 0) {
                  var inde = -1;
                  cart.map((item, index) => {
                    if (item.code == props.route.params.empresa.code) {
                      inde = index;
                    }
                  });

                  cart.splice(inde, 1);
                  AsyncStorage.setItem('cart', JSON.stringify(cart));
                  setisVisibleLoading(false);
                  setnameError('Compra existosa');
                  setisModalMenuOk(true);

                  props.navigation.goBack();
                  // this.props.navigation.goBack();
                }

                // cart.push(itemcart)
                // AsyncStorage.setItem('cart',JSON.stringify(cart));
              } else {
                setnameError('Existe un error Intentalo de nuevo');
                setisModalMenuOk(true);

                setisVisibleLoading(false);
                // this.setState({spinner: false});
              }
            })
            .catch((error) => {
              setnameError('Existe un error Intentalo de nuevo');
              setisModalMenuOk(true);

              setisVisibleLoading(false);
              // this.setState({spinner: false});
            });
        } else {
          // this.setState({spinner: false});

          setnameError('Existe un error Intentalo de nuevo');
          setisModalMenuOk(true);

          setisVisibleLoading(false);
        }
      })
      .catch((error) => {
        setnameError('Existe un error Intentalo de nuevo');
        setisModalMenuOk(true);

        setisVisibleLoading(false);
      });
  };

  const getTarifa = (personData) => {
    fetch(global.linkServer + 'wslocation/getCity?id=' + personData.codeCity)
      .then((response) => response.json())
      .then((data) => {
        getDistanciaValor(personData, data);
      })
      .catch((error) => console.log(error));
  };
  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);

          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
            getTarifa(personData);
          }, 100);
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  const allTotal = (delivery) => {
    var preci = (
      parseFloat(delivery) +
      parseFloat(sumaProductos(props.route.params.empresa.listeProduct))
    ).toFixed(2);

    setnewallTotal(preci);
  };

  const getDistanciaValor = (personData, tarifa) => {
    if (personData.addres != '' && personData.latitud != 0.0) {
      fetch(
        'https://maps.googleapis.com/maps/api/distancematrix/json?origins=' +
          props.route.params.empresa.latitud +
          ',' +
          props.route.params.empresa.longitud +
          '&destinations=' +
          personData.latitud +
          ',' +
          personData.longitud +
          '&key=AIzaSyCb7GI6TekZoXdGgUQWXP59benBCf-1VEo',
      )
        .then((response) => response.json())
        .then((data) => {
          if (
            data.rows[0].elements[0].distance.value >
            tarifa.loc_distancia_min * 1000
          ) {
            var extraDistance =
              data.rows[0].elements[0].distance.value -
              tarifa.loc_distancia_min * 1000;

            var priceNew = (extraDistance * tarifa.loc_tarifa) / 1000;

            var realExtraPrice = Math.round(priceNew / 0.05) * 0.05;

            var newDelivery = (realExtraPrice + tarifa.loc_tarifa_min).toFixed(
              2,
            );

            allTotal(newDelivery);
            setvalorEntrega(newDelivery);
          } else {
            allTotal(tarifa.loc_tarifa_min);
            setvalorEntrega(tarifa.loc_tarifa_min);
          }
        })
        .catch((error) => {
          setnameError('Existe un error Intentalo de nuevo');
          setisModalMenuOk(true);
        });

      // this.completeShop(personData)
      //this.setState({statusFinishShop:true})
    } else {
      setnameError('Existe un error Intentalo de nuevo');
      setisModalMenuOk(true);
    }
  };

  const productos = (listproductos) => {
    var text = '';
    var tamano = listproductos.length - 1;
    listproductos.map((item, index) => {
      if (index == tamano) {
        text = text = text + ' ' + item.title;
      } else {
        text = text + ' ' + item.title + ' +';
      }
    });

    return text;
  };

  const sumaProductos = (listaProductos) => {
    var totalP = 0;

    listaProductos.map((item, index) => {
      totalP = parseFloat(totalP) + parseFloat(item.total);
    });

    settotal(totalP.toFixed(2));

    return totalP.toFixed(2);
  };

  const _renderItemClient = (item) => {
    return (
      <View
        style={{
          alignItems: 'center',
          alignContent: 'center',
          width: '100%',
          marginTop: 5,
          flexDirection: 'row',
        }}>
        <View
          style={{
            flexDirection: 'row',
            paddingHorizontal: 14,

            alignItems: 'center',
            width: '100%',
            marginBottom: 12,
          }}>
          <View style={{width: '90%'}}>
            <Text
              style={{
                color: 'black',
                fontSize: 15,
                fontFamily: 'Metropolis-Bold',
              }}>
              {item.name} x{item.quality}
            </Text>

            <Text
              style={{
                color: 'gray',
                fontSize: 12,
                fontFamily: 'Metropolis-Medium',
              }}>
              {productos(item.producto)}
            </Text>

            <Text
              style={{
                color: 'gray',
                fontSize: 12,
                fontFamily: 'Metropolis-Medium',
              }}>
              $ {(item.total * 1.0).toFixed(2)}
            </Text>
          </View>
        </View>
      </View>
    );
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
      }}>
      <TouchableOpacity
        style={{width: width - 20, marginTop: 10}}
        onPress={() => props.navigation.goBack()}>
        <FastImage
          style={{height: 30, width: 30}}
          resizeMode="contain"
          source={require('../assets/img/ic_bro_icon_back.png')}
        />
      </TouchableOpacity>
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <ScrollView style={{marginTop: 10, flex: 1}}>
          <View
            style={{
              alignItems: 'center',
              alignContent: 'center',
              width: '100%',
            }}>
            <Card
              style={{
                paddingTop: 15,
                marginTop: 2,
                marginBottom: 10,

                margin: 10,
                elevation: 10,
                cornerRadius: 12,

                backgroundColor: '#ECECEC',
              }}>
              <Text style={styles.textTitle}>
                {props.route.params.empresa.name}
                {/* {this.props.route.params.productos.name} */}
              </Text>

              <View
                style={{
                  flexDirection: 'row',
                  paddingHorizontal: 14,

                  alignItems: 'center',
                  width: '90%',
                  marginBottom: 12,
                }}>
                <Icon
                  type="ionicon"
                  name="location"
                  color="white"
                  size={porcentaje(4)}
                  iconStyle={{color: '#BDBDBD'}}
                />
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 12,
                    fontFamily: 'Metropolis-Medium',
                  }}>
                  {props.route.params.empresa.direccion}
                </Text>
              </View>

              <View
                style={{
                  width: '100%',
                  backgroundColor: 'white',
                  alignItems: 'center',
                }}>
                <FlatList
                  //horizontal={true}
                  style={{width: '100%'}}
                  data={ltsCategorias}
                  renderItem={({item, index}) => _renderItemClient(item, index)}

                  // keyExtractor = {(item,index) => index.toString}
                />
              </View>
            </Card>
          </View>

          <View
            style={{
              width: '100%',
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',

                width: '90%',
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'black',

                  marginHorizontal: 5,
                  fontFamily: 'Metropolis-Medium',
                  marginTop: 1,
                }}>
                Subtotal
              </Text>

              <Text
                style={{
                  textAlign: 'right',
                  color: 'black',

                  marginHorizontal: 5,
                  fontFamily: 'Metropolis-Medium',
                  marginTop: 1,
                }}>
                $ {total}
              </Text>
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: 8,
                width: '90%',
              }}>
              <Text
                style={{
                  textAlign: 'left',
                  color: 'black',

                  marginHorizontal: 5,
                  fontFamily: 'Metropolis-Medium',
                  marginTop: 1,
                }}>
                Delivery
              </Text>

              <Text
                style={{
                  textAlign: 'right',
                  color: 'black',

                  marginHorizontal: 5,
                  fontFamily: 'Metropolis-Medium',
                  marginTop: 1,
                }}>
                $ {valorEntrega}
              </Text>
            </View>

            <View
              style={{
                alignItems: 'center',
                marginTop: 10,
                marginBottom: 15,
                width: '100%',
              }}>
              <TouchableOpacity
                style={{
                  borderRadius: 12,
                  backgroundColor: '#0f8d8c',
                  alignItems: 'center',
                  width: '85%',
                  marginLeft: 10,
                  marginTop: 10,
                  marginRight: 10,
                  alignContent: 'center',
                  paddingVertical: 8,
                }}
                activeOpacity={0.9}
                onPress={() => setisModalLocalizacion(true)}>
                <Text
                  style={{
                    color: 'white',

                    textAlign: 'center',
                    paddingVertical: 5,
                    fontFamily: 'Metropolis-Bold',
                    fontSize: 16,
                  }}>
                  Continuar $ {newallTotal}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>

      <Loading text="Enviado Compra..." isVisible={isVisibleLoading} />

      <Modal isVisible={isModalLocalizacion}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() =>
              setisModalMenuOk(!isModalLocalizacion)
            }></TouchableOpacity>
          {/* <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            // source={require('../../images/ic_bro_cuenta.png')}
            source={require('../assets/img/ic_bro_orden_act.png')}
            resizeMode={FastImage.resizeMode.contain}
          /> */}

          <Icon
            type="material"
            name="delivery-dining"
            color="white"
            size={porcentaje(30)}
            iconStyle={{color: '#0f8d8c', marginTop: 8}}
          />

          <Text style={styles.txtDialogTitle}>
            Bro tu pedido sera enviado a {person.addres}
          </Text>

          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <Button
              buttonStyle={styles.btnStyleDialogClose2}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Cambiar"
              onPress={() => {
                setisModalLocalizacion(false);
                props.navigation.navigate('MapaUbicacionLocation');
              }}
            />

            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Aceptar"
              onPress={() => {
                setisModalLocalizacion(false);
                completeShop();
              }}
            />
          </View>
        </View>
      </Modal>

      <Modal isVisible={isModalMenuOk}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() => setisModalMenuOk(!isModalMenuOk)}></TouchableOpacity>

          <Icon
            type="material"
            name="error-outline"
            color="white"
            size={porcentaje(35)}
            iconStyle={{color: '#0f8d8c', marginTop: 8}}
          />

          <Text style={styles.txtDialogTitle}>{nameError}</Text>

          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Aceptar"
              onPress={() => setisModalMenuOk(false)}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    //width:'90%',
    justifyContent: 'flex-end',
    marginRight: 10,
  },

  container4: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 20,

    marginHorizontal: 20,
    width: width - 40,
  },

  textOrangeLista: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    color: '#dc751b',
  },

  textFactura: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 + 20,
    textAlign: 'right',
    marginRight: 10,
    marginTop: 5,
    backgroundColor: 'red',
  },
  textFacturaNumbers: {
    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 - 65,
    textAlign: 'left',
    marginTop: 5,
  },

  textFacturaMorado: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 + 20,
    textAlign: 'right',
    color: '#482181',
    marginRight: 10,
    marginTop: 5,
  },
  textFacturaNumbersMorado: {
    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 - 65,
    textAlign: 'left',
    color: '#482181',
    marginTop: 5,
  },

  imagenEscudo: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 8,
  },

  textHorario: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 10,
    width: width / 2,
    textAlign: 'right',
    color: '#482181',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textHorario3: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 - 20,
    textAlign: 'right',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textHorario2: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 10,
    width: width / 2,
    textAlign: 'left',
    color: '#482181',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textHorario4: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 + 20,
    textAlign: 'left',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textTitle: {
    marginTop: 5,
    fontSize: 20,
    color: 'black',
    textAlign: 'left',
    width: '90%',
    paddingHorizontal: 15,

    fontFamily: 'Metropolis-Bold',
  },
  container2: {
    flexDirection: 'row',

    marginTop: 15,
    justifyContent: 'flex-start',
    marginHorizontal: 20,
    width: width,
  },
  imgLogo: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  imgBasurero: {
    width: 35,
    height: 35,
    marginLeft: 10,
    resizeMode: 'contain',
  },

  imgCircle: {
    width: 35,
    height: 35,
    marginLeft: 10,
    resizeMode: 'contain',
  },

  txtNombreCom: {
    fontWeight: 'bold',
    fontSize: 15,
    width: width / 2 + 20,
    marginLeft: 8,
  },

  txtDescripcion: {
    marginLeft: 2,
    fontSize: 12,
    width: width / 3,
  },

  imgRepresentacion: {
    width: width,
    height: width / 1.5,
  },

  pickerStyle: {
    width: width - 40,
    fontWeight: 'bold',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 5,

    color: '#482181',
    fontSize: 40,
  },

  modal: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  modal3: {
    height: 300,
    width: 300,
  },

  text: {
    color: 'black',
    fontSize: 22,
  },
  btn: {
    margin: 10,
    backgroundColor: '#3B5998',
    color: 'white',
    padding: 10,
  },

  btnStyleDialogClose: {
    backgroundColor: '#0f8d8c',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialogClose2: {
    backgroundColor: '#ffa35d',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialog: {
    backgroundColor: 'rgba(238, 130, 79,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },
  btnContainerDialog: {
    width: '40%',
    alignSelf: 'center',

    // position: 'absolute',
    // bottom: 16,
    // paddingBottom: 32,
  },
  btnTitleDialog: {
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Metropolis-Medium',
    color: '#ffffff',
  },
  txtDialogTitle: {
    // backgroundColor: 'red',
    width: width - 32,
    // height: '100%',
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontFamily: 'Metropolis-Bold',
    marginTop: 15,
    // position: 'absolute',
    // bottom: 0,
  },
});

export default Carrito;
