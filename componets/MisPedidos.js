import React, {useEffect, useState} from 'react';
import {Divider} from 'react-native-elements';

import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import Toast from 'react-native-simple-toast';
import styles from '../styles/clientstyle';
import {Avatar, Icon} from 'react-native-elements';
import moment from 'moment';
import Loading from '../componets/load/Loading';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import AsyncStorage from '@react-native-community/async-storage';
import messaging from '@react-native-firebase/messaging';

var {height, width} = Dimensions.get('window');
function MisPedidos(props) {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [search, setSearch] = useState('');
  const [isVisibleLoading, setisVisibleLoading] = useState(false);
  const sliupMenu = React.createRef();
  const [empresa, setempresa] = useState();

  const [lstPedidos, setlstPedidos] = useState([]);

  const porcentajeHeigt = (por) => {
    return (height * por) / 100;
  };

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((data) => {
        if (data !== null) {
          const per = JSON.parse(data);
          //   setempresa(empresadata);

          console.log(per.ci);
          listMisPedidos(per.ci);
        } else {
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
    });

    const unsubscribeMesa = messaging().onMessage(async (remoteMessage) => {
      getInfo();
    });

    return unsubscribe, unsubscribeMesa;
  }, []);

  const dateP = (time) => {
    return 'AHORA ' + moment(new Date(time)).format('DD/MM/YYYY hh:mm');
  };

  const call = (cellphone) => {
    console.log(cellphone);
    RNImmediatePhoneCall.immediatePhoneCall(cellphone);
  };

  const loadDriver = (item) => {
    setisVisibleLoading(true);

    fetch(
      global.linkServer + 'wsorder/changePedirDriver?idOrder=' + item.ord_code,
    )
      .then((response) => response.json())
      .then((data) => {
        setisVisibleLoading(false);

        if (data) {
          getInfo();
        } else {
          Toast.show('Intentalo de Nuevo');
        }
      })
      .catch((error) => {
        setisVisibleLoading(false);
      });
  };

  const loadEntregadoCamino = (item) => {
    setisVisibleLoading(true);

    fetch(global.linkServer + 'wsorder/changeEnCamino?idOrder=' + item.ord_code)
      .then((response) => response.json())
      .then((data) => {
        setisVisibleLoading(false);

        if (data) {
          getInfo();
        } else {
          setisVisibleLoading(false);
          Toast.show('Ocurrio un errro intentalo de nuevo ...');
        }
      })
      .catch((error) => {
        Toast.show('Ocurrio un errro intentalo de nuevo ...');
        setisVisibleLoading(false);
      });
  };

  const listMisPedidos = (code) => {
    fetch(global.linkServer + 'wsorder/getLtsOrderRecord?ci=' + code)
      .then((response) => response.json())
      .then((data) => {
        console.log(data);
        setlstPedidos(data);
      })
      .catch((error) => console.log(error));
  };

  const cancelShop = (idShop) => {
    setisVisibleLoading(true);

    fetch(global.linkServer + 'wsorder/cancelOrder?idOrder=' + idShop.ord_code)
      .then((response) => response.json())
      .then((data) => {
        setisVisibleLoading(false);

        if (data) {
          getInfo();

          Toast.show('Se elimino tu compra con exito');
        } else {
          Toast.show('Ocurrio un Error Intentalo de Nuevo');
        }
      })
      .catch((error) => console.log(error));
  };

  const estadoEntrega = (estado, item) => {
    if (estado == 'preparando' || estado == 'pidiendo driver') {
      return (
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            alignItems: 'center',
          }}>
          <Text
            style={{
              marginTop: 10,
              fontSize: 15,
              textAlign: 'left',
              color: '#468985',
              marginHorizontal: 12,
              fontFamily: 'Metropolis-Medium',
            }}>
            Estado : Preparando tu Pedido
          </Text>
          <FastImage
            style={{width: porcentaje(30), height: porcentaje(30)}}
            resizeMode="contain"
            source={require('../assets/img/ic_bro_preparando.gif')}
          />
        </View>
      );
    } else if (estado == 'asignado' || estado == 'en camino') {
      return (
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            alignItems: 'center',
          }}>
          <Text
            style={{
              marginTop: 10,
              fontSize: 15,
              textAlign: 'left',
              color: '#468985',
              marginHorizontal: 12,
              fontFamily: 'Metropolis-Bold',
            }}>
            {item.driver.dri_name} {item.driver.dri_last_name}
          </Text>

          <Avatar
            rounded
            size="xlarge"
            showEditButton
            onEditPress={() => chooseFile()}
            containerStyle={{marginRight: 20, marginVertical: 15}}
            source={{
              uri:
                item.driver.dri_photo != ''
                  ? item.driver.dri_photo
                  : 'https://yt3.ggpht.com/a/AATXAJyW9JJaWBDehS2q1Qo7a6lUIIYP7T24vMdgKw=s900-c-k-c0xffffffff-no-rj-mo',
            }}></Avatar>

          <TouchableOpacity
            // onPress={() => call(item.driver.dri_phone)}
            style={{
              width: '60%',
              marginHorizontal: 15,
              backgroundColor: '#72C56A',
              paddingVertical: 8,
              borderRadius: 18,
              marginTop: 2,
              alignItems: 'center',
              marginBottom: 8,
            }}>
            <Text
              style={{
                fontSize: 15,
                textAlign: 'center',
                color: 'white',
                fontFamily: 'Metropolis-Bold',
                width: '95%',
              }}>
              Llamar a Conductor
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() =>
              props.navigation.navigate('FollowDriver', {
                idDriver: item.driver.dri_ci,
              })
            }
            style={{
              width: '60%',
              marginHorizontal: 15,
              backgroundColor: '#0f8d8c',
              paddingVertical: 8,
              borderRadius: 18,
              marginTop: 2,
              alignItems: 'center',
              marginBottom: 8,
            }}>
            <Text
              style={{
                fontSize: 15,
                textAlign: 'center',
                color: 'white',
                fontFamily: 'Metropolis-Bold',
                width: '95%',
              }}>
              Ver en el Mapa
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else if (estado == 'pidiendo') {
      return (
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            alignItems: 'center',
          }}>
          <Text
            style={{
              marginTop: 10,
              fontSize: 15,
              textAlign: 'left',
              color: '#468985',
              marginHorizontal: 12,
              fontFamily: 'Metropolis-Medium',
            }}>
            Estado : Pidiendo
          </Text>
          <FastImage
            style={{width: porcentaje(25), height: porcentaje(25)}}
            resizeMode="contain"
            source={require('../assets/img/ic_bro_pidiendo.gif')}
          />
          <TouchableOpacity
            onPress={() => cancelShop(item)}
            style={{
              width: '60%',
              marginHorizontal: 15,
              backgroundColor: '#0f8d8c',
              paddingVertical: 8,
              borderRadius: 18,
              marginTop: 2,
              alignItems: 'center',
              marginBottom: 8,
            }}>
            <Text
              style={{
                fontSize: 15,
                textAlign: 'center',
                color: 'white',
                fontFamily: 'Metropolis-Bold',
                width: '95%',
              }}>
              Cancelar Pedido
            </Text>
          </TouchableOpacity>
        </View>
      );
    }
  };

  const productos = (listproductos) => {
    var text = '';
    var tamano = listproductos.length - 1;
    listproductos.map((item, index) => {
      if (index == tamano) {
        text = text = text + ' ' + item.pro_name;
      } else {
        text = text + ' ' + item.pro_name + ' +';
      }
    });

    return text;
  };

  const sumaProductos = (listaProductos) => {
    var totalP = 0;

    listaProductos.map((item, index) => {
      totalP = parseFloat(totalP) + parseFloat(item.pro_price);
    });

    return totalP.toFixed(2);
  };

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  const _renderItemClient = (item) => {
    return (
      <View
        style={{
          elevation: 10,
          alignItems: 'center',
          justifyContent: 'center',
          width: '100%',
          marginTop: 15,

          borderRadius: 12,
          shadowRadius: 10,

          shadowOpacity: 0.3,
          shadowOffset: {
            height: 10,
          },
        }}>
        <View
          style={{
            alignItems: 'flex-start',
            alignContent: 'flex-start',
            width: '100%',

            backgroundColor: '#C6C5C2',
            borderTopLeftRadius: 20,
            borderTopRightRadius: 20,
            paddingBottom: 8,
          }}>
          <Text
            style={{
              marginTop: 10,
              fontSize: 18,
              textAlign: 'left',
              color: 'black',
              marginHorizontal: 12,
              fontFamily: 'Metropolis-Bold',
            }}>
            {dateP(item.item.ord_date)}
          </Text>

          <TouchableOpacity
            // onPress={() => call(item.item.com_cellphone)}
            style={{
              flexDirection: 'row',
              paddingHorizontal: 14,
              marginTop: 5,
              alignItems: 'center',
              width: '90%',
              marginBottom: 5,
            }}>
            <Icon
              type="material"
              name="add-call"
              color="white"
              size={porcentaje(8)}
              iconStyle={{color: '#468985'}}
            />
            <Text
              style={{
                color: 'black',
                fontSize: 18,
                fontFamily: 'Metropolis-Bold',
              }}>
              {item.item.com_name}
            </Text>
          </TouchableOpacity>

          <View
            style={{
              flexDirection: 'row',
              width: '95%',
            }}>
            <Icon
              size={15}
              type="ionicon"
              name="md-location"
              iconStyle={styles.iconUbicacion}
            />

            <Text
              style={{
                marginTop: 5,
                alignContent: 'center',
                fontSize: 15,
                color: 'gray',

                marginHorizontal: 15,
                textAlign: 'left',

                fontFamily: 'Metropolis-Medium',
              }}>
              {item.item.com_address}
            </Text>
          </View>
          <Text
            style={{
              marginTop: 10,
              fontSize: 15,
              textAlign: 'left',
              color: '#468985',
              marginHorizontal: 12,
              fontFamily: 'Metropolis-Medium',
            }}>
            Código : {item.item.ord_code_order}
          </Text>
        </View>

        <View style={{backgroundColor: 'white', width: '100%'}}>
          <FlatList
            data={item.item.ltsKits}
            style={{marginTop: 5, width: '100%'}}
            keyExtractor={(item, index) => index.toString()}
            renderItem={_renderItemPedido}
            refreshing={isRefreshing}
          />
        </View>

        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            paddingHorizontal: 10,
          }}>
          <Text
            style={{
              fontSize: 14,
              textAlign: 'left',
              color: 'gray',

              fontFamily: 'Metropolis-Medium',
              width: '100%',
            }}>
            Compra : {(item.item.ord_total * 1.0).toFixed(2)}
          </Text>

          <Text
            style={{
              fontSize: 14,
              textAlign: 'left',
              color: 'gray',

              fontFamily: 'Metropolis-Medium',
              width: '100%',
            }}>
            Delivery : {(item.item.ord_total_delivery * 1.0).toFixed(2)}
          </Text>

          <Text
            style={{
              fontSize: 20,
              textAlign: 'left',
              color: 'black',

              fontFamily: 'Metropolis-Bold',
              width: '100%',
            }}>
            Total :{' '}
            {(
              (item.item.ord_total_delivery + item.item.ord_total) *
              1.0
            ).toFixed(2)}
          </Text>
        </View>

        {/* {estadoEntrega(item.item.ord_status, item.item)} */}
        {/* andres */}
        {/* <View
          style={{
            alignContent: 'space-around',
            flexDirection: 'row',
            backgroundColor: 'white',
            width: '100%',
          }}>
          <View
            style={{
              width: '40%',
              marginHorizontal: 15,
              backgroundColor: '#E608D2',
              paddingVertical: 8,
              borderRadius: 18,
              marginTop: 10,
              alignItems: 'center',
              marginBottom: 8,
            }}>
            <Text
              onPress={() => props.navigation.navigate('PedidosPreparando')}
              style={{
                fontSize: 15,
                textAlign: 'center',
                color: 'white',
                fontFamily: 'Metropolis-Bold',
                width: '95%',
              }}>
              Rechazar
            </Text>
          </View>

          <TouchableOpacity
            onPress={() => loadShop(item.item)}
            style={{
              width: '40%',
              marginHorizontal: 15,
              backgroundColor: '#E608D2',
              paddingVertical: 8,
              borderRadius: 18,
              marginTop: 10,
              alignItems: 'center',
              marginBottom: 8,
            }}>
            <Text
              style={{
                fontSize: 15,
                textAlign: 'center',
                color: 'white',
                fontFamily: 'Metropolis-Bold',
                width: '95%',
              }}>
              Aceptar
            </Text>
          </TouchableOpacity>
        </View>
      
      
       */}
      </View>
    );
  };

  const _renderItemPedido = (item) => {
    return (
      <View
        style={{
          width: '100%',

          alignItems: 'center',
        }}>
        <Text
          style={{
            fontSize: 15,
            textAlign: 'left',
            color: 'black',
            width: '95%',
            fontFamily: 'Metropolis-Bold',
            marginVertical: 2,
          }}>
          {item.item.kit_name} x{item.item.ltsProducts[0].det_quantity}
        </Text>

        <Text
          style={{
            fontSize: 14,
            textAlign: 'left',
            color: 'gray',
            width: '95%',
            fontFamily: 'Metropolis-Medium',
          }}>
          {productos(item.item.ltsProducts)}
        </Text>

        <Text
          style={{
            fontSize: 14,
            textAlign: 'left',
            color: 'gray',
            width: '95%',
            fontFamily: 'Metropolis-Bold',
          }}>
          $ {sumaProductos(item.item.ltsProducts)}
        </Text>

        <Divider
          style={{
            backgroundColor: '#ECECEC',
            height: 1,
            width: '95%',
            marginVertical: 4,
          }}
        />
      </View>
    );
  };

  const updateSearch = (search) => {
    setSearch(search);
  };

  return (
    <SafeAreaView>
      <Loading
        text="Ejecutando Acción En Bro App ... "
        isVisible={isVisibleLoading}
      />

      <View
        style={{
          height: '100%',
          backgroundColor: 'white',
          alignContent: 'center',
          alignItems: 'center',
        }}>
        <View
          style={{
            width: '90%',
            alignItems: 'flex-start',
            marginTop: 8,
          }}>
          <TouchableOpacity
            activeOpacity={0.5}
            style={{marginLeft: 8, padding: 5}}
            onPress={() => props.navigation.navigate('MainMenu')}>
            <Icon
              type="ionicon"
              name="arrow-back"
              color="white"
              size={porcentaje(9)}
              iconStyle={{color: 'black'}}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,

            width: '100%',
            alignContent: 'center',
            alignItems: 'center',
            backgroundColor: 'white',
          }}>
          <View
            style={{
              width: '90%',

              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 5,
            }}>
            <Text
              style={{
                width: '100%',
                fontSize: 20,
                textAlign: 'center',
                color: 'black',
                fontFamily: 'Metropolis-Bold',
              }}>
              Mis Pedidos
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              alignContent: 'center',

              width: '100%',
            }}>
            <FlatList
              data={lstPedidos}
              showsVerticalScrollIndicator={false}
              style={{
                width: '90%',
                backgroundColor: 'white',
              }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={_renderItemClient}
              refreshing={isRefreshing}
            />
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default MisPedidos;
