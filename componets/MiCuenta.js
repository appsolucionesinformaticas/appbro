import React, { Component, useState, useEffect } from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';

var { height, width } = Dimensions.get('window');
function MiCuenta(props) {
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backJS);
    return () => BackHandler.removeEventListener('hardwareBackPress', backJS);
  }, []);

  const backJS = () => {
    props.navigation.goBack();
  };
  return (
    <ImageBackground
      source={require('../images/ic_bro_fondo_app.png')}
      style={{
        height: null,
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'flex-start',
            alignItems: 'flex-start',
            width: width - 40,
          }}>
          <TouchableOpacity onPress={() => backJS()}>
            <FastImage
              style={{ width: 30, height: 30, marginTop: 10 }}
              resizeMode="contain"
              source={require('../images/ic_bro_atras_celeste.png')}
            />
          </TouchableOpacity>
        </View>

        <FastImage
          style={{ width: width / 4, height: width / 4, marginTop: 10 }}
          resizeMode="contain"
          source={require('../images/ic_bro_cuenta_cele.png')}
        />

        <Text>Jhonny Pauta</Text>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default MiCuenta;
