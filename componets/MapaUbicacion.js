import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  PermissionsAndroid,
  Platform,
  ToastAndroid,
} from 'react-native';
import MapView, {
  PROVIDER_GOOGLE,
  AnimatedRegion,
  Animated,
} from 'react-native-maps';
var {height, width} = Dimensions.get('window');
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';

import FastImage from 'react-native-fast-image';

class MapaUbicacion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      latitude: -2.897342,
      longitude: -79.004189,
      latitudeDelta: 0.007,
      longitudeDelta: 0.007,

      isDraggingMap: false,

      namestreet: 'Ningun Sector',

      loading: false,
      updatesEnabled: false,
      region: {
        latitudeDelta: 0.007,
        longitudeDelta: 0.007,
        latitude: -2.897477,
        longitude: -78.99508,
      },
      region2: {
        latitudeDelta: 0.007,
        longitudeDelta: 0.007,
        latitude: -2.897477,
        longitude: -78.99508,
      },
      da: 'dfsdfsdf',
    };
  }

  componentDidMount() {
    Geocoder.init('AIzaSyAu7oPOKRK8IVtMJWLTlqxDnXf1VUAuQoY');

    this._getLocation();
  }

  onRegionChangeComplete = (region) => {
    console.log(region);

    // this.onGetDirectionLl(region.latitude,region.longitude)

    this.setState({
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta,
      longitudeDelta: region.longitudeDelta,
    });
  };

  onGetDirectionLl(latitude, longitude) {
    Geocoder.from(latitude, longitude)
      .then((json) => {
        var addressComponent = json.results[0].formatted_address;
        this.setState({namestreet: addressComponent});
      })
      .catch((error) => console.warn(error));
  }

  hasLocationPermission = async () => {
    if (
      Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)
    ) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Location permission denied by user.',
        ToastAndroid.LONG,
      );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Location permission revoked by user.',
        ToastAndroid.LONG,
      );
    }

    return false;
  };

  getInitialState() {
    return {
      region: new AnimatedRegion({
        latitude: -2.891447,
        longitude: -78.999071,
        latitudeDelta: 0.007,
        longitudeDelta: 0.007,
      }),
    };
  }

  _getLocation() {
    Geolocation.getCurrentPosition(
      (info) => {
        if (!this.hasLocationPermission()) return;

        this.onGetDirectionLl(info.coords.latitude, info.coords.longitude);

        this.setState({
          latitude: info.coords.latitude,
          longitude: info.coords.longitude,
        });
      },
      (error) => {},
    );
  }

  onMapBack() {
    try {
      const itemPerson = {
        latitud: this.state.latitude,
        longitud: this.state.longitude,
        addres: this.state.namestreet,
      };

      // AsyncStorage.mergeItem("person", JSON.stringify(itemPerson))

      this.props.navigation.goBack();
    } catch (error) {
      alert(error);
    }
  }

  render() {
    return (
      <View
        style={{
          marginTop: StatusBar.currentHeight,
          flex: 1,
          width: width,
          height: height,
        }}>
        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          // initialRegion  ={this.state.region}

          region={{
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            latitudeDelta: this.state.latitudeDelta,
            longitudeDelta: this.state.longitudeDelta,
          }}
          getCamera={this.onRegionChangeComplete}
          // onRegionChange={this.onRegionChangeComplete}
          // onRegionChangeComplete={this.onRegionChangeComplete}
        />

        <View style={styles.markerFixed}>
          <FastImage
            style={styles.marker}
            resizeMode="contain"
            source={require('../images/marker.png')}
          />
        </View>

        <View style={styles.positionFixes}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => this._getLocation()}>
            <FastImage
              style={styles.imgPosition}
              resizeMode="contain"
              source={require('../images/ic_bro_ubicacion_celeste.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.positionFixes2}>
          <TouchableOpacity
            activeOpacity={0.9}
            onPress={() => this.onMapBack()}>
            <FastImage
              style={styles.imgBack}
              resizeMode="contain"
              source={require('../images/ic_bro_atras.png')}
            />
          </TouchableOpacity>
        </View>

        <View style={styles.titleFixes}>
          <Text
            style={{
              marginHorizontal: 5,
              marginVertical: 6,
              color: 'white',
              fontSize: 12,
            }}>
            {this.state.namestreet}
          </Text>
        </View>

        <View style={styles.aceptesFixes}>
          <TouchableOpacity
            style={{
              borderRadius: 8,
              backgroundColor: '#00b2aa',
              alignItems: 'center',
              flexDirection: 'row',
              width: width / 2 - 30,
              marginLeft: 10,
              marginRight: 20,
            }}
            activeOpacity={0.9}
            onPress={() => this.onMapBack()}>
            <Text
              style={{
                color: 'white',
                width: width / 2 - 30,
                textAlign: 'center',
                paddingVertical: 5,
                fontSize: 14,
                marginVertical: 5,
              }}>
              Confirmar
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  markerFixed: {
    left: '49%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '49%',
  },
  positionFixes: {
    left: 20,
    right: 20,
    position: 'absolute',
    top: '2%',
    alignItems: 'flex-end',
  },

  positionFixes2: {
    left: 20,
    right: 20,
    resizeMode: 'contain',
    position: 'absolute',
    top: '3%',
    alignItems: 'flex-start',
  },

  titleFixes: {
    left: 20,
    right: 20,
    backgroundColor: '#00b2aa',
    position: 'absolute',
    top: '10%',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 12,
  },

  aceptesFixes: {
    left: 20,
    right: 20,
    position: 'absolute',
    top: '92%',
    alignItems: 'center',
  },

  marker: {
    height: 60,
    width: 60,
  },

  imgPosition: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },

  imgBack: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },

  footer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: 0,
    position: 'absolute',
    width: '100%',
  },
  region: {
    color: '#fff',
    lineHeight: 20,
    margin: 20,
  },
});

export default MapaUbicacion;
