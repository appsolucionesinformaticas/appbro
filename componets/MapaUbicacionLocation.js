import React, {useEffect, useState} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  StyleSheet,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import MapView, {
  PROVIDER_GOOGLE,
  AnimatedRegion,
  Animated,
} from 'react-native-maps';
import AsyncStorage from '@react-native-community/async-storage';
import Geocoder from 'react-native-geocoding';
import Geolocation from '@react-native-community/geolocation';

var {height, width} = Dimensions.get('window');
function MapaUbicacionLocation(props) {
  const [namestreet, setnamestreet] = useState('Sin Dirección');
  const [person, setperson] = useState('');
  const [region, setRegion] = useState({
    latitude: -2.87342,
    longitude: -79.004189,
    latitudeDelta: 0.007,
    longitudeDelta: 0.007,
  });

  useEffect(() => {
    Geocoder.init('AIzaSyAu7oPOKRK8IVtMJWLTlqxDnXf1VUAuQoY');

    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
    });
  }, []);

  const hasLocationPermission = async () => {
    if (
      Platform.OS === 'ios' ||
      (Platform.OS === 'android' && Platform.Version < 23)
    ) {
      return true;
    }

    const hasPermission = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (hasPermission) return true;

    const status = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (status === PermissionsAndroid.RESULTS.GRANTED) return true;

    if (status === PermissionsAndroid.RESULTS.DENIED) {
      ToastAndroid.show(
        'Location permission denied by user.',
        ToastAndroid.LONG,
      );
    } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
      ToastAndroid.show(
        'Location permission revoked by user.',
        ToastAndroid.LONG,
      );
    }

    return false;
  };
  const _getLocation = () => {
    Geolocation.getCurrentPosition(
      (info) => {
        if (!hasLocationPermission()) return;

        onGetDirectionLl(info.coords.latitude, info.coords.longitude);

        var interval = setInterval(() => {
          clearInterval(interval);
          setRegion((region) => {
            return {
              ...region,
              latitude: info.coords.latitude,
              longitude: info.coords.longitude,
            };
          });
        }, 100);
      },
      (error) => {
        console.log(error);
      },
    );
  };

  const onMapBack = () => {
    try {
      const itemPerson = {
        latitud: region.latitude,
        longitud: region.longitude,
        addres: namestreet,
      };

      AsyncStorage.mergeItem('person', JSON.stringify(itemPerson));

      props.navigation.goBack();
    } catch (error) {
      alert(error);
    }
  };

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);

          if (personData.latitud == 0) {
            setRegion((region) => {
              return {
                ...region,
                latitude: personData.latitudCiudad,
                longitude: personData.longitudCiudad,
              };
            });
          } else {
            setRegion((region) => {
              return {
                ...region,
                latitude: personData.latitud,
                longitude: personData.longitud,
              };
            });
          }

          _getLocation();
          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
          }, 100);
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  const onGetDirectionLl = (latitude, longitude) => {
    Geocoder.from(latitude, longitude)
      .then((json) => {
        var addressComponent = json.results[0].formatted_address;
        setnamestreet(addressComponent);
      })
      .catch((error) => console.warn(error));
  };

  return (
    <View style={{flex: 1, width: width, height: height}}>
      <MapView
        style={styles.map}
        region={region}
        onRegionChangeComplete={(region) => {
          onGetDirectionLl(region.latitude, region.longitude);
          setRegion(region);
        }}

        // setUsuario((usuario) => {
        //   return { ...usuario, us_nombre: val };
        // });

        // getCamera={this.onRegionChangeComplete}
        // onRegionChange={this.onRegionChangeComplete}
        // onRegionChangeComplete={this.onRegionChangeComplete}
      />

      <View style={styles.markerFixed}>
        <FastImage
          style={styles.marker}
          resizeMode="contain"
          source={require('../images/marker.png')}
        />
      </View>

      <View
        style={{
          position: 'absolute',
          top: '3%',
          width: '90%',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <TouchableOpacity
          style={styles.positionFixes2}
          activeOpacity={0.9}
          onPress={() => props.navigation.goBack()}>
          <FastImage
            style={styles.imgBack}
            resizeMode="contain"
            source={require('../images/ic_bro_atras.png')}
          />
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.positionFixes}
          onPress={() => _getLocation()}>
          <FastImage
            style={styles.imgPosition}
            resizeMode="contain"
            source={require('../images/ic_bro_ubicacion_celeste.png')}
          />
        </TouchableOpacity>
      </View>

      <View style={styles.titleFixes}>
        <Text
          style={{
            marginHorizontal: 5,
            marginVertical: 6,
            color: 'white',
            fontSize: 12,
            fontFamily: 'Metropolis-Medium',
          }}>
          {namestreet}
        </Text>
      </View>

      <View style={styles.aceptesFixes}>
        <TouchableOpacity
          style={{
            borderRadius: 8,
            backgroundColor: '#0f8d8c',
            alignItems: 'center',
            flexDirection: 'row',
            width: width / 2 - 30,
            marginLeft: 10,
            marginRight: 20,
          }}
          activeOpacity={0.9}
          onPress={() => onMapBack()}>
          <Text
            style={{
              color: 'white',
              width: width / 2 - 30,
              textAlign: 'center',
              paddingVertical: 5,
              fontSize: 14,
              marginVertical: 5,
              fontFamily: 'Metropolis-Medium',
            }}>
            Confirmar
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  map: {
    flex: 1,
  },
  markerFixed: {
    left: '49%',
    marginLeft: -24,
    marginTop: -48,
    position: 'absolute',
    top: '50%',
  },
  positionFixes: {
    left: 20,
    right: 20,

    top: '2%',
    alignItems: 'flex-end',
  },

  positionFixes2: {
    left: 20,
    right: 20,
    resizeMode: 'contain',

    top: '2%',
    alignItems: 'flex-start',
  },

  titleFixes: {
    left: 20,
    right: 20,
    backgroundColor: '#0f8d8c',
    position: 'absolute',
    top: '10%',
    alignItems: 'center',
    paddingVertical: 10,
    borderRadius: 12,
  },

  aceptesFixes: {
    left: 20,
    right: 20,
    position: 'absolute',
    top: '92%',
    alignItems: 'center',
  },

  marker: {
    height: 60,
    width: 60,
  },

  imgPosition: {
    height: 40,
    width: 40,
    resizeMode: 'contain',
  },

  imgBack: {
    height: 30,
    width: 30,
    resizeMode: 'contain',
  },

  footer: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: 0,
    position: 'absolute',
    width: '100%',
  },
  region: {
    color: '#fff',
    lineHeight: 20,
    margin: 20,
  },
});

export default MapaUbicacionLocation;
