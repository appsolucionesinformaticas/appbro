import React, {useEffect, useState} from 'react';
// import SpinningImage from 'react-native-spinning-image';

import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  Animated,
  Keyboard,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
} from 'react-native';
import {ListItem, Icon} from 'react-native-elements';
import styles from './registro/registrostyle';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import messaging from '@react-native-firebase/messaging';
import Loading from '../componets/load/Loading';
import AsyncStorage from '@react-native-community/async-storage';
import {TextInput, ScrollView} from 'react-native-gesture-handler';
var {height, width} = Dimensions.get('window');

function LoginNew(props) {
  const [email, setEmail] = useState();
  const [isVisibleLoading, setisVisibleLoading] = useState(false);
  const [password, setPassword] = useState();

  const [dataBanner] = useState([
    require('../assets/img/banner1.png'),
    require('../assets/img/banner2.png'),
    require('../assets/img/banner3.png'),
  ]);

  const getLogin = () => {
    // console.log(props.navigation.navigate);
    setisVisibleLoading(true);

    fetch(
      global.linkServer +
        'wsperson/login?email=' +
        email +
        '&password=' +
        password,
    )
      .then((response) => response.json())
      .then((data) => {
        setisVisibleLoading(false);

        if (data.status == 'exito') {
          const itemPerson = {
            name: data.person.per_name,
            lastname: data.person.per_last_name,
            email: data.person.per_email,
            ci: data.person.per_ci,
            cellphone: data.person.per_cellphone,
            pass: data.person.per_password,
            latitud: 0.0,
            longitud: 0.0,
            urlphoto: data.person.per_url_image,
            addres: '',
            codeCity: data.person.location.loc_id,
            nameCiudad: data.person.location.loc_name,
            latitudCiudad: data.person.location.loc_latitude,
            longitudCiudad: data.person.location.loc_longitude,
            tarifa: data.person.location.loc_tarifa,
            distancia: data.person.location.loc_distancia_min,
            tminima: data.person.location.loc_tarifa_min,
          };

          AsyncStorage.getItem('person')
            .then((data) => {
              AsyncStorage.setItem('person', JSON.stringify(itemPerson));
              // props.navigation.reset({
              //   routes: [{ name: 'MainMenu' }],
              // });
              // props.navigate.navigation('Profile')
              // props.navigation.navigate('Profile')
              props.navigation.goBack(null);
            })
            .catch((error) => {
              // setcargando(false);
              alert(error);
            });
        } else {
          alert('Hubo un error Intenalo de nuevo');
        }
      })
      .catch((error) => {
        setisVisibleLoading(false);
        alert('Hubo un error Intenalo de nuevo');
      });
  };

  useEffect(() => {
    console.log(props.navigation);
    messaging()
      .getToken()
      .then((token) => {
        //return saveTokenToDatabase(token);
      });
  }, []);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <SafeAreaView
          style={{
            borderRadius: 12,
          }}>
          <View style={{height: '100%', backgroundColor: 'white'}}>
            <View
              style={{
                height: '35%',
                borderRadius: 12,
              }}>
              <Swiper
                style={{height: '100%'}}
                autoplay={true}
                autoplayTimeout={5}>
                {dataBanner.map((itemmap) => {
                  console.log(itemmap);
                  return (
                    <TouchableOpacity activeOpacity={0.9}>
                      <FastImage
                        style={{height: '100%', width: width}}
                        resizeMode="cover"
                        source={itemmap}
                      />
                    </TouchableOpacity>
                  );
                })}
              </Swiper>
              <TouchableOpacity
                activeOpacity={0.5}
                style={{marginLeft: 8, padding: 5, position: 'absolute'}}
                onPress={() => props.navigation.goBack()}>
                <Icon
                  type="ionicon"
                  name="arrow-back"
                  color="#0f8d8c"
                  size={30}
                  iconStyle={{color: '#0f8d8c'}}
                />
              </TouchableOpacity>
            </View>

            <ScrollView
              style={{
                height: '65%',
                borderRadius: 20,
                backgroundColor: 'white',
                marginTop: -20,
              }}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  alignContent: 'center',
                  borderRadius: 30,
                  paddingBottom: 20,
                }}>
                <Text
                  style={{
                    marginTop: 30,
                    fontSize: 35,
                    textAlign: 'center',
                    color: 'black',
                    fontFamily: 'Metropolis-Bold',
                    width: '90%',
                  }}>
                  Bienvenido/a
                </Text>

                <Text
                  style={{
                    marginTop: 12,
                    marginBottom: 12,
                    fontSize: 16,
                    textAlign: 'center',
                    color: 'gray',
                    fontFamily: 'Metropolis-Medium',

                    width: '90%',
                  }}>
                  Registra tu cuenta
                </Text>

                <TextInput
                  textContentType="givenName"
                  keyboardType="email-address"
                  returnKeyType="next"
                  // onSubmitEditing={() => lstname.current.focus()}
                  onChangeText={(text) => setEmail(text)}
                  placeholder="Email"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  style={styles.inputDatos}
                />

                <TextInput
                  textContentType="givenName"
                  keyboardType="name-phone-pad"
                  returnKeyType="next"
                  password={true}
                  secureTextEntry={true}
                  onChangeText={(text) => setPassword(text)}
                  // onSubmitEditing={() => lstname.current.focus()}
                  placeholder="Contraseña"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  style={styles.inputDatos}
                />

                <TouchableOpacity
                  style={{
                    marginTop: 35,
                    backgroundColor: '#0f8d8c',
                    borderRadius: 30,
                    color: 'white',
                    fontWeight: 'bold',
                    width: '80%',
                  }}
                  activeOpacity={0.9}
                  onPress={() => getLogin()}>
                  <Text
                    style={{
                      paddingVertical: 12,
                      textAlign: 'center',

                      color: 'white',

                      width: '100%',

                      fontFamily: 'Metropolis-Medium',
                    }}>
                    LOGIN
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => props.navigation.navigate('RecuperarPass')}>
                  <Text
                    style={{
                      marginTop: 30,
                      width: '90%',

                      fontSize: 16,
                      textAlign: 'center',
                      color: 'black',
                      fontFamily: 'Metropolis-Medium',
                    }}>
                    Olvidaste tu contraseña?
                  </Text>
                </TouchableOpacity>

                <TouchableOpacity
                  style={{
                    backgroundColor: '#ffa35d',
                    marginTop: 35,
                    borderRadius: 30,
                    width: '55%',
                  }}
                  activeOpacity={0.9}
                  onPress={() => props.navigation.navigate('Registro')}>
                  <Text
                    style={{
                      paddingVertical: 12,
                      textAlign: 'center',
                      borderRadius: 30,
                      color: 'white',

                      width: '100%',

                      fontFamily: 'Metropolis-Medium',
                    }}>
                    Crear Cuenta
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
          <Loading text="Cargando datos..." isVisible={isVisibleLoading} />
        </SafeAreaView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

export default LoginNew;
