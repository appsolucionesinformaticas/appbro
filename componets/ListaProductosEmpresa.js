import React, {useEffect, useState} from 'react';
import {
  SectionList,
  Text,
  Dimensions,
  FlatList,
  TouchableWithoutFeedback,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  View,
  Image,
} from 'react-native';
import TextTicker from 'react-native-text-ticker';
import Modal from 'react-native-modal';
import FastImage from 'react-native-fast-image';
import AsyncStorage from '@react-native-community/async-storage';
import {Badge, Icon, CheckBox, Button} from 'react-native-elements';
import AnimatedHeader from 'react-native-animated-header';
var {height, width} = Dimensions.get('window');
function ListaProductosEmpresa(props) {
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [dataSection, setdataSection] = useState([]);
  const [isModalMenu, setisModalMenu] = useState(false);

  const [isModalMenuOk, setisModalMenuOk] = useState(false);
  const [detallesMenu, setdetallesMenu] = useState([]);
  const [refresh, setRefresh] = useState('');
  const [quiality, setquiality] = useState(1);
  const [finalPrice, setfinalPrice] = useState(0.0);
  const [precioAllItems, setprecioAllItems] = useState(0.0);
  const [nombreProducto, setnombreProducto] = useState('');
  const [selectProducto, setselectProducto] = useState();
  const [dataComplete, setdataComplete] = useState();
  const [totalCart, settotalCart] = useState(0);

  const itemCarrito = (numberProductos) => {
    if (numberProductos <= 0) {
      return (
        <TouchableOpacity
          activeOpacity={0.5}
          onPress={() => props.navigation.navigate('Mi orden')}>
          <Icon
            type="ionicon"
            name="ios-cart-outline"
            color="white"
            size={porcentaje(8)}
            iconStyle={{color: 'white', marginRight: 25}}
          />
        </TouchableOpacity>
      );
    } else {
      return (
        <View>
          {/* <TouchableOpacity activeOpacity={0.5} onPress={() => this.props.navigation.reset({index: 0,routes: [{ name: 'MainMenu' }]})}></TouchableOpacity> */}
          <TouchableOpacity
            activeOpacity={0.5}
            style={{padding: 5}}
            onPress={() => props.navigation.navigate('Mi orden')}>
            <Icon
              type="ionicon"
              name="ios-cart-outline"
              color="white"
              size={porcentaje(8)}
              iconStyle={{color: 'white', marginRight: 25}}
            />
            <Badge
              value={numberProductos}
              status="error"
              containerStyle={{
                position: 'absolute',
                top: -4,
                right: 0,
                marginRight: 25,
              }}
            />
          </TouchableOpacity>
        </View>
      );
    }
  };

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  const itemData = (section) => {
    return (
      <>
        <Text
          style={{
            width: '90%',
            marginTop: 8,
            marginBottom: 8,
            fontSize: 20,
            fontFamily: 'Metropolis-Bold',
          }}>
          {section.item.name}
        </Text>

        <FlatList
          data={section.item.ltsviewKit}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          renderItem={_renderItem}
        />
      </>
    );
  };

  const onClickAddCart = (data, quiality) => {
    const itemcart = {
      producto: data,
      quality: quiality,
      name: selectProducto.kit_title,
      imagen: selectProducto.kit_url_image,
      totalCombo: precioAllItems,
      total: finalPrice,
      kit_id: selectProducto.kit_id,
    };

    const company = {
      code: dataComplete.com_id,
      name: dataComplete.com_name,
      image: dataComplete.com_url_img,
      description: dataComplete.com_description,
      latitud: dataComplete.com_lat,
      longitud: dataComplete.com_lon,
      direccion: dataComplete.com_address,
      listeProduct: [],
    };

    AsyncStorage.getItem('cart')
      .then((datacart) => {
        if (datacart !== null) {
          const cart = JSON.parse(datacart);

          if (cart.length > 0) {
            var inde = -1;
            cart.map((item, index) => {
              if (item.code == dataComplete.com_id) {
                inde = index;
              }
            });

            if (inde >= 0) {
              cart[inde].listeProduct.push(itemcart);
              AsyncStorage.setItem('cart', JSON.stringify(cart));
            } else {
              company.listeProduct.push(itemcart);
              cart.push(company);
              AsyncStorage.setItem('cart', JSON.stringify(cart));
            }
          } else {
            company.listeProduct.push(itemcart);
            cart.push(company);
            AsyncStorage.setItem('cart', JSON.stringify(cart));
          }

          // cart.push(itemcart)
          // AsyncStorage.setItem('cart',JSON.stringify(cart));
        } else {
          const cart = [];
          company.listeProduct.push(itemcart);
          cart.push(company);
          AsyncStorage.setItem('cart', JSON.stringify(cart));
        }

        var newValor = totalCart + 1;

        settotalCart(newValor);
        setisModalMenu(false);
        setisModalMenuOk(true);
      })
      .catch((error) => {
        alert(error);
      });
  };

  const _renderItem = (item) => {
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={() => {
          setnombreProducto(item.item.kit_title);
          setselectProducto(item.item);
          detailItems(item.item.ltsProducts);
        }}>
        <View
          style={{
            width: '100%',
            alignContent: 'center',
            alignItems: 'center',
            marginHorizontal: 8,
          }}>
          <View
            style={{
              width: '70%',

              alignItems: 'center',
            }}>
            <View
              style={{
                width: '90%',
                alignItems: 'center',
                marginTop: 8,
              }}>
              {/* <View
                style={{
                  borderRadius: 12,
                  width: porcentaje(60),
                  height: porcentaje(30),

                  backgroundColor: 'orange',
                  marginTop: 8,
                }}> */}
              <FastImage
                style={{
                  borderRadius: porcentaje(3),
                  width: porcentaje(50),
                  height: porcentaje(30),
                }}
                source={{
                  uri: item.item.kit_url_image,
                  priority: FastImage.priority.normal,
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
              <Text
                style={{
                  width: porcentaje(50),
                  marginTop: 4,
                  fontSize: 15,
                  color: 'black',
                  fontFamily: 'Metropolis-Bold',
                }}>
                {item.item.kit_title}
              </Text>
              <Text
                style={{
                  width: porcentaje(50),
                  marginTop: 4,
                  fontSize: 15,
                  color: 'gray',
                  fontFamily: 'Metropolis-Medium',
                }}>
                $ {(item.item.kit_price * 1.0).toFixed(2)}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const _renderHeader = (title) => {
    return (
      <Text
        style={{
          color: 'black',
          fontSize: 18,

          width: '90%',
          fontFamily: 'Metropolis-Bold',
          marginHorizontal: 20,
        }}>
        {title}
      </Text>
    );
  };

  useEffect(() => {
    setdataComplete(props.route.params.empresa.item);

    const unsubscribe = props.navigation.addListener('focus', () => {
      AsyncStorage.getItem('cart')
        .then((cart) => {
          if (cart !== null) {
            const cartfood = JSON.parse(cart);

            var number = cartfood.length;
            settotalCart(number);
          }
        })
        .catch((err) => {
          alert(err);
        });
    });

    fetch(
      global.linkServer +
        'wscategorykit/ltsCompanies?idCom=' +
        props.route.params.empresa.item.com_id,
    )
      .then((response) => response.json())
      .then((data) => {
        setdataSection(data.ltsCategoryKit);
      })
      .catch((error) => console.log(error));
  }, []);

  const detailItems = (nuevoArray) => {
    const dataComa = nuevoArray.reduce((r, s) => {
      r.push({title: s.name, data: s.ltsVariant});
      return r;
    }, []);
    console.log(dataComplete);

    setdetallesMenu(dataComa);
    sumValorFinal2(dataComa);

    setisModalMenu(true);
  };

  const _checkItem = (indexSection, indexItem) => {
    setRefresh(false);
    let newData = detallesMenu;
    newData[indexSection].data.map((item, index) => {
      if (index == indexItem) {
        newData[indexSection].data[index].default = true;
      } else {
        newData[indexSection].data[index].default = false;
      }
    });

    setdetallesMenu(newData);

    setRefresh(indexSection + ' ' + indexItem);
    sumValorFinal();
  };

  const sumarProductos = () => {
    var newQ = quiality + 1;

    setquiality(newQ);
    setfinalPrice((precioAllItems * newQ).toFixed(2));
  };

  const restarProductos = () => {
    // let {finalPrice, quiality, precioAllItems} = this.state;
    if (quiality > 1) {
      var newQ = quiality - 1;
      setquiality(newQ);

      setfinalPrice((precioAllItems * newQ).toFixed(2));
    }
  };

  const sumValorFinal = () => {
    var sum = 0;

    // setprecioAllItems(0.0);

    setfinalPrice(0.0);

    detallesMenu.map((item, index) => {
      // console.log(item);
      item.data.map((item2) => {
        if (item2.default) {
          sum += item2.pro_var_price;
        }
      });
    });

    var finalPrice2 = sum * quiality;
    setfinalPrice(finalPrice2);
    setprecioAllItems(sum);
  };

  const sumValorFinal2 = (array2) => {
    var sum = 0;

    // setprecioAllItems(0.0);

    setfinalPrice(0.0);

    array2.map((item, index) => {
      // console.log(item);
      item.data.map((item2) => {
        if (item2.default) {
          sum += item2.pro_var_price;
        }
      });
    });

    var finalPrice2 = sum * quiality;
    setfinalPrice(finalPrice2);
    setprecioAllItems(sum);
  };

  const _renderItemDetalles = (item, index, section) => {
    return (
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={() => _checkItem(detallesMenu.indexOf(section), index)}>
        <View
          style={{
            backgroundColor: '#ECECEC',
            alignItems: 'center',
            width: '98%',
            alignContent: 'center',
            justifyContent: 'center',
            marginHorizontal: 3,
            marginTop: 4,
          }}>
          <View style={styles.container2}>
            <View style={{width: '80%'}}>
              <Text style={styles.text}>{item.pro_var_name}</Text>
              <Text style={styles.text2}>
                $ {(item.pro_var_price * 1.0).toFixed(2)}
              </Text>
            </View>

            <CheckBox
              onPress={() => _checkItem(detallesMenu.indexOf(section), index)}
              checkedIcon={
                <Image
                  source={require('../assets/img/ic_bro_item_activado.png')}
                />
              }
              uncheckedIcon={
                <Image
                  source={require('../assets/img/ic_bro_item_desactivado.png')}
                />
              }
              checked={item.default}
            />
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SafeAreaView
      style={{
        flex: 1,
        width: '100%',
      }}>
      <AnimatedHeader
        style={{flex: 1}}
        title={props.route.params.empresa.item.com_name}
        renderLeft={() => (
          <TouchableOpacity
            activeOpacity={0.5}
            style={{marginLeft: 8, padding: 5}}
            onPress={() => props.navigation.goBack()}>
            <Icon
              type="ionicon"
              name="arrow-back"
              color="white"
              size={porcentaje(8)}
              iconStyle={{color: 'white'}}
            />
          </TouchableOpacity>
        )}
        renderRight={() => itemCarrito(totalCart)}
        backStyle={{
          fontSize: 12,
          fontFamily: 'Metropolis-Bold',
          color: 'white',
        }}
        backTextStyle={{
          fontSize: 12,
          color: 'white',
          fontFamily: 'Metropolis-Bold',
        }}
        titleStyle={{
          fontSize: 25,
          left: 10,
          bottom: 5,
          color: 'white',
          right: 50,

          fontFamily: 'Metropolis-Bold',
        }}
        imageSource={{
          uri: props.route.params.empresa.item.com_url_img,
        }}
        toolbarColor="#0f8d8c"
        disabled={false}>
        <ScrollView>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              justifyContent: 'center',
              alignSelf: 'center',
              alignContent: 'center',
            }}>
            <Text
              style={{
                width: '90%',
                marginTop: 8,
                marginBottom: 8,

                fontSize: 20,
                fontFamily: 'Metropolis-Bold',
              }}>
              Menú
            </Text>

            <FlatList
              data={dataSection}
              style={{width: '95%'}}
              keyExtractor={(item, index) => index.toString()}
              renderItem={itemData}
            />

            {/* <SectionList
              sections={dataSection}
              style={{width: '100%'}}
              keyExtractor={(item, index) => item + index}
              renderItem={({section}) => itemData(section)}
              renderSectionHeader={({section: {title}}) => _renderHeader(title)}
            /> */}
          </View>
        </ScrollView>
      </AnimatedHeader>

      <Modal transparent={true} visible={isModalMenu} animationType={'fade'}>
        <TouchableOpacity
          activeOpacity={1}
          onPressOut={() => setisModalMenu(false)}
          style={{
            flex: 1,
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View
            style={{
              width: width,
              height: height,
              backgroundColor: '#000000aa',
              alignContent: 'center',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TouchableWithoutFeedback>
              <View
                style={{
                  backgroundColor: 'white',
                  borderRadius: 25,
                  width: width / 1.2,
                  alignContent: 'center',
                  alignItems: 'center',
                  justifyContent: 'center',
                  paddingVertical: 5,
                }}>
                <View
                  style={{
                    width: '100%',
                    flexDirection: 'row-reverse',
                    // backgroundColor: 'green',
                    alignContent: 'flex-end',
                  }}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => setisModalMenu(false)}>
                    <Icon
                      type="material"
                      name="highlight-remove"
                      color="red"
                      size={40}
                      iconStyle={{color: '#0f8d8c', marginRight: 6}}
                    />
                  </TouchableOpacity>
                </View>

                <View style={{width: '90%', alignItems: 'center'}}>
                  <TextTicker
                    style={{
                      marginTop: 8,
                      marginBottom: 8,

                      color: '#0f8d8c',
                      fontSize: 20,
                      fontFamily: 'Metropolis-Bold',
                    }}
                    duration={10000}
                    loop
                    bounce
                    repeatSpacer={50}
                    marqueeDelay={1000}>
                    {nombreProducto}
                  </TextTicker>
                </View>

                <ScrollView
                  style={{
                    borderRadius: 25,
                    width: width / 1.2,
                    height: height / 1.8,

                    paddingVertical: 5,
                  }}>
                  <View
                    style={{
                      borderRadius: 25,
                      width: width / 1.2,

                      alignContent: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',
                      paddingVertical: 5,
                    }}>
                    <SectionList
                      sections={detallesMenu}
                      keyExtractor={(item, index) => item + index}
                      style={{
                        width: '100%',
                      }}
                      renderItem={({item, index, section}) =>
                        _renderItemDetalles(item, index, section)
                      }
                      renderSectionHeader={({section: {title}}) =>
                        _renderHeader(title)
                      }
                      refreshing={refresh}
                    />
                  </View>
                </ScrollView>

                <View
                  style={{
                    flexDirection: 'row',

                    width: '100%',
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <View
                    style={{
                      borderRadius: 12,

                      alignItems: 'center',
                      flexDirection: 'row',

                      width: '60%',

                      alignContent: 'center',
                      alignItems: 'center',
                      justifyContent: 'center',

                      marginVertical: 10,
                    }}>
                    <TouchableOpacity
                      activeOpacity={0.5}
                      onPress={() => restarProductos()}>
                      <Icon
                        type="font-awesome"
                        name="minus-circle"
                        color="white"
                        size={porcentaje(12)}
                        iconStyle={{color: '#0f8d8c'}}
                      />
                    </TouchableOpacity>
                    <Text
                      style={{
                        color: '#0f8d8c',
                        width: '30%',
                        textAlign: 'center',
                        paddingVertical: 5,
                        fontSize: 40,
                        marginHorizontal: 8,
                        fontFamily: 'Metropolis-Bold',
                      }}
                      onPress={() => this.sumarProductos()}>
                      {quiality}
                    </Text>

                    <TouchableOpacity onPress={() => sumarProductos()}>
                      <Icon
                        type="font-awesome"
                        name="plus-circle"
                        color="white"
                        size={porcentaje(12)}
                        iconStyle={{color: '#0f8d8c'}}
                      />
                    </TouchableOpacity>
                  </View>
                </View>

                <View
                  style={{
                    flexDirection: 'row',

                    width: '85%',
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    backgroundColor: '#0f8d8c',
                    paddingVertical: 8,
                    borderRadius: 7,
                  }}>
                  <Text
                    style={{
                      width: '95%',
                      textAlign: 'center',

                      fontSize: 16,
                      color: 'white',
                      marginHorizontal: 8,
                      fontFamily: 'Metropolis-Medium',
                    }}
                    onPress={() => onClickAddCart(detallesMenu, quiality)}>
                    Añadir a la orden ${(finalPrice * 1.0).toFixed(2)}
                  </Text>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableOpacity>
      </Modal>

      <Modal isVisible={isModalMenuOk}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() => setisModalMenuOk(!isModalMenuOk)}></TouchableOpacity>
          {/* <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            // source={require('../../images/ic_bro_cuenta.png')}
            source={require('../assets/img/ic_bro_orden_act.png')}
            resizeMode={FastImage.resizeMode.contain}
          /> */}

          <Icon
            type="ionicon"
            name="checkmark-circle-outline"
            color="white"
            size={porcentaje(35)}
            iconStyle={{color: '#0f8d8c', marginTop: 8}}
          />

          <Text style={styles.txtDialogTitle}>
            Tu producto se agrego con éxito
          </Text>

          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Continuar Comprando"
              onPress={() => setisModalMenuOk(false)}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  titleSection: {
    color: '#482181',
    fontWeight: 'bold',
    width: width - 40,
    marginHorizontal: 20,
    fontSize: 20,
    marginTop: 15,
  },

  textModal: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
  },
  imgModalMenu: {
    width: width / 5,
    height: width / 5,
    borderRadius: 12,
  },
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
    width: '95%',
  },
  text: {
    width: '95%',

    marginTop: 1,
    fontSize: 15,
    color: 'black',
    fontFamily: 'Metropolis-Medium',
  },
  text2: {
    width: '95%',
    marginTop: 1,
    fontSize: 12,
    color: 'gray',
    textAlign: 'justify',
    fontFamily: 'Metropolis-Medium',
  },
  imgProduct: {
    width: width / 2 - 60,
    height: width / 2 - 60,
    resizeMode: 'contain',
    marginLeft: 15,
  },
  imgCompania: {
    width: width,
    height: width / 1.5,
  },
  container3: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 15,
    width: width - 60,
    marginLeft: 10,
  },
  logoComapania: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  listCategoria: {
    alignItems: 'center',
    alignContent: 'center',
    width: width - 20,
    marginTop: 16,
  },
  btnStyleDialogClose: {
    backgroundColor: '#0f8d8c',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialog: {
    backgroundColor: 'rgba(238, 130, 79,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },
  btnContainerDialog: {
    width: '80%',
    alignSelf: 'center',

    // position: 'absolute',
    // bottom: 16,
    // paddingBottom: 32,
  },
  btnTitleDialog: {
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Metropolis-Medium',
    color: '#ffffff',
  },
  txtDialogTitle: {
    // backgroundColor: 'red',
    width: width - 32,
    // height: '100%',
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontFamily: 'Metropolis-Bold',
    marginTop: 15,
    // position: 'absolute',
    // bottom: 0,
  },
});

export default ListaProductosEmpresa;
