import React, {Component, useState, useEffect} from 'react';
import {
  StyleSheet,
  BackHandler,
  Text,
  Dimensions,
  Image,
  KeyboardAvoidingView,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  SafeAreaView,
  View,
} from 'react-native';
import {Icon} from 'react-native-elements';
import FastImage from 'react-native-fast-image';
import {TextInput} from 'react-native-gesture-handler';

var {height, width} = Dimensions.get('window');
function RecuperarPass(props) {
  useEffect(() => {
    // BackHandler.addEventListener('hardwareBackPress', backJS);
    // return () => BackHandler.removeEventListener('hardwareBackPress', backJS);
  }, []);

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <SafeAreaView style={styles.container}>
          <TouchableOpacity
            activeOpacity={0.5}
            style={{marginLeft: 8, padding: 5, position: 'absolute'}}
            onPress={() => props.navigation.goBack()}>
            <Icon
              type="ionicon"
              name="arrow-back"
              color="#0f8d8c"
              size={30}
              iconStyle={{color: '#0f8d8c'}}
            />
          </TouchableOpacity>

          <Image
            source={require('../assets/img/mano.png')}
            style={styles.image}
            resizeMode="contain"
          />

          <TextInput
            textContentType="emailAddress"
            keyboardType="name-phone-pad"
            returnKeyType="next"
            // onSubmitEditing={() => lstname.current.focus()}
            placeholder="Correo electrónico"
            placeholderTextColor="rgba(122,122,122,0.2)"
            style={styles.inputDatos}
          />

          <View
            style={{
              width: '100%',
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              style={{
                marginTop: 35,
                backgroundColor: '#0f8d8c',
                borderRadius: 30,
                color: 'white',
                fontWeight: 'bold',
                width: '80%',
              }}
              activeOpacity={0.9}
              onPress={() => getLogin()}>
              <Text
                style={{
                  paddingVertical: 12,
                  textAlign: 'center',

                  color: 'white',

                  width: '100%',

                  fontFamily: 'Metropolis-Medium',
                }}>
                ENVIAR
              </Text>
            </TouchableOpacity>
          </View>
          {/* <Text style={styles.txtIngresa} onPress={() => props.navigation.navigate('MainMenu')}>INGRESAR</Text> */}
        </SafeAreaView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
    width: '90%',
    justifyContent: 'center',
  },
  container3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    width: '90%',
    justifyContent: 'center',
  },

  container: {
    flex: 1,
    // alignItems: 'center',
    // alignContent: 'center',
    // alignItems: 'center',
    // justifyContent: 'center'
  },

  imagenEscudo: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },

  imageLogo: {
    width: '65%',
    height: '50%',
    resizeMode: 'contain',
  },

  txtIngresa: {
    width: '40%',
    // fontFamily: 'DINPro-CondensedRegular',
    height: 40,
    borderColor: '#dc751b',
    borderWidth: 4,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    textAlignVertical: 'center',
    color: '#482181',
    marginTop: 25,
    fontSize: 15,
    padding: 5,
  },

  etxt: {
    height: 40,
    // fontFamily: 'din',
    width: '20%',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 10,
    marginLeft: 10,
    padding: 10,
  },
  txtGreenL: {
    // fontFamily: 'din',
    width: '50%',
    fontSize: 12,
    color: '#65B22E',
    textAlign: 'left',
    marginTop: 8,
    // backgroundColor: 'red'
  },
  txtGreenR: {
    // fontFamily: 'din',
    width: '50%',
    fontSize: 12,
    color: '#65B22E',
    textAlign: 'right',
    marginTop: 8,
    // backgroundColor: 'yellow'
  },
  image: {
    height: 150,
    width: '100%',
    marginTop: 48,
    marginBottom: 16,
  },
  imageback: {
    height: 30,
    width: 30,
    marginTop: 16,
    marginLeft: 32,
    alignSelf: 'flex-start',
  },
  inputDatos: {
    backgroundColor: 'rgba(0, 0, 0,0.0525)',
    borderRadius: 5,
    width: '80%',

    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 18,
    textAlign: 'center',
    color: 'gray',
    fontWeight: 'bold',
    marginVertical: 4,
    marginTop: 16,

    // paddingVertical: height / 20
  },
});

export default RecuperarPass;
