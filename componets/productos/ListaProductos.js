import React, {Component, useState, useEffect} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../../styles/clientstyle';
var {height, width} = Dimensions.get('window');
import Carousel, {Pagination} from 'react-native-snap-carousel';
import {SearchBar, Icon, Button} from 'react-native-elements';
import TextTicker from 'react-native-text-ticker';
import {ScrollView} from 'react-native-gesture-handler';

function ListaProductos(props) {
  const [paginationP, setPagination] = useState(0);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [ltsProductos, setltsProductos] = useState([
    {
      name: '123123123',
      id: 12,
    },
    {
      name: '123123123',
      id: 2,
    },
    {
      name: '123123123',
      id: 3,
    },
    {
      name: '123123123',
      id: 4,
    },
    {
      name: '123123123',
      id: 5,
    },
  ]);
  const [search, setSearch] = useState('');
  const htop = StatusBar.currentHeight;

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backJS);

    return () => BackHandler.removeEventListener('hardwareBackPress', backJS);
  }, []);

  const backJS = () => {
    props.navigation.goBack();
  };

  const _renderItemProduct = (item) => {
    return (
      <ImageBackground
        source={require('../../images/ic_bro_back_menu.png')}
        style={styles.bgItemProducto}>
        <FastImage
          style={styles.imgProducto}
          source={{
            uri:
              'https://firebasestorage.googleapis.com/v0/b/mercard-adcad.appspot.com/o/pngkit_comida-png_1248671.png?alt=media&token=dbdff2ed-bf97-48f9-b30c-85dbec5e440e',
            priority: FastImage.priority.normal,
          }}
          resizeMode={FastImage.resizeMode.contain}
        />
        <View style={styles.txtViewStyleTitleProducto}>
          <TextTicker
            style={styles.txtTitleProducto}
            duration={5000}
            loop
            bounce
            repeatSpacer={50}
            marqueeDelay={1000}>
            Seco de carne con papas
          </TextTicker>
        </View>
        <ScrollView
          style={{
            // flex: 1,
            // alignContent: 'center',
            // backgroundColor: '#FFFFE2',
            height: '90%',
            width: '100%',
            marginBottom: 48,
            //   borderColor: '#c2c2c2',
            //   borderWidth: 1,
            //   marginTop: 16,
            //   alignItems: 'center',
          }}>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
          <Text style={styles.txtDescriptionProducto}>
            Auto-linking React Native modules for target `broapp`:
            RNCAsyncStorage, RNCMaskedView, RNFastImage,
          </Text>
        </ScrollView>
        <Button
          buttonStyle={styles.btnStyle}
          containerStyle={styles.btnContainer}
          titleStyle={styles.btnTitle}
          title="Agregar al carrito"
          onPress={() => props.viewPager.current.setPage(1)}></Button>
      </ImageBackground>
    );
  };

  const currentProducto = (index) => {
    setPagination(index);
  };

  return (
    <ImageBackground
      source={require('../../images/ic_bro_fondo_app.png')}
      style={{
        height: null,
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            width: width,
            height: 60,
            // backgroundColor: 'red',
            justifyContent: 'center',
            // marginTop: StatusBar.currentHeight,
          }}>
          <TouchableOpacity
            style={styles.btnTopPerfil}
            onPress={() => backJS()}>
            <FastImage
              style={styles.btnTopPerfil}
              source={require('../../images/ic_bro_atras_celeste.png')}
            />
          </TouchableOpacity>

          <View style={styles.txtViewStyleTitleRestaurante}>
            <TextTicker
              style={styles.txtTitleRestaurant}
              duration={7000}
              loop
              bounce
              repeatSpacer={50}
              marqueeDelay={1000}>
              Almuerzos
            </TextTicker>
          </View>

          <TouchableOpacity
            style={styles.btnTopMenu}
            onPress={() => props.navigation.navigate('Carrito')}>
            <FastImage
              style={styles.btnTopMenu}
              source={require('../../images/ic_bro_carrito_celeste.png')}
            />
          </TouchableOpacity>
        </View>

        {/* <Text style={styles.txtItemProducto}>JA</Text> */}

        <Carousel
          ref={(c) => {
            _carousel = c;
          }}
          data={ltsProductos}
          renderItem={_renderItemProduct}
          sliderWidth={width}
          layout="default"
          layoutCardOffset={18}
          itemWidth={width - 64}
          inactiveSlideShift={0}
          useScrollView={true}
          onSnapToItem={(index) => currentProducto(index)}
        />
        <Pagination
          dotsLength={ltsProductos.length}
          activeDotIndex={paginationP}
          vertical={false}
          containerStyle={{
            backgroundColor: 'rgba(0, 0, 0, 0)',
            // height: 150,
            width: width,
          }}
          dotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            // marginHorizontal: 8,
            backgroundColor: 'rgba(238, 132, 85, 0.92)',
          }}
          inactiveDotStyle={{
            width: 10,
            height: 10,
            borderRadius: 5,
            // marginHorizontal: 8,
            backgroundColor: 'rgba(26, 196, 156, 0.92)',
          }}
          inactiveDotOpacity={0.4}
          inactiveDotScale={0.6}
        />
      </SafeAreaView>
    </ImageBackground>
  );
}

export default ListaProductos;
