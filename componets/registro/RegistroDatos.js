import React, {Component, useState, useRef} from 'react';
import {
  KeyboardAvoidingView,
  Text,
  Dimensions,
  FlatList,
  ScrollView,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
  View,
  Keyboard,
  ImageBackground,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Button} from 'react-native-elements';
import styles from './registrostyle';
import {TextInput} from 'react-native-gesture-handler';
var {height, width} = Dimensions.get('window');
import DatePicker from 'react-native-datepicker';

function RegistroDatos(props) {
  const {lstname, email, ci, dob} = useRef();
  const [date, setDate] = useState();

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        style={{
          flex: 1,
          alignContent: 'flex-start',
          justifyContent: 'flex-start',
        }}>
        <SafeAreaView style={styles.container}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-start',
            }}
            activeOpacity={0.2}
            onPress={() => openDialog()}>
            <FastImage
              style={{
                width: 35,
                height: 35,
                position: 'relative',
                // top: 8,
                left: 16,
                alignSelf: 'flex-start',
              }}
              source={require('../../images/ic_bro_atras.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
          <View style={styles.containerTop}>
            <Text style={styles.txtTitle}>Registrate</Text>
          </View>

          <View style={styles.containerBottomRegisto}>
            <ImageBackground
              source={require('../../images/ic_bro_back_menu.png')}
              style={{
                width: '100%',
                height: '100%',
                borderTopLeftRadius: 50,
                borderTopRightRadius: 50,
                resizeMode: 'cover',
                overflow: 'hidden',
              }}>
              <Text style={styles.txtRegistroTitle}>
                Ingresa tu información!
              </Text>

              <TextInput
                textContentType="givenName"
                keyboardType="name-phone-pad"
                returnKeyType="next"
                // onSubmitEditing={() => lstname.current.focus()}
                placeholder="Nombre"
                placeholderTextColor="rgba(255,255,255,0.6)"
                style={styles.inputDatos}
              />
              <TextInput
                textContentType="familyName"
                keyboardType="name-phone-pad"
                returnKeyType="next"
                // ref={lstname}
                placeholder="Apellido"
                placeholderTextColor="rgba(255,255,255,0.6)"
                style={styles.inputDatos}
              />

              <TextInput
                textContentType="telephoneNumber"
                keyboardType="name-phone-pad"
                returnKeyType="next"
                // ref={lstname}
                placeholder="Celular"
                placeholderTextColor="rgba(255,255,255,0.6)"
                style={styles.inputDatos}
              />
              <TextInput
                textContentType="emailAddress"
                keyboardType="email-address"
                returnKeyType="next"
                placeholder="Correo"
                placeholderTextColor="rgba(255,255,255,0.6)"
                style={styles.inputDatos}
              />
              <TextInput
                keyboardType="number-pad"
                returnKeyType="next"
                placeholder="Cédula"
                placeholderTextColor="rgba(255,255,255,0.6)"
                style={styles.inputDatos}
              />

              <DatePicker
                // style={{ width: 200 }}
                date={date}
                mode="date"
                placeholder="Fecha de nacimiento"
                placeholderTextColor="rgba(255,255,255,0.6)"
                format="DD-MM-YYYY"
                minDate="1970-01-01"
                maxDate="2010-06-01"
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                // customStyles={{
                //     dateIcon: {
                //         position: 'absolute',
                //         left: 0,
                //         top: 4,
                //         marginLeft: 0
                //     },
                //     dateInput: {
                //         backgroundColor: "rgba(255, 255, 255,0.25)",
                //         borderRadius: 5,
                //         width: '80%',
                //         height: 35,
                //         alignSelf: 'center',
                //         alignItems: 'center',
                //         justifyContent: 'center',
                //         fontSize: 20,
                //         textAlign: 'center',
                //         color: 'white',
                //         fontWeight: 'bold',
                //         marginVertical: 4
                //     }
                //     // ... You can check the source to find the other keys.
                // }}
                style={styles.inputDatos}
                onDateChange={(date) => {
                  setDate(date);
                }}
              />

              <Button
                buttonStyle={styles.btnStyle}
                containerStyle={styles.btnContainer2}
                titleStyle={styles.btnTitle}
                title="Continuar"
                onPress={() => props.viewPager.current.setPage(4)}></Button>
            </ImageBackground>
          </View>
        </SafeAreaView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

export default RegistroDatos;
