import React, { Component, useState } from 'react';
import { KeyboardAvoidingView, Text, Dimensions, FlatList, ScrollView, Modal, TouchableOpacity, TouchableWithoutFeedback, Linking, SafeAreaView, View, Keyboard, ImageBackground } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Button } from 'react-native-elements'
import styles from './registrostyle'
import { TextInput } from 'react-native-gesture-handler';
var { height, width } = Dimensions.get("window")

function RegistroCell(props) {


    return (
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
            <KeyboardAvoidingView
                behavior={Platform.OS == "ios" ? "padding" : "height"}
                style={{ flex: 1, alignContent: 'flex-start', justifyContent: 'flex-start' }}
            >
                <SafeAreaView
                    style={styles.container} >

                    <View style={styles.containerTop}>
                        <Text style={styles.txtTitle}>Registrate</Text>
                    </View>

                    <View style={styles.containerBottomRegisto}>
                        <ImageBackground
                            source={require('../../images/ic_bro_back_menu.png')}
                            style={{
                                width: '100%',
                                height: '100%',
                                borderTopLeftRadius: 50,
                                borderTopRightRadius: 50,
                                resizeMode: "cover",
                                overflow: "hidden",
                            }}

                        >
                            <Text style={styles.txtRegistroTitle}>Ayudanos a completar tu registro, introduce tu número y recibiras un código</Text>


                            <TextInput
                                maxLength={10}
                                keyboardType='number-pad'
                                style={styles.inputCell} />

                            <Button
                                buttonStyle={styles.btnStyle}
                                containerStyle={styles.btnContainer}
                                titleStyle={styles.btnTitle}
                                title="Continuar"
                                onPress={() => props.viewPager.current.setPage(3)}
                            >

                            </Button>
                        </ImageBackground>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
    )

}

export default RegistroCell