import React, {Component, useState, useEffect} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  ScrollView,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  Alert,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Button} from 'react-native-elements';
import styles from './registrostyle';
var {height, width} = Dimensions.get('window');
import ViewPager from '@react-native-community/viewpager';
import BienvenidoIntro from './BienvenidoIntro';
import RegistroCell from './RegistroCell';
import RegistroCode from './RegistroCode';
import RegistroDatos from './RegistroDatos';
import RegistroUbicacion from './RegistroUbicacion';
import RegistroTerminos from './RegistroTerminos';

function RegistroMain(props) {
  const [index, setIndex] = useState(0);
  const [animationsAreEnabled, setanimationsAreEnabled] = useState(true);
  const viewPager = React.createRef();
  // move = (delta) => {
  //     const page = this.state.page + delta;
  //     this.go(page);
  //   };

  //   go = (page) => {
  //     if (animationsAreEnabled) {
  //       /* $FlowFixMe we need to update flow to support React.Ref and createRef() */
  //       ViewPager.setPageWithoutAnimation
  //     } else {
  //       /* $FlowFixMe we need to update flow to support React.Ref and createRef() */
  //       this.viewPager.current.setPageWithoutAnimation(page);
  //     }
  //   };

  return (
    <ImageBackground
      source={require('../../images/ic_bro_fondo_app.png')}
      style={{
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <ViewPager
        ref={viewPager}
        style={{flex: 1}}
        initialPage={index}
        showPageIndicator={true}
        scrollEnabled={true}
        onPageScroll={() => {
          setIndex(index);
        }}>
        <View key="1">
          <BienvenidoIntro
            viewPager={viewPager}
            navigation={props.navigation}></BienvenidoIntro>
          {/* <Text>asd</Text> */}
        </View>
        <View key="2">
          <RegistroCell viewPager={viewPager}></RegistroCell>
        </View>
        <View key="3">
          <RegistroCode viewPager={viewPager}></RegistroCode>
        </View>
        <View key="4">
          <RegistroDatos viewPager={viewPager}></RegistroDatos>
        </View>
        <View key="5">
          <RegistroUbicacion viewPager={viewPager}></RegistroUbicacion>
        </View>
      </ViewPager>
    </ImageBackground>
  );
}

export default RegistroMain;
