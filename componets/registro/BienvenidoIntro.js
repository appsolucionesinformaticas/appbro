import React, {Component, useState, useEffect} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
  BackHandler,
  Alert,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import {Button} from 'react-native-elements';
import styles from './registrostyle';
import Modal from 'react-native-modal';
var {height, width} = Dimensions.get('window');

function BienvenidoIntro(props) {
  const [isModalMenu, setisModalMenu] = useState(false);

  const backAction = () => {
    Alert.alert('Hold on!', 'Are you sure you want to go back?', [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {text: 'YES', onPress: () => props.navigation.goBack()},
    ]);
    return true;
  };

  const backJS = () => {
    setisModalMenu(false);
    props.navigation.goBack();
  };

  const openDialog = () => {
    setisModalMenu(true);
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backAction);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', backAction);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      {/* onPress={() => props.viewPager.current.setPage(1)}> */}
      <TouchableOpacity
        style={{
          alignSelf: 'flex-start',
        }}
        activeOpacity={0.2}
        onPress={() => openDialog()}>
        <FastImage
          style={{
            width: 35,
            height: 35,
            position: 'relative',
            // top: 8,
            left: 16,
            alignSelf: 'flex-start',
          }}
          source={require('../../images/ic_bro_atras.png')}
          resizeMode={FastImage.resizeMode.contain}
        />
      </TouchableOpacity>
      <Modal isVisible={isModalMenu}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'rgba(0, 178, 170,0.9)',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() => setisModalMenu(!isModalMenu)}>
            <FastImage
              style={{
                width: 25,
                height: 25,
                top: 8,
                right: 8,
              }}
              source={require('../../images/ic_bro_cerrar_session.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          </TouchableOpacity>
          <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            source={require('../../images/ic_bro_cuenta.png')}
            resizeMode={FastImage.resizeMode.contain}
          />

          <Text style={styles.txtDialogTitle}>
            Deseas salir del registro?, los datos ingresados se perderán
          </Text>

          <View style={{flexDirection: 'row', alignSelf: 'center'}}>
            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Salir"
              onPress={() => backJS()}
            />
            <Button
              buttonStyle={styles.btnStyleDialog}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Continuar"
              onPress={() => setisModalMenu(!isModalMenu)}
            />
          </View>
        </View>
      </Modal>

      <View style={styles.containerTop}>
        <Text style={styles.txtTitle}>Hola bro!</Text>
      </View>

      <View style={styles.containerBottom}>
        <ImageBackground
          source={require('../../images/ic_bro_back_menu.png')}
          style={styles.imgbgBienvenido}>
          <Text style={styles.txtBienvenido}>
            Hola bro! For stretch to have an effect, children must not have a
            fixed dimension along the secondary axis. In the following example,
            setting alignItems: stretch does nothing until the width: 50 is
            removed from the children. la bro! la bro! la bro! Hola bro! la bro!
            la bro! la bro! Hola bro! la bro! la bro! la bro! Hola bro! la bro!
            la bro! la bro! Hola bro! la bro! la bro! la bro!{' '}
          </Text>

          <Button
            buttonStyle={styles.btnStyle}
            containerStyle={styles.btnContainer}
            titleStyle={styles.btnTitle}
            title="Continuar"
            onPress={() => props.viewPager.current.setPage(1)}></Button>
        </ImageBackground>
      </View>
    </SafeAreaView>
  );
}

export default BienvenidoIntro;
