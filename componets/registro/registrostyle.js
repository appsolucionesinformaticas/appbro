import {Dimensions, StyleSheet, StatusBar, SafeAreaView} from 'react-native';

const React = require('react-native');

var {height, width} = Dimensions.get('window');
// const { StyleSheet } = React;

export default {
  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
    marginTop: StatusBar.currentHeight,

    // marginBottom: Sta
  },

  containerTop: {
    width: width,
    height: '20%',
    // marginTop: 8,
    // backgroundColor: 'rgba(0, 178, 170,0.9)',
  },

  inputLogin: {
    backgroundColor: 'rgba(0, 178, 170,0.9)',
    borderRadius: 25,
    width: width / 1.2,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: height / 20,
  },
  inputCell: {
    backgroundColor: 'rgba(255, 255, 255,0.6)',
    borderRadius: 5,
    width: '60%',
    height: 45,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 24,
    textAlign: 'center',
    color: 'white',
    // paddingVertical: height / 20
  },
  inputDatos: {
    backgroundColor: 'rgba(52, 52, 52,0.05)',
    borderRadius: 30,
    width: '80%',
    height: 45,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 16,
    textAlign: 'center',
    color: 'black',
    fontFamily: 'Metropolis-Medium',
    marginVertical: 4,

    // paddingVertical: height / 20
  },

  imgbgBienvenido: {
    width: '100%',
    height: '100%',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
    resizeMode: 'cover',
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },

  containerBottom: {
    width: width - 32,
    height: '75%',
    // backgroundColor: "#c1c1c1",
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    borderTopLeftRadius: 50,
    borderTopRightRadius: 50,
  },
  containerBottomRegisto: {
    width: width - 32,
    height: '75%',
    // backgroundColor: "#c1c1c1",
    // paddi: 32
    position: 'absolute',
    bottom: 0,
  },
  txtTitle: {
    // backgroundColor: 'red',
    width: width,
    // height: '100%',
    textAlign: 'center',
    color: 'rgba(0, 178, 170,1)',
    fontSize: 50,
    fontWeight: 'bold',
    position: 'absolute',
    bottom: 0,
  },
  txtDialogTitle: {
    // backgroundColor: 'red',
    width: width - 32,
    // height: '100%',
    textAlign: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: -5,
    // position: 'absolute',
    // bottom: 0,
  },
  txtBienvenido: {
    fontSize: 20,
    // backgroundColor: 'red',
    // height: '100%',
    // width: '50%',
    paddingLeft: 64,
    paddingRight: 64,
    textAlign: 'center',
    // justifyContent: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
  txtRegistroTitle: {
    fontSize: 22,
    // backgroundColor: 'red',
    // height: '90%',
    // width: '50%',
    paddingVertical: 32,
    paddingLeft: 64,
    paddingRight: 64,
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
  },
  btnStyle: {
    backgroundColor: 'rgba(238, 130, 79,0.9)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
  },
  btnContainer: {
    width: '60%',
    alignSelf: 'center',
    position: 'absolute',
    bottom: 16,
    paddingBottom: 32,
  },
  btnContainer2: {
    width: '60%',
    alignSelf: 'center',
    // position: 'absolute',
    marginTop: 8,
    // paddingBottom: 32
  },
  btnTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#ffffff',
  },

  btnStyleDialogClose: {
    backgroundColor: 'rgba(110, 100, 110,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialog: {
    backgroundColor: 'rgba(238, 130, 79,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },
  btnContainerDialog: {
    width: '40%',
    alignSelf: 'center',

    // position: 'absolute',
    // bottom: 16,
    // paddingBottom: 32,
  },
  btnTitleDialog: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'center',
    color: '#ffffff',
  },
};
