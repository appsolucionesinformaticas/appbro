import React, { Component, useState, useRef } from 'react';
import { KeyboardAvoidingView, Text, Dimensions, FlatList, ScrollView, Modal, TouchableOpacity, TouchableWithoutFeedback, Linking, SafeAreaView, View, Keyboard, ImageBackground } from 'react-native';
import FastImage from 'react-native-fast-image';
import { Button } from 'react-native-elements'
import styles from './registrostyle'
import { TextInput } from 'react-native-gesture-handler';
var { height, width } = Dimensions.get("window")
import DatePicker from 'react-native-datepicker'

function RegistroUbicacion(props) {
    const { lstname, email, ci, dob } = useRef()
    const [date, setDate] = useState()

    return (
        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss() }}>
            <KeyboardAvoidingView
                behavior={Platform.OS == "ios" ? "padding" : "height"}
                style={{ flex: 1, alignContent: 'flex-start', justifyContent: 'flex-start' }}
            >
                <SafeAreaView
                    style={styles.container} >

                    <View style={styles.containerTop}>
                        <Text style={styles.txtTitle}>Registrate</Text>
                    </View>

                    <View style={styles.containerBottomRegisto}>
                        <ImageBackground
                            source={require('../../images/ic_bro_back_menu.png')}
                            style={{
                                width: '100%',
                                height: '100%',
                                borderTopLeftRadius: 50,
                                borderTopRightRadius: 50,
                                resizeMode: "cover",
                                overflow: "hidden",
                            }}

                        >
                            <Text style={styles.txtRegistroTitle}>Selecciona tu Provincia y Cantón</Text>


                            <TextInput
                                textContentType='addressState'
                                keyboardType='name-phone-pad'
                                returnKeyType='next'
                                // onSubmitEditing={() => lstname.current.focus()}
                                placeholder='Provincia'
                                placeholderTextColor='rgba(255,255,255,0.6)'
                                style={styles.inputDatos} />
                            <TextInput
                                textContentType='addressCity'
                                keyboardType='name-phone-pad'
                                returnKeyType='next'
                                // ref={lstname}
                                placeholder='Cantón'
                                placeholderTextColor='rgba(255,255,255,0.6)'
                                style={styles.inputDatos} />
                            

                            <Button
                                buttonStyle={styles.btnStyle}
                                containerStyle={styles.btnContainer2}
                                titleStyle={styles.btnTitle}
                                title="Continuar"
                                onPress={() => props.viewPager.current.setPage(4)}
                            >

                            </Button>
                        </ImageBackground>
                    </View>
                </SafeAreaView>
            </KeyboardAvoidingView>
        </TouchableWithoutFeedback>
    )

}

export default RegistroUbicacion