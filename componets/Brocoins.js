import React, {Component, useState, useEffect} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  BackHandler,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';

var {height, width} = Dimensions.get('window');
function Brocoins(props) {
  const [datebrocoins, setdatebrocoins] = useState(0);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backJS);

    return () => BackHandler.removeEventListener('hardwareBackPress', backJS);
  }, []);

  const backJS = () => {
    props.navigation.goBack();
  };

  const sumar = () => {
    setdatebrocoins(datebrocoins + 1);
  };

  const resta = () => {
    if (datebrocoins > 1) {
      setdatebrocoins(datebrocoins - 1);
    }
  };

  return (
    <ImageBackground
      source={require('../images/ic_bro_fondo_app.png')}
      style={{
        height: null,
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'flex-start',
            alignItems: 'flex-start',
            width: width - 40,
          }}>
          <TouchableOpacity onPress={() => backJS()}>
            <FastImage
              style={{width: 30, height: 30, marginTop: 10}}
              resizeMode="contain"
              source={require('../images/ic_bro_atras_celeste.png')}
            />
          </TouchableOpacity>
        </View>

        <FastImage
          style={{width: width / 4, height: width / 4, marginTop: 10}}
          resizeMode="contain"
          source={require('../images/ic_bro_brocoins_celeste.png')}
        />

        <Text
          style={{
            fontSize: 34,
            textAlign: 'center',
            color: '#00b2aa',
            width: width - 40,
            fontWeight: 'bold',
            marginTop: 8,
          }}>
          BroCoins
        </Text>

        <View
          style={{
            width: width - 20,
            alignContent: 'center',
            alignItems: 'center',
            flexDirection: 'row',
            justifyContent: 'center',
            marginTop: 10,
          }}>
          <FastImage
            style={{width: width / 2, height: width / 2, marginTop: 10}}
            resizeMode="contain"
            source={require('../images/ic_bro_coins_ca.png')}>
            <Text
              style={{
                textAlign: 'center',
                bottom: 20,
                position: 'absolute',
                alignSelf: 'center',
                fontSize: 45,
                fontWeight: 'bold',
                color: '#00b2aa',
              }}>
              {datebrocoins}
            </Text>
          </FastImage>

          <View style={{marginLeft: 10}}>
            <TouchableOpacity onPress={() => sumar()}>
              <FastImage
                style={{width: 40, height: 40, marginTop: 10}}
                resizeMode="contain"
                source={require('../images/ic_bro_arriba_celeste.png')}
              />
            </TouchableOpacity>

            <TouchableOpacity onPress={() => resta()}>
              <FastImage
                style={{width: 40, height: 40, marginTop: 10}}
                resizeMode="contain"
                source={require('../images/ic_bro_abajo_celeste.png')}
              />
            </TouchableOpacity>
          </View>
        </View>

        <Text
          style={{
            color: 'white',
            marginVertical: 30,
            width: width - 50,
            textAlign: 'center',
            backgroundColor: 'rgba(238, 130, 79,0.9)',
            width: width / 2,
            paddingVertical: 8,
            borderRadius: 8,
          }}>
          Canjear!
        </Text>

        <Text
          style={{
            fontSize: 18,
            textAlign: 'center',
            color: '#00b2aa',
            width: width - 40,
          }}>
          Qué son bro coins ?
        </Text>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default Brocoins;
