import React, {Component, useState} from 'react';
import {
  ImageBackground,
  Text,
  Dimensions,
  FlatList,
  StatusBar,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Linking,
  SafeAreaView,
  View,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import styles from '../styles/clientstyle';

var {height, width} = Dimensions.get('window');
function Ayuda(props) {
  return (
    <ImageBackground
      source={require('../images/ic_bro_fondo_app.png')}
      style={{
        height: null,
        width: width,
        resizeMode: 'cover',
        overflow: 'hidden',
        flex: 1,
      }}>
      <SafeAreaView style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'flex-start',
            alignItems: 'flex-start',
            width: width - 40,
          }}>
          <FastImage
            style={{width: 30, height: 30, marginTop: 10}}
            resizeMode="contain"
            source={require('../images/ic_bro_atras_celeste.png')}
          />
        </View>

        <FastImage
          style={{
            width: width / 4,
            height: width / 4,
            marginTop: 10,
            marginBottom: 10,
          }}
          resizeMode="contain"
          source={require('../images/ic_bro_ayuda_celeste.png')}
        />

        <Text style={styles.textCategoria}>
          No te ahogues bro! aquí estamos para ayudarte
        </Text>

        <Text
          style={{
            fontSize: 16,
            textAlign: 'center',
            color: 'white',
            width: width / 1.5,
            marginHorizontal: 5,
            backgroundColor: 'rgba(0, 178, 170,0.70)',
            paddingVertical: 8,
            borderRadius: 8,
            marginTop: 20,
          }}>
          Consultas Frecuentes
        </Text>

        <Text
          style={{
            fontSize: 16,
            textAlign: 'center',
            color: 'white',
            width: width / 1.5,
            marginHorizontal: 5,
            backgroundColor: 'rgba(0, 178, 170,0.70)',
            paddingVertical: 8,
            borderRadius: 8,
            marginTop: 20,
          }}>
          Soporte
        </Text>

        <Text
          style={{
            fontSize: 16,
            textAlign: 'center',
            color: 'white',
            width: width / 1.5,
            marginHorizontal: 5,
            backgroundColor: 'rgba(0, 178, 170,0.70)',
            paddingVertical: 8,
            borderRadius: 8,
            marginTop: 20,
          }}>
          Enviar un Mensaje
        </Text>
      </SafeAreaView>
    </ImageBackground>
  );
}

export default Ayuda;
