import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  SafeAreaView,
} from 'react-native';
import { Badge, Icon, CheckBox, Button } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
var { height, width } = Dimensions.get('window');
import FastImage from 'react-native-fast-image';
import { Card } from 'react-native-shadow-cards';
import Modal from 'react-native-modal';

function Miorden(props) {
  const [numberProceso, setnumberProceso] = useState(0);
  const [numberCompra, setnumberCompra] = useState(0);
  const [dataCart, setdataCart] = useState([]);
  const [statusShopa, setstatusShopa] = useState(false);
  const [itemDelete, setitemDelete] = useState();
  const [person, setperson] = useState('');

  const [isModalLocalizacion, setisModalLocalizacion] = useState(false);
  const [isModalSesion, setisModalSesion] = useState(false);

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
      AsyncStorage.getItem('cart')
        .then((cart) => {
          if (cart !== null) {
            const cartfood = JSON.parse(cart);
            var number = cartfood.length;
            setnumberCompra(number);
            setdataCart(cartfood);
          }
        })
        .catch((err) => {
          alert(err);
        });
    });
  }, []);

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);
          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
          }, 100);
        }
      })
      .catch((err) => {
        alert(err);
      });
  };

  const rendernumberProceso = () => {
    if (numberProceso > 0) {
      return (
        <View style={styles.container32}>
          <TouchableOpacity
            activeOpacity={1}
            onPressOut={() => this.props.navigation.navigate('ShopLoading')}>
            <FastImage
              style={styles.imagenEscudo}
              resizeMode="contain"
              source={require('../assets/img/ic_vesi_empaquetando.gif')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            activeOpacity={1}
            onPressOut={() => this.props.navigation.navigate('ShopLoading')}>
            <Text style={styles.textOrangeLista}>Proceso</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
    }
  };

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  const _renderItem = (item, index) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          alignContent: 'center',
          justifyContent: 'center',
          width: '100%',
          marginTop: 4,
          marginBottom: 4,
          overflow: 'hidden',
        }}>
        <TouchableOpacity
          activeOpacity={0.1}
          style={{ alignItems: 'center', alignContent: 'center', width: '90%' }}
          onPress={() => {
            if (person.ci == "") {
              setisModalSesion(true);
            } else {
              if (person.latitud != 0) {
                props.navigation.navigate('Carrito', { empresa: item });
              } else {
                setisModalLocalizacion(true);
              }
            }

          }}>
          <Card
            style={{
              flexDirection: 'row',
              padding: 10,
              paddingTop: 15,
              marginTop: 2,
              marginBottom: 10,
              padding: 10,
              margin: 10,
              elevation: 10,
              cornerRadius: 12,
            }}>
            <View
              style={{
                width: '40%',

                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View style={styles.imgCategoriaEmpresas}>
                <FastImage
                  style={styles.imgCategoriaEmpresas}
                  source={{
                    uri: item.image,
                    priority: FastImage.priority.normal,
                  }}
                  resizeMode={FastImage.resizeMode.contain}
                />
              </View>
            </View>

            <View
              style={{
                width: '60%',

                alignContent: 'center',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 10,
                  alignItems: 'center',
                  width: '100%',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 18,
                    fontFamily: 'Metropolis-Bold',
                  }}>
                  {item.name}
                </Text>
                <Icon
                  type="material"
                  name="delete-forever"
                  color="white"
                  onPress={() => {
                    setitemDelete(item);
                    setstatusShopa(true);
                  }}
                  size={porcentaje(10)}
                  iconStyle={{ color: '#ffa35d' }}
                />
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 8,
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Icon
                  type="ionicon"
                  name="location"
                  color="white"
                  size={porcentaje(4)}
                  iconStyle={{ color: '#BDBDBD' }}
                />
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 12,

                    fontFamily: 'Metropolis-Medium',
                  }}>
                  {item.direccion}
                </Text>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  marginTop: 8,
                  alignItems: 'center',
                  width: '100%',
                }}>
                <Icon
                  type="font-awesome"
                  name="cart-plus"
                  color="white"
                  size={porcentaje(4)}
                  iconStyle={{ color: '#BDBDBD' }}
                />
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 12,
                    marginHorizontal: 5,
                    fontFamily: 'Metropolis-Medium',
                  }}>
                  {item.listeProduct.length} Productos Seleccionados
                </Text>
              </View>

              <View
                style={{
                  justifyContent: 'center',
                  backgroundColor: '#0f8d8c',
                  borderRadius: 12,
                  marginTop: 8,
                }}>
                <Text
                  style={{
                    color: 'gray',
                    fontSize: 12,
                    color: 'white',
                    padding: 5,
                    paddingHorizontal: 12,
                    textAlign: 'center',
                    fontFamily: 'Metropolis-Bold',
                  }}>
                  Hacer Pedido
                </Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
    );
  };

  const renderBody = () => {
    if (numberCompra > 0) {
      return (
        <FlatList
          //horizontal={true}
          data={dataCart}
          renderItem={({ item, index }) => _renderItem(item, index)}
          keyExtractor={(item, index) => index.toString}
        />
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          {/* <FastImage
            style={{
              width: width - 100,
              resizeMode: 'contain',
              height: width - 100,
            }}
            source={require('../assets/img/ic_vesi_no_existe_compras.png')}
          /> */}

          <Icon
            type="material"
            name="remove-shopping-cart"
            color="white"
            size={porcentaje(25)}
            iconStyle={{ color: '#0f8d8c' }}
          />
          <Text
            style={{
              color: '#cecece',
              fontSize: 12,
              marginHorizontal: 20,
              textAlign: 'center',
              marginTop: 10,
              fontFamily: 'Metropolis-Medium',
            }}>
            No Productos en tu Carrito
          </Text>
        </View>
      );
    }
  };

  const deleteItemModal = (item) => {
    var newCom = numberCompra - 1;
    const index = dataCart.indexOf(item);
    const newArray = dataCart;
    newArray.splice(index, 1);
    setdataCart(newArray);
    setstatusShopa(false);
    setnumberCompra(newCom);
    // this.setState({ dataCart: newArray,numberCompra,statusShopa:false});

    AsyncStorage.setItem('cart', JSON.stringify(newArray));
  };

  return (
    <SafeAreaView
      style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <View style={{ flex: 1, backgroundColor: 'white', width: width }}>
        <View
          style={{
            flexDirection: 'row',
            alignContent: 'center',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={styles.container32}>
            {/* <TouchableOpacity activeOpacity={1} onPressOut={() => this.props.navigation.navigate('ShopLoading')} >
                <FastImage style = {styles.imagenEscudo} resizeMode="contain"  source={require('../../assets/img/ic_vesi_empaquetando.gif')}/>
                
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={1}  onPressOut={() => this.props.navigation.navigate('ShopLoading')} >
                <Text style={styles.textOrangeLista}>Proceso</Text>
                </TouchableOpacity> */}

            {rendernumberProceso()}
          </View>

          <View style={styles.container3}>
            {/* <TouchableOpacity
              activeOpacity={1}
              onPressOut={() => this.props.navigation.navigate('MyShop')}>
              <FastImage
                style={styles.imagenEscudo2}
                resizeMode="contain"
                source={require('../assets/img/ic_vesi_compras.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity
              activeOpacity={1}
              onPressOut={() => this.props.navigation.navigate('MyShop')}>
              <Text style={styles.textOrangeLista}>Lista de compras</Text>
            </TouchableOpacity> */}
          </View>
        </View>

        <Text style={styles.textTitle}>Mis Ordenes</Text>

        {renderBody()}
      </View>

      <Modal isVisible={statusShopa}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() => setstatusShopa(!statusShopa)}></TouchableOpacity>
          {/* <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            // source={require('../../images/ic_bro_cuenta.png')}
            source={require('../assets/img/ic_bro_orden_act.png')}
            resizeMode={FastImage.resizeMode.contain}
          /> */}

          <Icon
            type="ionicon"
            name="information-circle-outline"
            color="white"
            size={porcentaje(20)}
            iconStyle={{ color: '#0f8d8c', marginTop: 8 }}
          />

          <Text style={styles.txtDialogTitle}>
            Deseas eliminar este Producto de tus Compras ?
          </Text>

          <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
            <Button
              buttonStyle={styles.btnStyleDialogClose2}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Eliminar"
              onPress={() => deleteItemModal(itemDelete)}
            />

            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Continuar"
              onPress={() => setstatusShopa(false)}
            />
          </View>
        </View>
      </Modal>

      <Modal isVisible={isModalLocalizacion}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() =>
              setisModalMenuOk(!isModalLocalizacion)
            }></TouchableOpacity>
          {/* <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            // source={require('../../images/ic_bro_cuenta.png')}
            source={require('../assets/img/ic_bro_orden_act.png')}
            resizeMode={FastImage.resizeMode.contain}
          /> */}

          <Icon
            type="ionicon"
            name="md-location-outline"
            color="white"
            size={porcentaje(35)}
            iconStyle={{ color: '#0f8d8c', marginTop: 8 }}
          />

          <Text style={styles.txtDialogTitle}>
            Bro aun no tienes una dirección de envio
          </Text>

          <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
            <Button
              buttonStyle={styles.btnStyleDialogClose2}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Cancelar"
              onPress={() => setisModalLocalizacion(false)}
            />

            <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Elegir"
              onPress={() => {
                setisModalLocalizacion(false);
                props.navigation.navigate('MapaUbicacionLocation');
              }}
            />
          </View>
        </View>
      </Modal>


      <Modal isVisible={isModalSesion}>
        <View
          style={{
            // flex: 1,
            alignSelf: 'center',
            width: width - 32,
            // height: 200,
            backgroundColor: 'white',
            borderRadius: 20,
            paddingBottom: 20,
          }}>
          <TouchableOpacity
            style={{
              alignSelf: 'flex-end',
            }}
            activeOpacity={0.2}
            onPress={() =>
              setisModalSesion(!isModalSesion)
            }></TouchableOpacity>
          {/* <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            // source={require('../../images/ic_bro_cuenta.png')}
            source={require('../assets/img/ic_bro_orden_act.png')}
            resizeMode={FastImage.resizeMode.contain}
          /> */}

          <Icon
            type="ionicon"
            name="md-location-outline"
            color="white"
            size={porcentaje(35)}
            iconStyle={{ color: '#0f8d8c', marginTop: 8 }}
          />

          <Text style={styles.txtDialogTitle}>
            Bro aún no has iniciado sesión!
          </Text>

          <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
            <Button
              buttonStyle={styles.btnStyleDialogClose2}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Entendido"
              onPress={() => setisModalSesion(false)}
            />

            {/* <Button
              buttonStyle={styles.btnStyleDialogClose}
              containerStyle={styles.btnContainerDialog}
              titleStyle={styles.btnTitleDialog}
              title="Elegir"
              onPress={() => {
                setisModalLocalizacion(false);
                props.navigation.navigate('MapaUbicacionLocation');
              }}
            /> */}
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container3: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    //width:'90%',
    justifyContent: 'flex-end',
    marginRight: 5,

    width: width / 2 - 20,
  },

  container32: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    //width:'90%',
    justifyContent: 'flex-start',
    marginRight: 5,

    width: width / 2 - 10,
  },

  container4: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,

    marginHorizontal: 20,
    width: width - 40,
  },

  textOrangeLista: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    color: '#dc751b',
  },

  textFactura: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 + 20,
    textAlign: 'right',
    marginRight: 10,
    marginTop: 5,
  },
  textFacturaNumbers: {
    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 - 65,
    textAlign: 'left',
    marginTop: 5,
  },

  textFacturaMorado: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 + 20,
    textAlign: 'right',
    color: '#482181',
    marginRight: 10,
    marginTop: 5,
  },
  textFacturaNumbersMorado: {
    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 - 65,
    textAlign: 'left',
    color: '#482181',
    marginTop: 5,
  },

  imagenEscudo: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
    marginRight: 2,
  },
  imagenEscudo2: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
    marginRight: 2,
  },

  textHorario: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 10,
    width: width / 2,
    textAlign: 'right',
    color: '#482181',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textHorario3: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 - 20,
    textAlign: 'right',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textHorario2: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 10,
    width: width / 2,
    textAlign: 'left',
    color: '#482181',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  textHorario4: {
    // fontFamily: 'din',

    fontWeight: 'bold',
    fontSize: 12,
    width: width / 2 + 20,
    textAlign: 'left',
    marginRight: 5,
    marginTop: 5,
    opacity: 0.5,
  },

  imgCategoriaEmpresas: {
    width: width / 3.5,
    height: width / 5,
    borderRadius: 8,
  },

  textTitle: {
    marginTop: 5,
    marginBottom: 10,

    fontSize: 18,
    color: 'black',
    textAlign: 'center',
    fontFamily: 'Metropolis-Bold',
  },
  container2: {
    flexDirection: 'row',

    marginTop: 15,
    justifyContent: 'flex-start',
    marginHorizontal: 30,
    width: width - 40,
  },
  imgLogo: {
    width: 80,
    height: 80,
    resizeMode: 'contain',
  },

  imgLogo2: {
    width: width - 40,
    height: width / 2,
    resizeMode: 'contain',
    borderRadius: 8,
    backgroundColor: 'green',
    marginTop: 5,
    borderColor: 'gray',
    borderWidth: 0.5,
  },

  imgBasurero: {
    width: 35,
    height: 35,
    marginLeft: 10,
    resizeMode: 'contain',
  },

  imgCircle: {
    width: 35,
    height: 35,
    marginLeft: 10,
    resizeMode: 'contain',
  },

  txtNombreCom: {
    fontWeight: 'bold',
    fontSize: 15,
    width: width - 100,
    marginLeft: 8,
    textAlign: 'center',
  },

  txtNombreCom2: {
    fontWeight: 'bold',
    fontSize: 15,
    width: width - 40,
    textAlign: 'center',
    marginHorizontal: 20,
  },

  txtDescripcion: {
    marginLeft: 2,
    fontSize: 12,
    width: width / 3,
  },

  imgRepresentacion: {
    width: width,
    height: width / 1.5,
  },

  pickerStyle: {
    width: width - 40,
    fontWeight: 'bold',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 5,

    color: '#482181',
    fontSize: 40,
  },

  modal: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  modal3: {
    height: 300,
    width: 300,
  },

  text: {
    color: 'black',
    fontSize: 22,
  },
  btn: {
    margin: 10,
    backgroundColor: '#3B5998',
    color: 'white',
    padding: 10,
  },
  btnStyleDialogClose: {
    backgroundColor: '#0f8d8c',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialogClose2: {
    backgroundColor: '#ffa35d',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialog: {
    backgroundColor: 'rgba(238, 130, 79,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },
  btnContainerDialog: {
    width: '40%',
    alignSelf: 'center',

    // position: 'absolute',
    // bottom: 16,
    // paddingBottom: 32,
  },
  btnTitleDialog: {
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Metropolis-Medium',
    color: '#ffffff',
  },
  txtDialogTitle: {
    // backgroundColor: 'red',
    width: width - 32,
    // height: '100%',
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontFamily: 'Metropolis-Bold',
    marginTop: 15,
    // position: 'absolute',
    // bottom: 0,
  },
});

export default Miorden;
