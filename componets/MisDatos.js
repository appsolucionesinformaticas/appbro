import React, {useEffect, useState, useRef} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  Animated,
  Keyboard,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  SafeAreaView,
} from 'react-native';

import styles from './registro/registrostyle';
import AsyncStorage from '@react-native-community/async-storage';
import Swiper from 'react-native-swiper';
import FastImage from 'react-native-fast-image';
import Modal from 'react-native-modal';
import {Input, Icon, Button} from 'react-native-elements';

import {TextInput, ScrollView} from 'react-native-gesture-handler';
import Loading from './load/Loading';
var {height, width} = Dimensions.get('window');
function MisDatos(props) {
  const txtNombre = useRef();
  const txtApellido = useRef();
  const txtcelular = useRef();
  const txtcorreo = useRef();
  const txtcedula = useRef();
  const txtpass = useRef();

  const [nombre, setnombre] = useState('');
  const [pass, setpass] = useState('');
  const [apellido, setapellido] = useState('');
  const [celular, setcelular] = useState('');
  const [correo, setcorreo] = useState('');
  const [cedula, setcedula] = useState('');
  const [cargando, setcargando] = useState(false);
  const [isModalMenuOk, setisModalMenuOk] = useState(false);
  const [person, setperson] = useState('');
  const [nameError, setnameError] = useState('');

  const porcentaje = (porcentaje) => {
    return (width * porcentaje) / 100;
  };

  useEffect(() => {
    const unsubscribe = props.navigation.addListener('focus', () => {
      getInfo();
    });
  }, []);

  const savePerson = () => {
    var newCorreo = correo.replace(' ', '');

    var newCellphone = celular.replace(' ', '');

    fetch(global.linkServer + 'wsperson/UpdatePerson', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        code: '',
        per_name: nombre,
        per_last_name: apellido,
        per_ci: person.ci,
        per_cellphone: newCellphone,
        per_email: newCorreo,
        per_password: pass,
        per_url_image: person.urlphoto,
        street: person.addres,
        lon: person.longitud,
        lat: person.latitud,
        idLoc: person.codeCity,
      }),
    })
      .then((response) => response.json())
      .then((data) => {
        if (data == true) {
          const itemPerson = {
            name: nombre,
            lastname: apellido,
            email: newCorreo,
            cellphone: newCellphone,
            pass: pass,
          };

          try {
            AsyncStorage.mergeItem('person', JSON.stringify(itemPerson));
            setcargando(false);

            setnameError('Datos Guardados con Exito');
            setisModalMenuOk(true);
          } catch (error) {
            setcargando(false);
            alert('Hubo un error Intenalo de nuevo');
          }
        } else {
          setcargando(false);
          alert('Hubo un error Intenalo de nuevo');
        }
      })
      .catch((error) => console.log(error));
  };

  const getInfo = () => {
    AsyncStorage.getItem('person')
      .then((person) => {
        if (person !== null) {
          var personData = JSON.parse(person);

          setapellido(personData.lastname);
          setnombre(personData.name);
          setcelular(personData.cellphone);
          setcorreo(personData.email);
          setcedula(personData.ci);
          setpass(personData.pass);

          var interval = setInterval(() => {
            clearInterval(interval);
            setperson(personData);
          }, 100);
        }
      })
      .catch((err) => {
        alert(err);
      });
  };
  const registrar = () => {
    var ingresar = true;

    // txtcedula.current.shake();
    if (nombre === '') {
      ingresar = false;
      txtNombre.current.shake();
    }

    if (apellido === '') {
      ingresar = false;
      txtApellido.current.shake();
    }

    if (celular === '') {
      ingresar = false;
      txtcelular.current.shake();
    }

    if (correo === '') {
      ingresar = false;
      txtcorreo.current.shake();
    }

    if (cedula == '') {
      ingresar = false;
      txtcedula.current.shake();
    }
    if (pass == '') {
      ingresar = false;
      txtpass.current.shake();
    }

    if (ingresar) {
      setcargando(true);
      savePerson();
    }

    // props.navigation.navigate('Localidad');
  };

  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}>
      <KeyboardAvoidingView
        behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
        style={{flex: 1}}>
        <SafeAreaView
          style={{
            borderRadius: 12,
          }}>
          <View
            style={{
              height: '100%',
              backgroundColor: 'white',
              alignContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '90%',
                alignItems: 'flex-start',
                marginTop: 8,
              }}>
              <TouchableOpacity
                activeOpacity={0.5}
                style={{marginLeft: 8, padding: 5}}
                onPress={() => props.navigation.goBack()}>
                <Icon
                  type="ionicon"
                  name="arrow-back"
                  color="white"
                  size={porcentaje(9)}
                  iconStyle={{color: 'black'}}
                />
              </TouchableOpacity>
            </View>
            <ScrollView
              style={{
                height: '100%',
                borderRadius: 20,
                backgroundColor: 'white',
              }}>
              <View
                style={{
                  flex: 1,
                  alignItems: 'center',
                  alignContent: 'center',
                  borderRadius: 30,
                  paddingBottom: 20,
                }}>
                <Text
                  style={{
                    marginTop: 30,
                    fontSize: 35,
                    textAlign: 'center',
                    color: 'black',
                    fontFamily: 'Metropolis-Bold',
                    width: '90%',
                  }}>
                  Mis Datos
                </Text>

                <Text
                  style={{
                    marginTop: 12,
                    marginBottom: 12,
                    fontSize: 16,
                    textAlign: 'center',
                    color: 'gray',
                    fontFamily: 'Metropolis-Medium',

                    width: '90%',
                  }}>
                  Información General
                </Text>

                <Input
                  textContentType="givenName"
                  keyboardType="numeric"
                  returnKeyType="next"
                  buttonbor
                  disabled={true}
                  value={cedula}
                  ref={txtcedula}
                  inputContainerStyle={{
                    borderBottomWidth: 0,
                    alignSelf: 'center',
                    marginVertical: -12,
                    marginTop: 10,
                  }}
                  onSubmitEditing={() => txtNombre.current.focus()}
                  onChangeText={(text) => setcedula(text)}
                  placeholder="Cédula"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  style={styles.inputDatos}
                />

                <Input
                  textContentType="givenName"
                  keyboardType="name-phone-pad"
                  returnKeyType="next"
                  ref={txtNombre}
                  value={nombre}
                  inputContainerStyle={{
                    borderBottomWidth: 0,
                    alignSelf: 'center',
                    marginVertical: -12,
                    borderBottomColor: 'red',
                  }}
                  onSubmitEditing={() => txtApellido.current.focus()}
                  placeholder="Nombre"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  onChangeText={(text) => setnombre(text)}
                  style={styles.inputDatos}
                />

                <Input
                  textContentType="givenName"
                  keyboardType="name-phone-pad"
                  returnKeyType="next"
                  value={apellido}
                  ref={txtApellido}
                  inputContainerStyle={{
                    borderBottomWidth: 0,
                    alignSelf: 'center',
                    marginVertical: -12,
                  }}
                  onSubmitEditing={() => txtcelular.current.focus()}
                  placeholder="Apellido"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  onChangeText={(text) => setapellido(text)}
                  style={styles.inputDatos}
                />

                <Input
                  textContentType="givenName"
                  keyboardType="numeric"
                  returnKeyType="next"
                  value={celular}
                  ref={txtcelular}
                  onSubmitEditing={() => txtcorreo.current.focus()}
                  placeholder="Celular"
                  inputContainerStyle={{
                    borderBottomWidth: 0,
                    alignSelf: 'center',
                    marginVertical: -12,
                  }}
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  onChangeText={(text) => setcelular(text)}
                  style={styles.inputDatos}
                />

                <Input
                  textContentType="givenName"
                  keyboardType="numeric"
                  returnKeyType="next"
                  buttonbor
                  value={correo}
                  ref={txtcorreo}
                  inputContainerStyle={{
                    borderBottomWidth: 0,
                    alignSelf: 'center',
                    marginVertical: -12,
                  }}
                  onSubmitEditing={() => txtpass.current.focus()}
                  onChangeText={(text) => setcorreo(text)}
                  placeholder="Cédula"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  style={styles.inputDatos}
                />

                <Input
                  textContentType="givenName"
                  keyboardType="name-phone-pad"
                  returnKeyType="done"
                  buttonbor
                  value={pass}
                  ref={txtpass}
                  inputContainerStyle={{
                    borderBottomWidth: 0,
                    alignSelf: 'center',
                    marginVertical: -12,
                  }}
                  // onSubmitEditing={() => lstname.current.focus()}
                  onChangeText={(text) => setpass(text)}
                  placeholder="Constraseña"
                  placeholderTextColor="'rgba(52, 52, 52,0.2)'"
                  style={styles.inputDatos}
                />

                <TouchableOpacity
                  style={{
                    backgroundColor: '#ffa35d',
                    marginTop: 40,

                    borderRadius: 30,

                    width: '55%',
                  }}
                  activeOpacity={0.9}
                  onPress={() => registrar()}>
                  <Text
                    style={{
                      paddingVertical: 12,
                      textAlign: 'center',
                      borderRadius: 30,
                      color: 'white',

                      width: '100%',

                      fontFamily: 'Metropolis-Medium',
                    }}>
                    Actualizar Datos
                  </Text>
                </TouchableOpacity>
              </View>
            </ScrollView>
          </View>
          <Loading text="Cargando datos..." isVisible={cargando} />

          <Modal isVisible={isModalMenuOk}>
            <View
              style={{
                // flex: 1,
                alignSelf: 'center',
                width: width - 32,
                // height: 200,
                backgroundColor: 'white',
                borderRadius: 20,
                paddingBottom: 20,
              }}>
              <TouchableOpacity
                style={{
                  alignSelf: 'flex-end',
                }}
                activeOpacity={0.2}
                onPress={() =>
                  setisModalMenuOk(!isModalMenuOk)
                }></TouchableOpacity>
              {/* <FastImage
            style={{
              width: 35,
              height: 35,
              top: -13,
              alignSelf: 'center',
            }}
            // source={require('../../images/ic_bro_cuenta.png')}
            source={require('../assets/img/ic_bro_orden_act.png')}
            resizeMode={FastImage.resizeMode.contain}
          /> */}

              <Icon
                type="material"
                name="error-outline"
                color="white"
                size={porcentaje(35)}
                iconStyle={{color: '#0f8d8c', marginTop: 8}}
              />

              <Text style={stylesR.txtDialogTitle}>{nameError}</Text>

              <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                <Button
                  buttonStyle={stylesR.btnStyleDialogClose}
                  containerStyle={stylesR.btnContainerDialog}
                  titleStyle={stylesR.btnTitleDialog}
                  title="Continuar"
                  onPress={() => setisModalMenuOk(false)}
                />
              </View>
            </View>
          </Modal>
        </SafeAreaView>
      </KeyboardAvoidingView>
    </TouchableWithoutFeedback>
  );
}

const stylesR = StyleSheet.create({
  titleSection: {
    color: '#482181',
    fontWeight: 'bold',
    width: width - 40,
    marginHorizontal: 20,
    fontSize: 20,
    marginTop: 15,
  },

  textModal: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
  },
  imgModalMenu: {
    width: width / 5,
    height: width / 5,
    borderRadius: 12,
  },
  container2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    marginBottom: 5,
    width: '95%',
  },
  text: {
    width: '95%',

    marginTop: 1,
    fontSize: 15,
    color: 'black',
    fontFamily: 'Metropolis-Medium',
  },
  text2: {
    width: '95%',
    marginTop: 1,
    fontSize: 12,
    color: 'gray',
    textAlign: 'justify',
    fontFamily: 'Metropolis-Medium',
  },
  imgProduct: {
    width: width / 2 - 60,
    height: width / 2 - 60,
    resizeMode: 'contain',
    marginLeft: 15,
  },
  imgCompania: {
    width: width,
    height: width / 1.5,
  },
  container3: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 15,
    width: width - 60,
    marginLeft: 10,
  },
  logoComapania: {
    width: 30,
    height: 30,
    resizeMode: 'contain',
  },
  listCategoria: {
    alignItems: 'center',
    alignContent: 'center',
    width: width - 20,
    marginTop: 16,
  },
  btnStyleDialogClose: {
    backgroundColor: '#0f8d8c',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },

  btnStyleDialog: {
    backgroundColor: 'rgba(238, 130, 79,0.85)',
    shadowColor: 'rgba(238, 130, 79,0.9)',
    shadowRadius: 5,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 4,
    borderRadius: 5,
    marginTop: 24,
    marginHorizontal: 5,
  },
  btnContainerDialog: {
    width: '80%',
    alignSelf: 'center',

    // position: 'absolute',
    // bottom: 16,
    // paddingBottom: 32,
  },
  btnTitleDialog: {
    fontSize: 18,
    textAlign: 'center',
    fontFamily: 'Metropolis-Medium',
    color: '#ffffff',
  },
  txtDialogTitle: {
    // backgroundColor: 'red',
    width: width - 32,
    // height: '100%',
    textAlign: 'center',
    color: 'black',
    fontSize: 20,
    fontFamily: 'Metropolis-Bold',
    marginTop: 15,
    // position: 'absolute',
    // bottom: 0,
  },
});

export default MisDatos;
