import {Dimensions, StyleSheet, StatusBar} from 'react-native';

const React = require('react-native');

var {height, width} = Dimensions.get('window');
// const { StyleSheet } = React;

export default {
  container: {
    flex: 1,
    alignItems: 'center',
    alignContent: 'center',
  },

  imgCategoria: {
    width: width / 3.5,
    height: width / 3.5,
    borderRadius: 12,
  },

  iconUbicacion: {
    color: 'gray',
    marginLeft: 16,
    marginTop: 8,
    alignContent: 'flex-start',
    alignItems: 'flex-start',
  },

  imgCategoriaEmpresas: {
    width: width / 3.5,
    height: width / 5,
    borderRadius: 8,
    justifyContent: 'center',
  },

  imgCategoriaEmpresas2: {
    width: width / 3.5,
    height: width / 7,
    borderRadius: 8,
  },

  imgModalMenu: {
    width: width / 5,
    height: width / 5,
    borderRadius: 12,
  },

  listCategoria: {
    alignItems: 'center',
    alignContent: 'center',
    width: width - 20,
    marginTop: 16,
    backgroundColor: 'red',
  },

  itemdedoview: {
    width: width - width / 1.4,
    height: width / 3,
    alignContent: 'center',
    alignContent: 'center',
    justifyContent: 'center',
  },

  viewTextCenter: {
    width: width / 1.4,
    height: width / 3,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  txtCenterItem: {
    textAlign: 'center',
    color: 'white',
    fontSize: 30,
    fontWeight: 'bold',
  },

  viewdedo: {
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgDedo: {
    width: width / 12,
    height: width / 12,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },

  txtitemListCategoria: {
    textAlign: 'center',
    marginLeft: 5,
    marginRight: 5,
    fontSize: 14,
    color: '#ee824f',
  },

  itemCategoriaIzquierda: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
    width: width,
    marginTop: 16,
  },

  itemCategoriaDerecha: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    alignContent: 'flex-end',
    width: width,
    marginTop: 16,
  },

  imgItemCategoria: {
    width: width / 3,
    height: 50,
  },

  textCategoria: {
    fontSize: 18,
    textAlign: 'center',
    color: 'black',
  },

  textModal: {
    fontSize: 16,
    textAlign: 'center',
    color: 'white',
    marginTop: 3,
  },

  cardList: {
    flexDirection: 'row',
    marginTop: 2,
    backgroundColor: '#fff', //f7fafd
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#c1c1c1',
    marginHorizontal: 1,
    marginTop: 3,
  },
  imgClient: {
    width: 80,
    height: 80,
    borderWidth: 3,
    borderColor: '#0d518b',
    borderRadius: 40,
    marginLeft: 8,
  },
  dividerLts: {
    width: '100%',
    // marginHorizontal: "1%",
    height: 1,
    marginBottom: 4,
    marginTop: 4,
    backgroundColor: '#0d518b',
    shadowColor: '#073053',
    shadowRadius: 1,
    shadowOpacity: 0.3,
    shadowOffset: {
      width: 1,
      height: 2,
    },
    elevation: 2,
  },
  txtTitle: {
    fontWeight: 'bold',
    fontSize: 18,
    textAlign: 'left',
    color: '#191919', ////"#EEBECB"
  },
  txtSubTitle: {
    fontWeight: 'normal',
    fontSize: 14,
    textAlign: 'left',
    color: '#191919', ////"#EEBECB"
  },
  txtNameTitle: {
    fontWeight: 'bold',
    fontSize: 24,
    textAlign: 'center',
    color: '#191919', ////"#EEBECB"
  },
  imgOpt: {
    marginTop: 4,
    height: '58%',
    width: '58%',
    alignSelf: 'center',
    // backgroundColor: "#c1c1c1"
  },
  bgOpt: {
    backgroundColor: '#137ad1',
    width: width / 3 - 24,
    height: width / 3 - 24,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#fff',
    marginHorizontal: 5,
    shadowColor: '#073053',
    shadowRadius: 3,
    shadowOpacity: 0.8,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    elevation: 5,
  },
  txtTitleOpt: {
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
    alignSelf: 'center',
    textAlignVertical: 'center',
    color: '#fff',
    height: '38%',
    width: '90%',
    // backgroundColor: "#c1c1c1"
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    marginTop: 5,
    marginRight: 5,
    marginLeft: 5,
  },

  containerCancho: {
    width: width - 20,
    height: width / 5,
    alignContent: 'flex-end',
    alignItems: 'flex-end',
    position: 'absolute',
    bottom: 0,
  },

  imgChancho: {
    position: 'absolute',
    bottom: 0,
    width: width / 5,
    height: width / 5,
    flexDirection: 'row',
    alignContent: 'flex-end',
    alignItems: 'flex-end',
  },

  buscarStyle: {
    width: width - 32 - 70 - 32,
    backgroundColor: 'rgba(238, 130, 79,0.8)',
    backgroundColor: 'rgba(0, 0, 0, 0)',
    fontSize: 20,
    color: 'white',
    paddingLeft: 4,
  },

  buscarContainerStyle: {
    width: width - 32 - 70 - 32,
    height: 60,
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderBottomColor: 'rgba(0, 0, 0, 0)',
    borderTopColor: 'rgba(0, 0, 0, 0)',
  },
  btnTopMenu: {
    width: 35,
    height: 35,
    position: 'absolute',
    right: 16,
    alignSelf: 'center',
  },
  btnTopPerfil: {
    width: 35,
    height: 35,
    position: 'absolute',
    left: 16,
    alignSelf: 'center',
  },
};
